/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_specie_unit.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_specie_unit.c
 * @brief       Functions related to the specie_unit calculation object
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <errno.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Creates and initializes a new specie_unit
 * @return Pointer towards newly created specie_unit
 */
s_specie_unit_fp *FP_create_specie_unit(int id, int id_type) {

  int i;
  s_specie_unit_fp *p_speunit = NULL;

  p_speunit = new_specie_unit();
  bzero((char *)p_speunit, sizeof(p_speunit));

  p_speunit->id[id_type] = id;
  p_speunit->area = 0.;
  p_speunit->next = NULL;
  p_speunit->prev = NULL;

  p_speunit->pft = (s_ft **)malloc(FP_NVAR_INPUT_TRANSPORT * sizeof(s_ft *));
  bzero((char *)p_speunit->pft, FP_NVAR_INPUT_TRANSPORT * sizeof(s_ft *));
  for (i = 0; i < FP_NVAR_INPUT_TRANSPORT; i++)
    p_speunit->pft[i] = NULL;

  return p_speunit;
}

/**
 * @brief Frees one specie unit pointer
 * @return NULL pointer if everything goes well
 */
s_specie_unit_fp *FP_free_one_specie_unit(s_specie_unit_fp *p_speunit, FILE *fpout) {

  if (p_speunit == NULL)
    LP_warning(fpout, "libfp%4.2f, File %s in %s line %d : Nothing to free. Specie unit pointer is already NULL. Might cause crashes later on...\n", VERSION_FP, __FILE__, __func__, __LINE__);
  else {
    free(p_speunit);

    p_speunit = NULL;
  }
  return p_speunit;
}

/**
 * @brief Chain two specie units
 * @return Pointer towards the chain the last element of the chain
 */
s_specie_unit_fp *FP_secured_chain_specie_unit_fwd(s_specie_unit_fp *p_speunit1, s_specie_unit_fp *p_speunit2) {

  if (p_speunit1 == NULL)
    p_speunit1 = p_speunit2;
  else {
    p_speunit1->next = p_speunit2;
    p_speunit2->prev = p_speunit1;
  }

  return p_speunit1;
}

/**
 * @brief Browse a specie unit chain forward or backwards
 * @return Pointer to the first or last pointer in the chain
 */
s_specie_unit_fp *FP_browse_specie_unit_chain(s_specie_unit_fp *p_speunit, int browse_type, FILE *fpout) {

  s_specie_unit_fp *ptemp = NULL;

  if (p_speunit == NULL)
    LP_error(fpout, "In libfp%4.2f, File %s in %s line %d : Nothing to browse. Specie unit chain is NULL.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  ptemp = p_speunit;

  switch (browse_type) {
  case BEGINNING_FP:
    while (ptemp->prev != NULL)
      ptemp = ptemp->prev;
    break;
  case END_FP:
    while (ptemp->next != NULL)
      ptemp = ptemp->next;
    break;
  default:
    LP_error(fpout, "In libfp%4.2f, File %s in %s line %d : Unknown browse type.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    break;
  }
  return ptemp;
}

/**
 * @brief Prints attributes of each pointer of a specie unit pointer chain
 * @return none
 */
void FP_print_species_unit_chain(s_specie_unit_fp *p_speunit, FILE *fpout) {

  s_specie_unit_fp *ptemp = NULL;

  if (p_speunit == NULL)
    LP_error(fpout, "In libfp%4.2f, File %s in %s line %d : Chain pointer is NULL. Nothing to print out.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  ptemp = FP_browse_specie_unit_chain(p_speunit, BEGINNING_FP, fpout);

  while (ptemp != NULL) {
    LP_printf(fpout, "Specie units attributes for specie GIS ID %d - Area : %7.2f m2.\n", ptemp->id[FP_GIS], ptemp->area);
    ptemp = ptemp->next;
  }
}

/**
 * @brief Turns a chain of s_specie_unit pointer into a tabular of pointer in order to be stored in the carac_fp structure
 * @return Tabular of specie_unit_fp pointers
 */
s_specie_unit_fp **FP_tab_specie_unit(s_specie_unit_fp *chain, int nbtot_unit, FILE *flog) {

  int i;
  s_specie_unit_fp **tab_speunit = NULL;

  tab_speunit = (s_specie_unit_fp **)malloc(nbtot_unit * sizeof(s_specie_unit_fp *));
  bzero((char *)tab_speunit, nbtot_unit * sizeof(s_specie_unit_fp *));

  chain = FP_browse_specie_unit_chain(chain, BEGINNING_FP, flog);

  for (i = 0; i < nbtot_unit; i++) {
    chain->id[FP_INTERN] = i;
    chain->id[FP_ABS] = i;
    tab_speunit[i] = chain;
    chain = chain->next;
  }
  return tab_speunit;
}

/**
 * @brief Prints the specie unit attributes once they are stored in the carac_fp struct table
 * @return none
 */
void FP_print_species_unit_tab(s_specie_unit_fp **tab_speunit, int nb_specie_unit, int id_species, FILE *flog) {

  int i;
  double surface_tot = 0;

  if (tab_speunit != NULL) {
    LP_printf(flog, "Specie units attributes for specie INTERN id %d :\n", id_species);
    LP_printf(flog, "Total number of specie units declared = %d.\n", nb_specie_unit);

    for (i = 0; i < nb_specie_unit; i++) {
      LP_printf(flog, "IDs (INTERN: %d, ABS: %d, GIS: %d). Surface (in m2): %7.2f.\n", tab_speunit[i]->id[FP_INTERN], tab_speunit[i]->id[FP_ABS], tab_speunit[i]->id[FP_GIS], tab_speunit[i]->area);

      surface_tot += tab_speunit[i]->area;
    };
    LP_printf(flog, "Total surface of specie grid coverage : %7.2f m2.\n", surface_tot);
  } else
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Specie unit tabular in NULL.\n", VERSION_FP, __FILE__, __func__, __LINE__);
}

/**
 * @brief Counts the number of elements in a species unit chain
 * @return int Number of elements
 */
int FP_count_specie_units_from_chain(s_specie_unit_fp *pchain, FILE *flog) {

  s_specie_unit_fp *ptemp = NULL;
  int nb_units = 0;

  if (pchain == NULL)
    LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Specie unit chain in NULL.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  ptemp = FP_browse_specie_unit_chain(pchain, BEGINNING_FP, flog);

  while (ptemp != NULL) {
    nb_units++;
    ptemp = ptemp->next;
  }

  return nb_units;
}

/**
 * @brief Creates a copy of a pointer - Used to extract a pointer which is in a chain
 * @return pointer towards newly create single pointer
 */
s_specie_unit_fp *FP_copy_one_specie_unit_pointer(s_specie_unit_fp *p_speunit, FILE *flog) {
  s_specie_unit_fp *pcpy = NULL;

  if (p_speunit == NULL)
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Specie unit pointer in NULL : Copy impossible.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  else {
    pcpy = FP_create_specie_unit(p_speunit->id[FP_GIS], FP_GIS);
    pcpy->area = p_speunit->area;
  }
  return pcpy;
}

/**
 * @brief Based on the GIS id, verifies if a specie unit already has its pointer defined
 * @return If successfull, return the pointer towards this specie_unit
 */
s_specie_unit_fp *FP_check_is_ele_specie_unit_defined(s_specie_unit_fp **tab_ele, int nb_specie_unit, int id_gis, FILE *fpout) {

  int i, found;
  s_specie_unit_fp *psepunit = NULL;

  if (tab_ele == NULL)
    LP_error(fpout, "libfp%4.2f, File %s in %s line %d : Pointer table is null.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_specie_unit; i++) {
    found = NO;
    if (tab_ele[i]->id[FP_GIS] == id_gis) {
      psepunit = tab_ele[i];
      found = YES;
      break;
    }
  }
  if (found == NO)
    LP_error(fpout, "libfp%4.2f, File %s in %s line %d : Specie unit GIS id %d hasn't been defined yet. Check the consistency between \
             the species_cell and bu_transport input files.\n",
             VERSION_FP, __FILE__, __func__, __LINE__, id_gis);
  return psepunit;
}

/**
 * @brief Deallocates a entire specit unit chain
 * @return NULL is successful
 */
s_specie_unit_fp *FP_free_specie_unit_chain(s_specie_unit_fp *pchain, FILE *flog) {

  s_specie_unit_fp *ptmp;

  pchain = FP_browse_specie_unit_chain(pchain, BEGINNING_FP, flog);

  ptmp = pchain;

  while (pchain->next != NULL) {
    ptmp = pchain->next;
    pchain = FP_free_one_specie_unit(pchain, flog);
    pchain = ptmp;
  }
  pchain = FP_free_one_specie_unit(pchain, flog); // Freeing the last one

  if (pchain != NULL)
    LP_warning(flog, "In libfp%4.2f, File %s in %s line %d : Specie unit chain is not properly deallocated.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  return pchain;
}

/**
 * @brief Reads all forcing files, for all species, all parameters. Creates all times series for all specit_units
 * @return none
 */
void FP_read_all_transport_inputs(s_carac_fp *pcarac_fp, int nb_species, int year_start, FILE *flog) {

  int s, p, m;
  char *folder_path;
  s_input_fp *pinput;
  s_specie_unit_fp *pspeunit;
  FILE *fp_in = NULL;
  double sum = 0.;
  double conv_unit = 1.;

  pinput = pcarac_fp->pinputs;

  for (s = 0; s < nb_species; s++) {

    // Fetching the adequate folder path for each specie. It can't be NULL at this point (condition checked in input.y)
    folder_path = pinput->transport_paths[s];

    for (p = 0; p < FP_NVAR_INPUT_TRANSPORT; p++) {
      if (pinput->transport_input_prefixes[s][p] != NULL) {

        // Fetching here the unit of the forcing variable read from the command file and directly storing the la time_serie in the CaWaQS reference unit.
        conv_unit = pinput->forcing_unit[s][p];

        LP_printf(flog, "Transport input parameter : %s. Conversion factor for time serie : %f.\n", FP_transport_input_variable_name(p), conv_unit); // NG check

        fp_in = FP_check_exist_forcing_file(folder_path, pinput->transport_input_prefixes[s][p], pinput->transport_input_format[s], year_start, flog);

        for (m = 0; m < pcarac_fp->nb_specie_unit[s]; m++) {
          sum = 0.;
          pspeunit = pcarac_fp->p_species_unit[s][m];

          // Emptying time series for the previous year
          if (pspeunit->pft[p] != NULL)
            pspeunit->pft[p] = TS_free_ts(pspeunit->pft[p], flog);

          pspeunit->pft[p] = FP_read_bin_specie_unit(fp_in, pinput->transport_input_prefixes[s][p], pcarac_fp->nb_specie_unit[s], year_start, pspeunit->id[FP_INTERN], conv_unit, flog);

          // TS_print_ts_with_date(pspeunit->pft[p],CODE_FP,FR_TS,flog,flog); // NG check

          // NG : Verification function which prints out cumulative values once the ts is created to make sure files have been properly read and stored.
          // sum=TS_sum_ts(pspeunit->pft[p],flog);
          // LP_printf(flog,"Specie %d specie unit GIS id %d Variable %s File Prefix %s Year %d-%d : Yearly cummulative value : %7.2f.\n",s,pspeunit->id[FP_GIS],FP_transport_input_variable_name(p),pinput->transport_input_prefixes[s][p],year_start,year_start+1,sum);
        }
      }
    }
  }
}

/**
 * @brief Checks for forcing file existance prior to reading it
 * @return Pointer towards opened file, if it does exist
 */
FILE *FP_check_exist_forcing_file(char *path, char *prefix, int file_format, int year, FILE *flog) {

  char name[STRING_LENGTH_LP];
  FILE *fp_in = NULL;

  if (file_format == UNFORMATTED_IO) {
    sprintf(name, "%s/%s.%d%d", path, prefix, year, year + 1);
    fp_in = fopen(name, "rb");
    if (fp_in == NULL) {
      sprintf(name, "%s/%s.%d%d.dir", path, prefix, year, year + 1);
      fp_in = fopen(name, "rb");
      if (fp_in == NULL)
        LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d : Forcing file %s unreachable. Please check the input data.\n", VERSION_FP, __FILE__, __func__, __LINE__, name);
    }
  } else
    LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d : FORMATTED type option for forcing files has not been \
                       implemented yet ! Use binary files instead.\n",
             VERSION_FP, __FILE__, __func__, __LINE__);

  return fp_in;
}

/**
 * @brief Reads a yearly binary forcing file. Creates time series values for all specie units for one specie.
 * @return Pointer towards time serie forcing values
 */
s_ft *FP_read_bin_specie_unit(FILE *fpin, char *prefix, int nb_specie_unit, int year, int id_intern, double conv_unit, FILE *flog) {
  int i;
  int errno, pos = 0, location = 0, nb_days = 0, nb_cells = 0;
  float val = 0.;
  double forcing_value = 0., julian_day = 0.;
  double size_file, verif;
  s_ft *pft = NULL, *ptmp = NULL;

  // NG check : Thanks to FP_check_exist_forcing_file(), this should never occur. Just making sure anyway.
  if (fpin == NULL)
    LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d : Pointer for forcing \
                                     file %s for year %d-%d is NULL !\n",
             VERSION_FP, __FILE__, __func__, __LINE__, prefix, year, year + 1);

  errno = fseek(fpin, pos, SEEK_END);
  size_file = (double)ftell(fpin); // Returns -1 if fail

  if (size_file < 0)
    LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d : %s. Cannot determine size of \
					   forcing file %s for year %d-%d.\n",
             VERSION_FP, __FILE__, __func__, __LINE__, strerror(errno), prefix, year, year + 1);

  nb_days = TS_nb_jour_an(year + 1);
  verif = size_file / ((double)nb_days * sizeof(float));
  nb_cells = (int)verif;
  // LP_printf(flog,"Number of detected values per record in file %s for year %d-%d: %d\n",prefix,year,year+1,nb_cells); // NG check

  if (fabs(nb_cells - nb_specie_unit) > EPS_FP)
    LP_error(flog, "Incoherent record length. File %s for year %d-%d isn't formated properly. It should be a binary file filled with FLOAT type data with records of %d values.\n", prefix, year, year + 1, nb_specie_unit);

  for (i = 0; i < nb_days; i++) {
    location = (i * nb_cells + (id_intern)) * sizeof(float);

    if (location < 0 || location > size_file)
      LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d : %s - Could not reach value location in binary file %s.%d-%d (day= %d INTERN ID specie unit %d). The location is %d and the max location is %f.\n", VERSION_FP, __FILE__, __func__, __LINE__, prefix, year, year + 1, i, id_intern, location, size_file);

    errno = fseek(fpin, location, SEEK_SET);
    fread(&val, sizeof(float), 1, fpin);
    // LP_printf(flog,"nb_day : %d, id_unit %d position %d valeur %f.\n",i,id_intern,location,val); // NG check

    forcing_value = (double)val;
    julian_day = (double)TS_calculate_jour_julien_j_nbday(year, CODE_FP, i);

    ptmp = TS_create_function(julian_day, forcing_value * conv_unit); // Using the conversion factor for the time serie unit.
    pft = TS_secured_chain_fwd_ts(pft, ptmp);
  }

  pft = TS_browse_ft(pft, BEGINNING_TS);
  return pft;
}

/**
 * @brief Get values at t of all forcing variables (if defined) for a specie unit
 * @return Array of doubles filled with found values
 */
double *FP_get_forcing_values(double t, s_specie_unit_fp *punit, FILE *flog) {
  double *values;
  int i = 0;

  values = (double *)malloc(FP_NVAR_INPUT_TRANSPORT * sizeof(double));
  bzero((char *)values, FP_NVAR_INPUT_TRANSPORT * sizeof(double));

  if (punit->pft != NULL) {
    for (i = 0; i < FP_NVAR_INPUT_TRANSPORT; i++) {
      if (punit->pft[i] != NULL)
        values[i] = TS_interpolate_ts(t, punit->pft[i]);
      else
        values[i] = 0.;
    }
  }
  return values;
}

/**
 * @brief Checks if there are any duplicate in a specie_unit chain, based on the GIS ID. Error if so.
 * @return none
 */
void FP_check_duplicates_specie_unit(int id_species, s_specie_unit_fp *pchain, FILE *flog) {

  s_specie_unit_fp *ptmp = NULL, *pref = NULL;
  int count;

  if (pchain == NULL)
    LP_error(flog, "In libfp%4.2f : Error in file %s, function %s at line %d : %s. Specie unit pointer chain is NULL\n.", VERSION_FP, __FILE__, __func__, __LINE__);

  pref = FP_browse_specie_unit_chain(pchain, BEGINNING_FP, flog);

  while (pref != NULL) {
    count = 0;
    //	LP_printf(flog,"Checking duplicate for specie id %d, specie cell GIS ID %d.\n",id_species,pref->id[FP_GIS]); // NG check
    ptmp = FP_browse_specie_unit_chain(pchain, BEGINNING_FP, flog);

    while (ptmp != NULL) {
      if (ptmp->id[FP_GIS] == pref->id[FP_GIS]) {
        count++;
        if (count > 1)
          LP_error(flog, "In libfp%4.2f : Error in file %s, function %s at line %d. Duplicate GIS ID %d detected. Check your specie_cell file for specie %d.\n", VERSION_FP, __FILE__, __func__, __LINE__, pref->id[FP_GIS], id_species);
      }

      ptmp = ptmp->next;
    }
    pref = pref->next;
  }
}