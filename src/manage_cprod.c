/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_cprod.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_cprod.c
 * @brief       Functions dealing with attributes of the instances of the cell_prod structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Creates a new cell_prod and initializes with default settings
 * @return Newly created s_cprod_fp* pointer
 */
s_cprod_fp *FP_create_cprod(int indice, int type) {
  s_cprod_fp *pcprod;
  int i;
  pcprod = new_cprod();
  bzero((char *)pcprod, sizeof(s_cprod_fp));
  pcprod->link = (s_id_spa **)malloc(NLINK * sizeof(s_id_spa *));

  for (i = 0; i < NLINK; i++) {
    pcprod->link[i] = NULL;
  }

  pcprod->cell_prod_type = FP_CATCH_RIV; // NG : 21/01/2020
  pcprod->p_catch = NULL;                // NG : 23/01/2020
  pcprod->id[type] = indice;
  pcprod->pwat_bal = RSV_create_wat_bal();

  // Transport related attributes
  pcprod->input_mflux = NULL;
  return pcprod;
}

/**
 * @brief Frees a s_cprod_fp* cell prod pointer
 * @return Null pointer if success
 */
s_cprod_fp *FP_free_cprod(s_cprod_fp *pcprod, FILE *fpout) {

  int i;

  for (i = 0; i < NLINK; i++) {
    if (pcprod->link[i] != NULL) {
      // LP_printf(fpout,"freeing link %d\n",i); BL to debug
      pcprod->link[i] = SPA_free_ids(pcprod->link[i], fpout);
    }
  }
  free(pcprod->link);
  pcprod->link = NULL;
  pcprod->pwat_bal = RSV_free_wat_bal(pcprod->pwat_bal);
  if (pcprod->prsv != NULL) {
    pcprod->prsv = RSV_free_rsv(pcprod->prsv);
  }
  free(pcprod);
  return NULL;
}

/**
 * @brief Chains two s_cprod_fp* pointers
 */
s_cprod_fp *chain_cprod_fp(s_cprod_fp *cprod_fp1, s_cprod_fp *cprod_fp2) {
  cprod_fp1->next = cprod_fp2;
  cprod_fp2->prev = cprod_fp1;
  return cprod_fp1;
}

/**
 * @brief Chains two s_cprod_fp* pointers
 */
s_cprod_fp *FP_secured_chain_cprod_fp(s_cprod_fp *cprod_fp1, s_cprod_fp *cprod_fp2) {
  if (cprod_fp1 != NULL) {
    cprod_fp1 = chain_cprod_fp(cprod_fp1, cprod_fp2);
  } else {
    cprod_fp1 = cprod_fp2;
  }
  return cprod_fp1;
}

/**
 * @brief Stores all the s_cprod_fp* pointers into a single array. Also
 * attributes Cprod IDs.
 * @return Filled s_cprod_fp** array
 */
s_cprod_fp **FP_tab_cprod(s_cprod_fp *cprod, int nb_cprod, FILE *fpout) {

  int i = 0;
  s_cprod_fp **tab_cprod;

  tab_cprod = (s_cprod_fp **)malloc(nb_cprod * sizeof(s_cprod_fp *));
  bzero((char *)tab_cprod, nb_cprod * sizeof(s_cprod_fp *));

  cprod = (s_cprod_fp *)SPA_browse_all_struct(CPROD_SPA, cprod, BEGINNING_FP);
  for (i = 0; i < nb_cprod; i++) {

    cprod->id[FP_INTERN] = i;
    cprod->id[FP_ABS] = i;
    tab_cprod[i] = cprod;
    cprod = cprod->next;
  }
  return tab_cprod;
}

/**
 * @brief Launches surface water balance spatial agregation from balance
 * unit to cell prod scale (for all cell prods). Accounts for parallelized calculations.
 * Parent function launched from main_cawaqs.c
 * @return None
 */
void FP_calc_watbal_cprod_all(s_carac_fp *pcarac_fp, double dt, double t, FILE *fpout) {
  int i, j;
  s_id_spa *link, *link_mto;
  int nb_cprod;
  s_cprod_fp **p_cprod;

  s_bal_unit_fp *pBU;
  s_mto_fp *pmto;
  double *output_FP, *output_MTO, *output_STOCK;
  double surf_cp, surf_fp, prct;
  char *name;

  nb_cprod = pcarac_fp->nb_cprod;
  p_cprod = pcarac_fp->p_cprod;
  // LP_printf(fpout,"--> SURFACE MASS BALANCE : \n"); //BL to debug
#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_fp->nb_cprod, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_cprod, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (i = 0; i < nb_cprod; i++) {
      FP_calc_watbal_cprod(p_cprod[i], pcarac_fp, dt, t, fpout);
    }
#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief Launches transport fluxes (heat, solute) spatial agregation from balance
 * unit to cell prod scale (for all cell prods).
 * Accounts for parallelized calculations. Parent function launched from main_cawaqs.c
 * @return None
 */
void FP_calc_transport_fluxes_cprod_all(s_carac_fp *pcarac_fp, int nb_species, int *spetypes, double dt, double t, FILE *fpout) {
  int i;
  int nb_cprod = pcarac_fp->nb_cprod;
  s_cprod_fp **tab_cprod = pcarac_fp->p_cprod;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(fpout, nb_cprod, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(tab_cprod, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (i = 0; i < nb_cprod; i++) {
      FP_calc_transport_fluxes_cprod(tab_cprod[i], pcarac_fp, nb_species, spetypes, dt, t, fpout);
    }
#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief Launches surface water balance spatial agregation from balance
 * unit to cell prod scale (for a single cell prod). Accounts for parallelized calculations.
 * @return None
 */
void FP_calc_watbal_cprod(s_cprod_fp *cprod, s_carac_fp *pcarac_fp, double dt, double t, FILE *fpout) {
  int j;
  s_id_spa *link, *link_mto;
  int nb_cprod;
  s_bal_unit_fp *pBU;
  s_mto_fp *pmto;
  double *output_FP, *output_MTO, *output_STOCK;
  double surf_cp, surf_fp, prct;
  char *name;

  link = cprod->link[BU_FP];
  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
  } else {
    LP_error(fpout, "link NULL in function FP_calc_watbal_cprod");
  }

  surf_cp = cprod->area;
  RSV_reinit_wat_bal(cprod->pwat_bal);
  FP_reinit_mto_cprod(cprod->mto);
  FP_reinit_stock_cprod(cprod->stock);
  while (link != NULL) {
    pBU = pcarac_fp->p_bu[link->id];
    pmto = pBU->pmto;
    surf_fp = pBU->area;
    prct = link->prct;
    output_FP = SPA_calcul_values(CPROD_SPA, pBU, prct, 0., 0., fpout);
    output_MTO = SPA_calcul_values(MTO_SPA, pmto, prct, t, dt, fpout);
    output_STOCK = SPA_calcul_values(STOCK_SPA, pBU, prct, 0., 0., fpout);

    for (j = 0; j < RSV_DIRECT; j++) {
#ifdef DEBUG
      if (isnan(output_FP[j]) || isinf(output_FP[j]))
        LP_error(fpout, "while calculating %s fluxes anormal value produced at cprod %d\n", RSV_name_res(j), cprod->id[FP_GIS]);
#endif
      cprod->pwat_bal->q[j] += output_FP[j];
    }
    for (j = 0; j < FP_NMETEO; j++) {
      cprod->mto[j] += output_MTO[j];
#ifdef DEBUG
      if (isnan(output_MTO[j]) || isinf(output_MTO[j]))
        LP_error(fpout, "while calculating %s fluxes anormal value produced at cprod %d\n", FP_name_mto(j), cprod->id[FP_GIS]);
#endif
    }
    for (j = 0; j < FP_NSTCK; j++) {
      cprod->stock[j] += output_STOCK[j];
    }
    link = link->next;

    free(output_FP);
    free(output_MTO);
    free(output_STOCK);
  }

  link = cprod->link[NO_AQ_FP];
  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
    while (link != NULL) {
      pBU = pcarac_fp->p_bu[link->id];
      prct = link->prct;
      output_FP = SPA_calcul_values(CPROD_SOUT_SPA, pBU, prct, 0., 0., fpout);

      cprod->pwat_bal->q[RSV_DIRECT] += output_FP[0];
      cprod->pwat_bal->q[RSV_INFILT] -= output_FP[0];

      link = link->next;
      free(output_FP);
    }
  }
  for (j = 0; j < FP_NMETEO; j++) {
    cprod->mto[j] = RSV_mm_to_q(cprod->mto[j], surf_cp, dt, DAY_TS, fpout);
  }
  for (j = 0; j < RSV_NTYPE; j++) {
    cprod->pwat_bal->q[j] = RSV_mm_to_q(cprod->pwat_bal->q[j], surf_cp, dt, DAY_TS, fpout);
  }

  for (j = 0; j < FP_NSTCK; j++) {
    cprod->stock[j] = RSV_mm_to_q(cprod->stock[j], surf_cp, dt, DAY_TS, fpout);
  }
}

/**
* @brief TBD
* @return TBD
* @internal NG : 18/02/2020 : "type_link" is added so this function can be used in input.y both for the BU/CPROD
surface link AND the NSAT/CPROD link for the hyperdermic module @endinternal
*/
s_cprod_fp *FP_check_cprod(s_cprod_fp *one_cp, s_cprod_fp *cp_list, int type_link, FILE *fpout) {

  s_cprod_fp *cp_tmp;
  int id_cp, check_id = -1;
  s_id_spa *link;

  id_cp = one_cp->id[FP_GIS];
  if (id_cp == cp_list->id[FP_GIS]) {
    cp_tmp = NULL;
    link = SPA_create_ids(one_cp->link[type_link]->id, one_cp->link[type_link]->prct);
    cp_list->link[type_link] = SPA_secured_chain_id_fwd(cp_list->link[type_link], link);
    cp_list->area += one_cp->link[type_link]->prct;
    one_cp = FP_free_cprod(one_cp, fpout);
  } else {
    cp_list = (s_cprod_fp *)SPA_browse_all_struct(CPROD_SPA, cp_list, END_FP);
    if (cp_list == NULL) {
      LP_error(fpout, "NULL pointer returned by the SPA_browse_all_struct() function !\n");
    }

    while (cp_list->prev != NULL) {
      check_id = cp_list->id[FP_GIS];
      if (check_id == id_cp) {
        break;
      }
      cp_list = cp_list->prev;
    }
    if (cp_list->prev != NULL) {
      cp_tmp = NULL;
      link = SPA_create_ids(one_cp->link[type_link]->id, one_cp->link[type_link]->prct);
      cp_list->link[type_link] = SPA_secured_chain_id_fwd(cp_list->link[type_link], link);
      cp_list->area += one_cp->link[type_link]->prct;
      cp_list = (s_cprod_fp *)SPA_browse_all_struct(CPROD_SPA, cp_list, BEGINNING_FP);
      one_cp = FP_free_cprod(one_cp, fpout);
    } else {
      cp_tmp = one_cp;
    }
  }
  return cp_tmp;
}

/**
 * @brief Prints Cell prod links attributes with some other geometry units
 * @return None
 */
void FP_print_cprod(s_cprod_fp *pcprod, int type_link, FILE *fpout) {

  s_cprod_fp *pcptmp;
  pcptmp = (s_cprod_fp *)SPA_browse_all_struct(CPROD_SPA, pcprod, BEGINNING_FP);
  while (pcptmp != NULL) {
    LP_printf(fpout, " cell prod id %d GIS %d INTERN Area %f\n LINK : \n", pcptmp->id[FP_GIS], pcptmp->id[FP_INTERN], pcptmp->area);
    SPA_print_ids(pcptmp->link[type_link], fpout);
    pcptmp = pcptmp->next;
  }
}

/**
 * @brief TBD
 * @return None
 */
void FP_finalize_link_cp(s_carac_fp *pcarac_fp, int type_link, FILE *fpout) {
  int nb_cprod, i;
  double area_cp;
  s_cprod_fp *pcp;
  s_id_spa *link;
  nb_cprod = pcarac_fp->nb_cprod;
  for (i = 0; i < nb_cprod; i++) {
    pcp = pcarac_fp->p_cprod[i];
    area_cp = pcp->area;
    if (pcp->link[type_link] != NULL) {
      link = SPA_divide_ids(pcp->link[type_link], area_cp);
      pcp->link[type_link] = SPA_free_ids(pcp->link[type_link], fpout);
      pcp->link[type_link] = link;
    }
  }
}

/**
 * @brief Resets all input climate values for a cell_prod
 * @return None
 */
void FP_reinit_mto_cprod(double *mto) {
  int j;
  for (j = 0; j < FP_NMETEO; j++) {
    mto[j] = 0;
  }
}

/**
 * @brief Resets all water storage terms for a cell_prod
 * @return None
 */
void FP_reinit_stock_cprod(double *stock) {
  int j;
  for (j = 0; j < FP_NSTCK; j++) {
    stock[j] = 0;
  }
}

/**
 * @brief TBD
 * @return TBD
 * @internal Dummy return value (nb_cprod !?)
 */
int FP_test_browse_spa(s_carac_fp *pcarac_fp, FILE *fpout) {

  int nb_cprod, i, j;
  double area_cp;
  s_cprod_fp *pcp;

  nb_cprod = pcarac_fp->nb_cprod;

  for (i = 0; i < nb_cprod; i++) {
    pcp = pcarac_fp->p_cprod[i];
    if (pcp->link[BU_FP] != NULL) {

      pcp->link[BU_FP] = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcp->link[BU_FP], END_SPA);
      pcp->link[BU_FP] = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcp->link[BU_FP], BEGINNING_SPA);
    }
  }
  return nb_cprod;
}

/**
 * @brief Searches for a given s_cprod_fp* pointer and returns a given ID type (ABS, INT, GIS)
 * @return Cell prod ID if found.
 */
int FP_search_id_cprod(s_carac_fp *pchar_fp, int id2find, int id_int_type, int id_out_type, FILE *fp) {

  int ncprod, i, id_out;

  ncprod = pchar_fp->nb_cprod;
  id_out = CODE;
  for (i = 0; i < ncprod; i++) {
    if (fabs(pchar_fp->p_cprod[i]->id[id_int_type] - id2find) < EPS) {
      id_out = pchar_fp->p_cprod[i]->id[id_out_type];
      break;
    }
  }
  return id_out;
}

/**
 * @brief Looks for a cell_prod pointer based on a ID.
 * @return Corresponding s_cprod_fp* pointer if found.
 */
s_cprod_fp *FP_get_cprod(s_carac_fp *pcarac_fp, int id_cprod, int id_type, FILE *fpout) {

  s_cprod_fp *pcprod;
  int i = 0;
  int nb_cprod;
  int verif_id = -1;

  if (id_type == FP_INTERN || id_type == FP_ABS) {
    pcprod = pcarac_fp->p_cprod[id_cprod];
  } else { // Assuming id_type is GIS_ID
    nb_cprod = pcarac_fp->nb_cprod;

    if (id_cprod < nb_cprod && id_cprod > 0) {
      pcprod = pcarac_fp->p_cprod[id_cprod];
      verif_id = pcprod->id[id_type];
    }

    if (verif_id != id_cprod) {

      while (verif_id != id_cprod) {
        if (i == nb_cprod) {
          i++;
          break;
        }
        pcprod = pcarac_fp->p_cprod[i];
        verif_id = pcprod->id[id_type];
        i++;
      }
      if (i > nb_cprod) {
        LP_error(fpout, " No cprod cell with GIS id : %d check your input file \n", id_cprod);
      }
    }
  }
  return pcprod;
}

/**
 * @brief Adds a new link to a cell prod
 * @return None
 */
void FP_init_cprod(s_cprod_fp *pcprod, int id, double prct, int link_type, FILE *fpout) {

  s_id_spa *link_tmp;

  link_tmp = SPA_create_ids(id, prct);
  pcprod->link[link_type] = SPA_secured_chain_id_fwd(pcprod->link[link_type], link_tmp);
}

/**
 * @brief For all cell prods, resets the outgoing discharge of a given type
 * of water balance reservoir.
 * @return None
 */
void FP_reinit_q_cprod(s_carac_fp *pcarac_fp, int type_rsv, FILE *fpout) {

  int i, nb_cprod;
  s_cprod_fp *pcprod;

  nb_cprod = pcarac_fp->nb_cprod;
  for (i = 0; i < nb_cprod; i++) {
    pcprod = pcarac_fp->p_cprod[i];
    pcprod->pwat_bal->q[type_rsv] = 0;
  }
}

/**
 * @brief Adds a link of NO_AQ type to a cell_prod
 * @return None
 */
void FP_create_noaq_cprod(s_carac_fp *pcarac_fp, int id_cprod, int id_bu, double area, FILE *fp) {

  s_cprod_fp *pcprod;
  double area_cprod, prct;
  s_id_spa *id_tmp;
  pcprod = FP_get_cprod(pcarac_fp, id_cprod, FP_GIS, fp);
  area_cprod = pcprod->area;
  prct = area / area_cprod;
  id_tmp = SPA_create_ids(id_bu, prct);
  pcprod->link[NO_AQ_FP] = SPA_secured_chain_id_fwd(pcprod->link[NO_AQ_FP], id_tmp);
  if (pcprod->prsv == NULL) {
    pcprod->prsv = RSV_create_reservoir(1000, 1, 0);
  }
}

/**
 * @brief Consistency surface check between watbal unit areas and cell prod areas to ensure
 * no run off flow is lost.
 * @return None
 */
void FP_check_ruiss_surf(s_carac_fp *pcarac_fp, FILE *fp) {

  s_cprod_fp *pcprod;
  int nb_cprod, nb_BU, i, count = 0, length;
  double *check_ruis;
  s_id_io **correct_ruiss;
  s_id_io *pid_tmp;
  double area_bu, prct;
  s_id_spa *link;

  nb_BU = pcarac_fp->nb_BU;
  check_ruis = (double *)calloc(nb_BU, sizeof(double));
  correct_ruiss = (s_id_io **)malloc(nb_BU * sizeof(s_id_io *));
  bzero((char *)correct_ruiss, nb_BU * sizeof(s_id_io *));
  nb_cprod = pcarac_fp->nb_cprod;
  for (i = 0; i < nb_cprod; i++) {
    pcprod = pcarac_fp->p_cprod[i];
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcprod->link[BU_FP], BEGINNING_SPA);
    while (link != NULL) {
      check_ruis[link->id] += link->prct * pcprod->area;
      pid_tmp = IO_create_id(0, i);
      correct_ruiss[link->id] = IO_secured_chain_fwd_id(correct_ruiss[link->id], pid_tmp);
      link = link->next;
    }
  }
  for (i = 0; i < nb_BU; i++) {
    area_bu = pcarac_fp->p_bu[i]->area;
    prct = (fabs(check_ruis[i] - area_bu) / area_bu) * 100;

    if (prct > 0.2 && prct < 1) {

      pid_tmp = IO_browse_id(correct_ruiss[i], BEGINNING_IO);
      length = IO_length_id(pid_tmp);
      SPA_correct_intersect(CPROD_SPA, check_ruis[i], area_bu, correct_ruiss[i], pcarac_fp, length, BU_FP, i, fp);

      LP_warning(fp, "Mass conservation issue %f prct of surface water is lost for surface water balance calculation unit %d\n", prct, i + 1);
    } else if (prct > 1) {
      if (area_bu > 0.5) {
        LP_warning(fp, "Mass conservation issue %f prct of surface water is lost for surface water balance calculation unit %d : surface water balance unit area %f surface recalculated from cprod %f", prct, i + 1, area_bu, check_ruis[i]);
      }
      count++;
    }
  }
  free(check_ruis);
  correct_ruiss = IO_free_tab_id(correct_ruiss, nb_BU, fp);

  if (count > 0) {
    LP_warning(fp, "BE CAREFUL ! %d surface geometry inconsistencies have been detected which might lead to mass balance errors and water flux losses !!\n", count);
  }
}

/**
 * @brief Computes transport fluxes at the CPROD scale, for one CPROD cell for all species.
 * Functions does account for the specie type (HEAT and SOLUTE).
 * @return None
 */
void FP_calc_transport_fluxes_cprod(s_cprod_fp *pcprod, s_carac_fp *pcarac_fp, int nb_species, int *spetypes, double dt, double t, FILE *fpout) {
  int i, j, p;
  s_id_spa *link;
  double prct;
  s_bal_unit_fp *pbu;
  double *output_flux;
  double *input_flux;
  s_wat_bal_rsv *pwatbal = pcprod->pwat_bal;

  for (p = 0; p < nb_species; p++) {
    RSV_reinit_matter_fluxes_bal(pcprod->pwat_bal, p); // trflux and trvar resets to zero. Hydro has been reset in FP_calc_watbal()
    FP_reinit_input_mflux(pcprod->input_mflux, p);     // Input flux array reset
  }

  link = pcprod->link[BU_FP];
  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
  } else {
    LP_error(fpout, "libfp%4.2f : File %s, function %s at line %d : Link pointer towards bal_unit is NULL for CPROD cell GIS id %d.\n", VERSION_FP, __FILE__, __func__, __LINE__, pcprod->id[FP_GIS]);
  }

  while (link != NULL) {
    pbu = pcarac_fp->p_bu[link->id];
    prct = link->prct;

    for (p = 0; p < nb_species; p++) {
      output_flux = SPA_calcul_transport_fluxes(CPROD_SPA, pbu, p, prct, 0., 0., fpout);
      input_flux = SPA_calcul_transport_fluxes(STOCK_SPA, pbu, p, prct, 0., 0., fpout);
      pcprod->pwat_bal->trflux[p][RSV_INFILT] += output_flux[FP_MINF]; // Either matter flux (in g/m2) or energy flux (in W/m2)
      pcprod->pwat_bal->trflux[p][RSV_RUIS] += output_flux[FP_MRUIS];  // Either matter flux (in g/m2) or energy flux (in W/m2)
      pcprod->input_mflux[p][FP_MFORCED] += input_flux[FP_MFORCED];    // Either matter flux (in g/m2) or energy flux (in W/m2)
      pcprod->input_mflux[p][FP_MBOUND] += input_flux[FP_MBOUND];      // Either matter flux (in g/m2) or energy flux (in W/m2)
      free(output_flux);
      free(input_flux);
    }
    link = link->next;
  }

  // Routing of a fraction of the infiltration flux to the hydraulic network in case no aquifer system is declared :
  link = pcprod->link[NO_AQ_FP];
  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
    while (link != NULL) {
      pbu = pcarac_fp->p_bu[link->id];
      prct = link->prct;

      for (p = 0; p < nb_species; p++) {
        output_flux = SPA_calcul_transport_fluxes(CPROD_SOUT_SPA, pbu, p, prct, 0., 0., fpout);
        pcprod->pwat_bal->trflux[p][RSV_INFILT] -= output_flux[FP_MINF];
        pcprod->pwat_bal->trflux[p][RSV_DIRECT] += output_flux[FP_MINF];
        free(output_flux);
      }
      link = link->next;
    }
  }

  // Filling watbal structure depending on the specie type
  for (p = 0; p < nb_species; p++) {

    switch (spetypes[p]) {
    case FP_HEAT: {

      if (pwatbal->q[RSV_INFILT] > EPS_FP) {
        pwatbal->trvar[p][RSV_INFILT] = (pwatbal->trflux[p][RSV_INFILT] * pcprod->area) / (pwatbal->q[RSV_INFILT] * RHO_WATER * CP_WATER);
      }
      if (pwatbal->q[RSV_RUIS] > EPS_FP) {
        pwatbal->trvar[p][RSV_RUIS] = (pwatbal->trflux[p][RSV_RUIS] * pcprod->area) / (pwatbal->q[RSV_RUIS] * RHO_WATER * CP_WATER);
      }
      if (pwatbal->q[RSV_DIRECT] > EPS_FP) {
        pwatbal->trvar[p][RSV_DIRECT] = (pwatbal->trflux[p][RSV_DIRECT] * pcprod->area) / (pwatbal->q[RSV_DIRECT] * RHO_WATER * CP_WATER);
      }
    } break;
    case FP_SOLUTE: {

      if (pwatbal->q[RSV_INFILT] > EPS_FP) {
        pwatbal->trvar[p][RSV_INFILT] = (pwatbal->trflux[p][RSV_INFILT] * pcprod->area) / (pwatbal->q[RSV_INFILT] * dt * TS_unit2sec(DAY_TS, fpout));
      }
      if (pwatbal->q[RSV_RUIS] > EPS_FP) {
        pwatbal->trvar[p][RSV_RUIS] = (pwatbal->trflux[p][RSV_RUIS] * pcprod->area) / (pwatbal->q[RSV_RUIS] * dt * TS_unit2sec(DAY_TS, fpout));
      }
      if (pwatbal->q[RSV_DIRECT] > EPS_FP) {
        pwatbal->trvar[p][RSV_DIRECT] = (pwatbal->trflux[p][RSV_DIRECT] * pcprod->area) / (pwatbal->q[RSV_DIRECT] * dt * TS_unit2sec(DAY_TS, fpout));
      }
    } break;
    default:
      LP_error(fpout, "In libfp%4.2f, File %s in %s line %d : Unknown transport specie type\n", VERSION_FP, __FILE__, __func__, __LINE__); // Should never happen/ verified above but, still...
      break;
    }
  }
}

/**
 * @brief General function launching memory management for watbal type transport
 * attributes for a cprod cell and all species.
 * @return None
 */
void FP_transport_manage_memory_watbal_cprod(s_cprod_fp *pcprod, int nb_species, FILE *flog) // nb_species = total number of species
{
  RSV_manage_memory_transport(pcprod->pwat_bal, nb_species, flog);
  FP_manage_memory_input_mflux_cprod(pcprod, nb_species, flog);
}

/**
 * @brief (Re)-allocates *input_mflux according to the total number of species declared
 * @return -
 */
void FP_manage_memory_input_mflux_cprod(s_cprod_fp *pcprod, int nb_species, FILE *flog) // nb_species = total number of species
{
  int i, p;

  if (pcprod->input_mflux != NULL) {
    LP_error(flog, "libfp%4.2f : Error in file %s, function %s at line %d. Memory has already been allocated.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  }

  pcprod->input_mflux = (double **)malloc(nb_species * sizeof(double *));
  if (pcprod->input_mflux == NULL)
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory allocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  for (p = 0; p < nb_species; p++) {
    pcprod->input_mflux[p] = (double *)malloc(FP_FIN_TYPE * sizeof(double));
    for (i = 0; i < FP_FIN_TYPE; i++)
      pcprod->input_mflux[p][i] = 0.;
  }
}

/**
 * @brief Reinitializes transport reservoir components (trflux, trvar), for all CPROD cells,
 * for a given type of reseroivrand for one given specie.
 * @return None
 */
void FP_reinit_transp_rsv_cprod(s_carac_fp *pcarac_fp, int type_rsv, int id_species, FILE *fpout) {

  int i, nb_cprod = pcarac_fp->nb_cprod;
  s_cprod_fp *pcprod;

  for (i = 0; i < nb_cprod; i++) {
    pcprod = pcarac_fp->p_cprod[i];
    pcprod->pwat_bal->trvar[id_species][type_rsv] = 0.;
    pcprod->pwat_bal->trflux[id_species][type_rsv] = 0.;
  }
}
