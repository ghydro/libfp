/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: struct_fp.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        struct_fp.h
 * @brief       List of libfp structures
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <time.h>

typedef struct wat_bal_calc_unit s_bal_unit_fp;
typedef struct meteo s_mto_fp;
typedef struct param_fp s_param_fp;
typedef struct cell_prod s_cprod_fp;
typedef struct carac_fp s_carac_fp;
typedef struct input_fp s_input_fp;
typedef struct catchment_fp s_catchment_fp;
typedef struct specie_unit s_specie_unit_fp;
typedef struct forcing_boundary s_forcing_bound_fp;

/**
 * @struct param_fp
 * @brief Production-function (FP) structure
 */
struct param_fp {
  /*!< FP id */
  int id;
  /*!< FP name */
  char name[STRING_LENGTH_LP];
  /*!< FP parameter set */
  double param[FP_NPRODUCTION];
  /*!< FP-related LAI (leaf area index) time serie */ // NG : 26/11/2019
  s_ft *p_ft;
  /*!< Pointer to the next param_FP */
  s_param_fp *next;
  /*!< Pointer to the previous param_FP */
  s_param_fp *prev;
  /*!< YES/NO boolean to turn on/off rerouting option for inf bucket overflow to runoff bucket. Set to NO by default */ // NG : 05/12/2023
  int io_inf_to_runoff;
};

/**
 * @struct wat_bal_calc_unit
 * @brief A water balance unit is defined as the intersection area between a
 * production function and the MTO grid, to define the input water volume.
 */
struct wat_bal_calc_unit {
  /*!< Water balance calculation unit ID */
  int id;
  /*!< Pointer to the FP parameter set */
  s_param_fp *param_fp;
  /*!< Pointer to the MTO structure (values in mm) */
  s_mto_fp *pmto;
  /*!< Area of the water balance calculation unit (values in m2) */
  double area;
  /*!< pointer to the water balance structure (pwat_bal->q is in mm !)*/
  s_wat_bal_rsv *pwat_bal;
  /*!< Pointer to the volume of water stocked in SOIL INFILT and RUIS bucket (values in mm) */
  double stock[FP_NSTCK];
  /*!< Pointer to the next water balance calculation unit */
  s_bal_unit_fp *next;
  /*!< Pointer to the previous water balance calculation unit */
  s_bal_unit_fp *prev;

  // Transport-related attributes
  /*!< table (over n species) of id_spa pointer chains : For each species,
  one 'wat_bal_calc_unit' can be linked with several specie grid cells */
  s_id_spa **link;
  /*!< table (over n species) of table in [FP_NVAR_INPUT_TRANSPORT] of pbound pointers :
  For each species, one 'wat_bal_calc_unit' can be submitted to some imposed boundaries */
  s_forcing_bound_fp ***pbound;
  /*!< table (over n species) of table in [FP_FIN_TYPE] to store the different
  matter input fluxes. Used as a shortcut to write the input terms of the the matter mass balance */
  double **input_mflux;
};

/**
 * @struct forcing_boundary
 * @brief Manages boundary inputs associated with water balance units which
 * are not (or not entirely) covered by a forcing specie grid.
 */
struct forcing_boundary {
  /*!< Boundary frequency in [FP_NFREQ]. Set to FP_CST (constant) by default */
  int freq;
  /*!< Time serie associated with the forcing boundary */
  s_ft *pft;
};

/**
 * @struct specie_unit
 * @brief A specie unit is defined by a cell in a specie grid (transport external forcing grid)
 */
struct specie_unit {
  /*!< specie unit ID in [FP_INTERN,FP_GIS,FP_ABS] */
  int id[FP_NDID];
  /*!< Specie unit area (in m2) */
  double area;
  /*!< Table in [FP_NVAR_INPUT_TRANSPORT] of temporal forcing values */
  s_ft **pft;
  /*!< Pointer to the next specie unit */
  s_specie_unit_fp *next;
  /*!< Pointer to the previous specie unit */
  s_specie_unit_fp *prev;
};

/**
 * @struct cell_prod
 * @brief Elementary watershed linked (1-on-1 link) with a river reach. Subsurface runoff produced
 * is gathered at this scale to be routed toward the associated river reach
 */
struct cell_prod {
  /*!< Surface cell ID in [FP_INTERN,FP_GIS,FP_ABS] */
  int id[FP_NDID];
  /*!< Surface cell area (in m2) */
  double area;
  /*!< Water balance pointer of the surface cell (pwat_wal->q in m3/s)*/
  s_wat_bal_rsv *pwat_bal;
  /*!< Tabular to stock the mto values over the CPROD (in m3/s) */
  double mto[FP_NMETEO];
  /*!< Tabular to stock the stocked water value in RUIS and INFILT bucket over the CPROD (in m3/s) */
  double stock[FP_NSTCK];
  /*!< Link pointer of the surface cell. This is a tubular of pointer of NLINK dimension.
  One surface cell is linked with several wat_bal_calc_unit and to a river reach */
  s_id_spa **link;
  /*!< Reservoir to route infiltrated water to river system when no aquifer are defined */
  s_reservoir_rsv *prsv;
  /*!< Pointer to the next surface cell */
  s_cprod_fp *prev;
  /*!< Pointer to the previous surface cell */
  s_cprod_fp *next;
  /*!< Indicator to define if the CPROD cell is a chasm or not. Values in
  [FP_CATCH_RIV,FP_CATCH_CHASM] - Set to FP_CATCH_RIV by default */
  int cell_prod_type;
  s_catchment_fp *p_catch; /*!< Pointer towards the catchment the CPROD cell is part of */

  // Transport-related attributes
  /*!< table (over n species) of table in [FP_FIN_TYPE] to store the different
  matter input fluxes. Used as a shortcut to write the input terms of the the matter mass balance */
  double **input_mflux;
};

/**
 * @struct catchment_fp
 * @brief The "catchment" object defines a sub-basin within the surface model area.
 * A catchment is defined as a pre-determined set of CPROD cells (cell_prod).
 */
struct catchment_fp {
  /*!< GIS ID of the catchment */
  int id_gis;
  /*!< Catchment name */
  char *catchment_name;
  /*!< Number of CPROD within the catchment */
  int nb_cprod_catchment;
  /*!< Defines if a catchment if a chasm or not. Values in [FP_CATCH_RIV,FP_CATCH_CHASM]. Set to FP_CATCH_RIV by default. */
  int catchment_type;
  /*!< Catchment surface (in m2) */
  double area;
  /*!< Concentration time associated with the catchment - Set to TC_DEFAULT_FP by default */
  double concentration_time;
  /*!< Concentration time calculated for the catchment when using the
  FORMULA method to define K musk - Set to TC_DEFAULT_FP by default */
  double Tc_Formula;
  /*!< Tabular of pointers towards CPROD cells within the catchment */
  s_cprod_fp **cprod_catchment;
  /*!< Pointer towards the CPROD at outlet of the catchment */
  s_cprod_fp *pcprod_outlet;
  /*!< Value of runoff counted as infiltration towards a NSAT unit : either river discharge at
  the outlet of the catchment (if catchment_type is CHASM and pcprod_outlet->type = RIV) or runoff
  (if catchment_type is CHASM and pcprod_outlet->type = CHASM). Q_chasm is calculated in m3/s. */
  double Q_chasm;
  /*!< Pointer to the previous catchment */
  s_catchment_fp *prev;
  /*!< Pointer to the next catchment */
  s_catchment_fp *next;
  /*!< Catchment maximum relative transfer time index */
  double Itr_max;
  /*!< Infiltration coefficient [0;1] which allows to infiltrate only a given fraction of the chasm
  discharge (either river outlet or subsurface run-off) towards a NSAT unit. Set to CHASM_INF_DEFAULT by default */
  double infil_coef;
};

/**
 * @struct input_fp
 * @brief Libfp input files attributes (prefix, format, paths, etc.)
 */
struct input_fp {
  /*!< Number of MTO cell of the simulation */
  int nb_mto;
  /*!< Format of inputs*/
  int format;
  /*!< Tabular of pointer to the rainfall and pet values */
  s_mto_fp **mto;
  /*!< Prefix of MTO input files */
  char **prefix;
  /*!< Format of LAI input files */
  int format_lai;
  /*!< Prefix of LAI input files */
  char *prefix_lai;
  /*!< Paths leading to the folders containing FP binary input files. Table of char* over FP_NPATHS. */
  char **input_paths;

  // Transport-related attributes
  /*!< Parameter in [FP_NFORCING_TRANSPORT] defining the forcing conditions used for all species */
  int *transport_forcing_conditions;
  /*!< Format in [NFORMAT_IO] of forcing inputs for each species */
  int *transport_input_format;
  /*!< Paths leading to the folders containing FP binary input files. Table of char* over
  the number of species */
  char **transport_paths;
  /*!< Prefix of forcings inputs in [FP_NVAR_INPUT_TRANSPORT] for all species */
  char ***transport_input_prefixes;
  /*!< Multiplication factor applied to the forcing time series. Table in [FP_NVAR_INPUT_TRANSPORT] for all species */
  double **forcing_unit;
};

/**
 * @struct meteo
 * @brief climate data cell (MTO) attributes
 */
struct meteo {
  /*!< MTO cell ID in [INTERN_FP,GIS_FP] */
  int id[FP_NDID];
  /*!< Temporal function of rainfall and PET values */
  s_ft **p_ft;
  /*!< MTO cell area (in m2) */
  double area;
  /*!< Link of the MTO cell with wat_bal_calc_units */
  s_id_spa *link;
  /*!< Pointer to the next MTO cell */
  s_mto_fp *next;
  /*!< Pointer to the previous MTO cell */
  s_mto_fp *prev;
};

/**
 * @struct carac_fp
 * @brief Parent structure of the library. Allows to reach any objects.
 */
struct carac_fp {
  /*!< Number of FP of the simulation */
  int nb_FP;
  /*!< Number of water balance calculation units of the simulation */
  int nb_BU;
  /*!< Number of surface cell of the simulation */
  int nb_cprod;
  /*!< Answer-type integer if LAI data is read or not, set by default to NO */
  int input_lai;
  /*!< Pointer to surface inputs */
  s_input_fp *pinputs;
  /*!< Tabular of pointer to the FP parameters */
  s_param_fp **p_param_fp;
  /*!< Tabular of pointer to the water balance calculation units */
  s_bal_unit_fp **p_bu;
  /*!< Tabular of pointer to the surface cells */
  s_cprod_fp **p_cprod;

#ifdef OMP
  s_smp *psmp; /*!< Pointer to the libpc share_mem_paral structure for parallelized calculations */
#endif

  /*!< Number of catchments over the entire surface domain */
  int nb_catchment;
  /*!< Catchments pointer chain */
  s_catchment_fp *p_catchment;
  /*!< General default concentration_time for all catchments if left
  undifferentiated for each catchment. Set by default to TC_DEFAULT_FP */
  double default_concentration_time;
  /*!< Threshold catchment surface value used to automatically calibrate concentration
  times for HDERM catchments. Set by default to SMOY_THRESH_TC */
  double thresh_mean_surf;
  /*!< Tabular of parameter values used to determine Tc's in case of
  HDERM catchments. All values set to 0 by default */
  double param_tc[FP_TC_PARAMHDERM];
  /*!< Tabular of integer of the number of specie cell for each specie */
  int *nb_specie_unit;
  /*!< Tabular (over n species) of tabular of pointer to the specie units */
  s_specie_unit_fp ***p_species_unit;
};

#define new_bal_unit() ((s_bal_unit_fp *)malloc(sizeof(s_bal_unit_fp)))
#define new_meteo() ((s_mto_fp *)malloc(sizeof(s_mto_fp)))
#define new_param_fp() ((s_param_fp *)malloc(sizeof(s_param_fp)))
#define new_cprod() ((s_cprod_fp *)malloc(sizeof(s_cprod_fp)))
#define new_carac_fp() ((s_carac_fp *)malloc(sizeof(s_carac_fp)))
#define new_input_fp() ((s_input_fp *)malloc(sizeof(s_input_fp)))
#define new_catchment_fp() ((s_catchment_fp *)malloc(sizeof(s_catchment_fp)))
#define new_specie_unit() ((s_specie_unit_fp *)malloc(sizeof(s_specie_unit_fp)))
#define new_forcing_boundary() ((s_forcing_bound_fp *)malloc(sizeof(s_forcing_bound_fp)))