/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: param_fp.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        param_fp.h
 * @brief       libfp parameters and enumerations
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

/**
 * @def VERSION_FP
 * @brief Library version ID
 */
#define VERSION_FP 0.30

/**
 * @def MAX_RAIN_FP
 * @brief tbd
 */
#define MAX_RAIN_FP 2000

/**
 * @def NB_MB_VAL_FP
 * @brief Number of WATBAL output records per time-step (hydro calculations)
 */
#define NB_MB_VAL_FP 10

/**
 * @def NB_MB_VAL_TRANSPORT_FP
 * @brief Number of records in WATBAL_TRANSPORT output MB file for each species
 */
#define NB_MB_VAL_TRANSPORT_FP 12

/**
 * @def LCRT_DEF_FP
 * @brief Default values for LCRT parameter
 */
#define LCRT_DEF_FP 0.

/**
 * @def LDCR_DEF_FP
 * @brief Default values for LDCR parameter
 */
#define LDCR_DEF_FP 0.

/**
 * @def SMOY_THRESH_TC
 * @brief Default value for threshold surface value to calibrate hyperdermic catchment concentration times
 */
#define SMOY_THRESH_TC 0.

/**
 * @def TC_DEFAULT_FP
 * @brief Concentration time default value
 */
#define TC_DEFAULT_FP 86400.

/**
 * @def CHASM_INF_DEFAULT
 * @brief Default value for infiltration coefficient in a chasm configuration. Allows to only infiltrate a given fraction of the chasm discharge towdards NSAT
 */
#define CHASM_INF_DEFAULT 1.0

/**
 * @def EPSILON_SURF_FP
 * @brief Threshold used to verify coverage percentages between ele_bu and species cells
 */
#define EPSILON_SURF_FP 0.001

/**
 * @def EPS_VOL_FP
 * @brief Threshold water volume im m3/time step to compute a sub-root concentration. If below, concentration is set to 0.
 */
#define EPS_VOL_FP 0.0001

/**
 * @def RHO_WATER
 * @brief Water density in kg/m3
 */
#define RHO_WATER 1000.

/**
 * @def CP_WATER
 * @brief Specific heat capacity of liquid water in J/kg/K
 */
#define CP_WATER 4186.

// String replacement shortcuts
#define YES YES_TS
#define NO NO_TS
#define BEGINNING_FP BEGINNING_TS
#define END_FP FP_END
#define EPS_FP EPS_TS
#define CODE_FP CODE_TS
#define SMALL_FP SMALL_TS
#define BIG_FP BIG_TS
#define INBIN FP_INBIN
#define NSEC_DAY_FP NSEC_DAY_TS

/**
 * @enum type_param_prod
 * @brief Production-function calibration parameters
 */
enum type_param_prod { FP_DCRT, FP_CRT, FP_RMAX, FP_FN, FP_QRMAX, FP_CQR, FP_QIMAX, FP_CQI, FP_LCRT, FP_LDCR, FP_NPRODUCTION };

/**
 * @enum input_meteo
 * @brief Input climate data types
 */
enum input_meteo { FP_RAIN, FP_ETP, FP_NMETEO };

/**
 * @enum inputtype_paths
 * @brief libfp inputfiles dataset types
 */
enum inputtype_paths { FP_MTO, FP_LAI, FP_NPATHS };

/**
 * @enum parameters_tc_formula
 * @brief Input parameters used to automatically determine concentration times for
 * hyperdermic catchments (in case of user-selected Tc automatic calculation).
 */
enum parameters_tc_formula { FP_ALPHA, FP_BETA, FP_TC_PARAMHDERM };

/**
 * @enum id_FP
 * @brief IDs types for production-functions objects
 */
enum id_FP { FP_INTERN, FP_GIS, FP_ABS, FP_NDID };

/**
 * @enum bounds
 * @brief TBD
 */
enum bounds { FP_INIT, FP_END, FP_NBOUND };

/**
 * @enum link
 * @brief Link types for a given libfp object with some other geometry
 * (either from libfp or some other library)
 */
enum link { BU_FP, RIV_FP, AQ_FP, NO_AQ_FP, NSAT_HDERM_FP, NLINK };

/**
 * @enum catchment_type
 * @brief Defines the behavior of a catchment = if it is a chasm or not.
 */
enum catchment_type { FP_CATCH_RIV, FP_CATCH_CHASM, NCHASM_TYPE };

/**
 * @enum stock
 * @brief Storage term in differents buckets for surface water balance calculations (soil, runoff or infiltration).
 */
enum stock { FP_SOIL_STCK, FP_INFILT_STCK, FP_RUIS_STCK, FP_NSTCK };

/**
 * @enum transport_specie_type
 * @brief Transport specie type (Heat, Solute).
 * @warning Order has to be consistent with CaWaQS 'species_type' enumeration.
 */
enum transport_specie_type { FP_HEAT, FP_SOLUTE, FP_SPETYPE };

/**
 * @enum transport_forcing_type
 * @brief Forcing regimes for surface transport - see FP_transport_forcing_conditions() in itos.c for associated meanings
 */
enum transport_forcing_type { FP_TOTAL_TRFLUX, FP_DISSOCIATED_TRFLUX, FP_TOTAL_TRFLUX_WATER, FP_DISSOCIATED_TRFLUX_WATER, FP_TOTAL_TRVAR, FP_TOTAL_TRVAR_WATER, FP_DISSOCIATED_TRVAR_WATER, FP_NFORCING_TRANSPORT };

/**
 * @enum transport_var
 * @brief orcing variables for surface transport - see FP_transport_input_variable_name() in itos.c for associated meanings
 */
enum transport_var { FP_TRFLUX, FP_WATER, FP_TRVAR, FP_TRFLUX_INF, FP_TRFLUX_RUNOFF, FP_WATER_INF, FP_WATER_RUNOFF, FP_TRVAR_INF, FP_TRVAR_RUNOFF, FP_TRFLUX_DIRECT, FP_TRVAR_DIRECT, FP_NVAR_INPUT_TRANSPORT };

/**
 * @enum bound_frequency
 * @brief Transport boundary condition type (time-wise) : constant or variable with time
 */
enum bound_frequency { FP_CST, FP_VAR, FP_NFREQ };

/**
 * @enum type_output_flux
 * @brief Distinguishes output transport fluxes types (infiltrated, runoff) in mass balance
 */
enum type_output_flux { FP_MINF, FP_MRUIS, FP_FOUT_TYPE };

/**
 * @enum type_input_flux
 * @brief Distinguishes input matter fluxes types (forced = from external model, boundary = directly imposed in CaWaQS input files) in mass balance
 */
enum type_input_flux { FP_MFORCED, FP_MBOUND, FP_FIN_TYPE };

/**
 * @enum mb_output_terms
 * @brief Lists the records in order in the WATBAL mass balance output file
 */
enum mb_output_terms { FP_MB_RAIN, FP_MB_PET, FP_MB_ROFF, FP_MB_INF, FP_MB_AET, FP_MB_DSOUT, FP_MB_STOCK_ROFF, FP_MB_STOCK_SOIL, FP_MB_STOCK_INF, FP_MB_ERROR, FP_N_MBOUT };