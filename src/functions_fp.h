/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: functions_fp.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        functions_fp.h
 * @brief       Functions prototypes for libfp
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// in function manage_fp.c
s_param_fp *FP_create_param(int, char *);
void FP_fill_param(s_param_fp *, double, double, double, double, double, double, double, double, double, int);
s_param_fp *chain_param_fp(s_param_fp *, s_param_fp *);
s_param_fp *FP_secured_chain_param_fwd(s_param_fp *, s_param_fp *);
s_param_fp **FP_tab_param(s_param_fp *, int);
void FP_calc_wat_bal(s_bal_unit_fp *, double, double, FILE *, int); // NG : 26/11/2019 : Prototype modified to include optional lai
void FP_balance_distrib(s_bal_unit_fp *, FILE *);
void FP_runoff_infiltration(s_bal_unit_fp *, FILE *);
double *FP_get_mto(double, s_mto_fp *, FILE *);
double *FP_calc_mean_mto(double, double, s_mto_fp *, FILE *);
void FP_print_all_FP_param(s_carac_fp *, FILE *);
void FP_print_param(s_param_fp *, FILE *);
double FP_LAI_ponderation(double, double, double);                  // NG : 26/11/2019
double FP_get_LAI(double, s_ft *, FILE *);                          // NG : 26/11/2019
void FP_read_all_LAI(s_carac_fp *, int, FILE *);                    // NG : 26/11/2019
s_ft *FP_read_LAI_data(char *, char *, int, int, int, int, FILE *); // NG : 11/06/2021 : Folder data path added
s_ft *FP_read_LAI_year_bin(char *, FILE *, int, int, int, FILE *);  // NG : 26/11/2019

// in function manage_balance_unit.c
s_bal_unit_fp *chain_bu(s_bal_unit_fp *, s_bal_unit_fp *);
s_bal_unit_fp *FP_secured_chain_bu_fwd(s_bal_unit_fp *, s_bal_unit_fp *);
s_bal_unit_fp *FP_create_bal_unit(int);
s_bal_unit_fp **FP_tab_BU(s_bal_unit_fp *, int, FILE *);
s_bal_unit_fp *FP_define_bal_unit(s_carac_fp *, int, int, int, double, FILE *);
s_bal_unit_fp *FP_create_species_grid_link(s_bal_unit_fp *);           // NG : 08/07/2020 : Pointer initialization for wat_bal_calc_unit<-->specie grid cells
void FP_calc_wat_bal_BU(s_bal_unit_fp *, double, double, FILE *, int); // NG : 26/11/2019 : Function prototype modified to include optional LAI
void FP_print_BU(s_carac_fp *, FILE *);
s_bal_unit_fp *FP_check_is_ele_bu_defined(s_bal_unit_fp **, int, int, FILE *); // NG : 16/07/2020

// **** Transport-related functions ****
void FP_check_BU_species_links(s_carac_fp *, int, FILE *);
void FP_print_BU_species_links(s_bal_unit_fp *, int, FILE *);
void FP_transport_manage_memory_watbal_ele_bu(s_bal_unit_fp *, int, FILE *);
void FP_manage_memory_ele_bu_watbal_species(s_bal_unit_fp *, int, FILE *);
void FP_manage_memory_input_mflux_ele_bu(s_bal_unit_fp *, int, FILE *);
void FP_manage_memory_links_ele_bu_species(s_bal_unit_fp *, int, FILE *);
void FP_reinit_input_mflux(double **, int);

// SOLUTE : External forcing calculations using total matter flux only
void FP_transport_solute_total_trflux_one_BU(s_bal_unit_fp *, s_carac_fp *, double, double, int, double, double, FILE *);
// SOLUTE : External forcing calculations using total matter flux and water
void FP_transport_solute_total_trflux_water_one_BU(s_bal_unit_fp *, s_carac_fp *, double, double, int, double, double, FILE *);
// SOLUTE : External forcing calculations using dissociated infiltration and runoff fluxes for matter and water
void FP_transport_solute_dissociated_trflux_water_one_BU(s_bal_unit_fp *, s_carac_fp *, double, double, int, double, double, FILE *);
/* HEAT : External forcing calculations using dissociated heat fluxes and water
          WARNING ! Function do not account for partially covered surface extension. To be updated. */
void FP_transport_heat_dissociated_trvar_water_one_BU(s_bal_unit_fp *, s_carac_fp *, double, double, int, double, double, FILE *);

// in manage_catchment.c
s_catchment_fp *FP_create_catchment(int, int, s_carac_fp *);                        // NG : 19/01/2020
s_catchment_fp *FP_chain_catchment(s_catchment_fp *, s_catchment_fp *);             // NG : 19/01/2020
s_catchment_fp *FP_secured_chain_catchment_fwd(s_catchment_fp *, s_catchment_fp *); // NG : 19/01/2020
s_catchment_fp *FP_browse_catchment_chain(s_catchment_fp *, int, FILE *);           // NG : 23/01/2020
void FP_print_catchments_properties_from_chain(s_catchment_fp *, FILE *, int);      // NG : 23/01/2020
void FP_check_cprod_catchment_connexion(s_carac_fp *, FILE *);                      // NG : 23/01/2020
void FP_print_outlet_cprod_type(s_carac_fp *, FILE *);                              // NG : 23/01/2020
void FP_check_catchment_duplicate_ids(s_catchment_fp *, FILE *);                    // NG : 31/07/2020
void FP_calibrate_hderm_catchment_tc(s_catchment_fp *, s_carac_fp *, FILE *);       // NG : 27/02/2020

// in manage_specie_unit.c
s_specie_unit_fp *FP_create_specie_unit(int, int);                                            // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_secured_chain_specie_unit_fwd(s_specie_unit_fp *, s_specie_unit_fp *);   // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_browse_specie_unit_chain(s_specie_unit_fp *, int, FILE *);               // NG : 16/07/2020 Transport
s_specie_unit_fp **FP_tab_specie_unit(s_specie_unit_fp *, int, FILE *);                       // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_free_one_specie_unit(s_specie_unit_fp *, FILE *);                        // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_copy_one_specie_unit_pointer(s_specie_unit_fp *, FILE *);                // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_check_is_ele_specie_unit_defined(s_specie_unit_fp **, int, int, FILE *); // NG : 16/07/2020 Transport
s_specie_unit_fp *FP_free_specie_unit_chain(s_specie_unit_fp *, FILE *);                      // NG : 20/07/2020 Transport
int FP_count_specie_units_from_chain(s_specie_unit_fp *, FILE *);                             // NG : 16/07/2020 Transport
void FP_print_species_unit_tab(s_specie_unit_fp **, int, int, FILE *);                        // NG : 16/07/2020 Transport
void FP_print_species_unit_chain(s_specie_unit_fp *, FILE *);                                 // NG : 16/07/2020 Transport
void FP_read_all_transport_inputs(s_carac_fp *, int, int, FILE *);                            // NG : 19/07/2020 Transport
FILE *FP_check_exist_forcing_file(char *, char *, int, int, FILE *);                          // NG : 11/06/2021 Transport
s_ft *FP_read_bin_specie_unit(FILE *, char *, int, int, int, double, FILE *);                 // NG : 19/07/2020 Transport
double *FP_get_forcing_values(double, s_specie_unit_fp *, FILE *);                            // NG : 21/07/2020 Transport
void FP_check_duplicates_specie_unit(int, s_specie_unit_fp *, FILE *);                        // NG : 31/07/2020 Transport

// in manage_forcing_boundary.c
s_forcing_bound_fp *FP_create_forcing_boundary(void);                               // NG : 19/07/2020 Transport
void FP_manage_memory_boundary_ele_bu(s_bal_unit_fp *, int, FILE *);                // NG : 20/07/2020 Transport
void FP_define_forcing_boundary(s_carac_fp *, int, int, s_id_io *, s_ft *, FILE *); // NG : 20/07/2020 Transport
void FP_print_forcing_boundary_one_specie(s_carac_fp *, int, FILE *);               // NG : 20/07/2020 Transport
double FP_get_boundary_value(double, s_forcing_bound_fp ***, int, int, FILE *);     // NG : 22/07/2020 Transport

// in manage_carac_fp.c
s_carac_fp *FP_create_carac_fp();
void FP_calc_wat_bal_BU_all(s_carac_fp *, double, double, FILE *);
// **** Transport-related functions ****
void FP_manage_memory_carac_species(s_carac_fp *, int, FILE *);
void FP_calc_transport_all_surface_fluxes(s_carac_fp *, int, int *, double *, double *, double, double, FILE *);
void FP_manage_memory_rsv_transport(int, s_carac_fp *, FILE *);
void FP_transport_solute_total_trflux_mode(s_carac_fp *, double, double, int, double, double, FILE *);
void FP_transport_solute_total_trflux_water_mode(s_carac_fp *, double, double, int, double, double, FILE *);
void FP_transport_solute_dissociated_trflux_water_mode(s_carac_fp *, double, double, int, double, double, FILE *);
void FP_transport_heat_dissociated_trvar_water_mode(s_carac_fp *, double, double, int, double, double, FILE *);

// in manage_cprod.c
s_cprod_fp *FP_create_cprod(int, int);
s_cprod_fp *chain_cprod_fp(s_cprod_fp *, s_cprod_fp *);
s_cprod_fp *FP_secured_chain_cprod_fp(s_cprod_fp *, s_cprod_fp *);
s_cprod_fp **FP_tab_cprod(s_cprod_fp *, int, FILE *);
void FP_calc_watbal_cprod_all(s_carac_fp *, double, double, FILE *);
void FP_calc_watbal_cprod(s_cprod_fp *, s_carac_fp *, double, double, FILE *);
s_cprod_fp *FP_check_cprod(s_cprod_fp *, s_cprod_fp *, int, FILE *);
void FP_print_cprod(s_cprod_fp *, int, FILE *);
void FP_finalize_link_cp(s_carac_fp *, int, FILE *);
void FP_reinit_stock_cprod(double *);
void FP_reinit_mto_cprod(double *);
s_cprod_fp *FP_free_cprod(s_cprod_fp *, FILE *);
int FP_search_id_cprod(s_carac_fp *, int, int, int, FILE *);
s_cprod_fp *FP_get_cprod(s_carac_fp *, int, int, FILE *);
void FP_init_cprod(s_cprod_fp *, int, double, int, FILE *);
void FP_reinit_q_cprod(s_carac_fp *, int, FILE *);
void FP_create_noaq_cprod(s_carac_fp *, int, int, double, FILE *);
void FP_check_ruiss_surf(s_carac_fp *, FILE *);
// **** Transport-related functions ****
void FP_calc_transport_fluxes_cprod_all(s_carac_fp *, int, int *, double, double, FILE *);
void FP_calc_transport_fluxes_cprod(s_cprod_fp *, s_carac_fp *, int, int *, double, double, FILE *);
void FP_manage_memory_input_mflux_cprod(s_cprod_fp *, int, FILE *);
void FP_transport_manage_memory_watbal_cprod(s_cprod_fp *, int, FILE *);
void FP_reinit_transp_rsv_cprod(s_carac_fp *, int, int, FILE *); // NG : 12/06/2023

// in manage_mto.c
s_mto_fp *FP_create_mto(int, int);
s_mto_fp **FP_tab_mto(s_mto_fp *, int, FILE *);
s_mto_fp *chain_mto(s_mto_fp *, s_mto_fp *);
s_mto_fp *FP_secured_chain_mto_fwd(s_mto_fp *, s_mto_fp *);
int FP_get_MTO_id(s_mto_fp *, int);
s_mto_fp *FP_link_mto(s_carac_fp *, int, int, double, FILE *);
s_mto_fp *FP_get_mto_cell(s_carac_fp *, int, int, FILE *);
void FP_finalize_link_mto(s_carac_fp *, FILE *);
void FP_print_MTO(s_carac_fp *, FILE *);
void FP_read_all_MTO(s_carac_fp *, int, FILE *);                                     // NG : 07/09/2021 : now includes FORMATTED reading procedure
void FP_read_all_mto_year_bin(s_carac_fp *, char *, FILE *, int, int, FILE *);       // NG : 07/09/2021
void FP_read_all_mto_year_formatted(s_carac_fp *, char *, FILE *, int, int, FILE *); // NG : 07/09/2021

/* NG : 07/07/2020 : mto and input structure-related functions were all mixed up.
Therefore, manage_input.c file is created to seperate the two function kinds.
Doesn't add much, but cleaner.*/

// in manage_input.c
s_input_fp *FP_create_input();
void FP_set_default_input(s_input_fp *);
void FP_check_input_setup(s_input_fp *, int, FILE *);
// **** Transport-related functions ****
s_input_fp *FP_create_transport_species_input(FILE *);
void FP_manage_memory_input_species(s_carac_fp *, int, FILE *);
void FP_set_default_input_settings_species(s_input_fp *, int, FILE *);
void FP_check_input_transport_setup(s_input_fp *, int, FILE *);
void FP_check_transport_input_types(s_input_fp *, int, FILE *);

// in itos.c
char *FP_name_mto(int);
char *FP_name_param(int);
char *FP_name_stock(int);
char *FP_cprod_type(int);     // NG : 23/01/2020
char *FP_catchment_type(int); // NG : 23/01/2020
char *FP_input_types(int);    // NG : 11/06/2021
char *FP_transport_spetype(int);
// **** Transport-related functions ****
char *FP_transport_forcing_conditions(int);
char *FP_transport_input_variable_name(int);
char *FP_boundary_frequency(int);
char *FP_transport_outvar_shortname(int);
char *FP_transport_input_mflux(int);
int FP_test_browse_spa(s_carac_fp *, FILE *);

// in manage_output.c
void FP_print_header(FILE *);
void FP_print_header_transport(int, int *, FILE *);          // NG : 23/07/2020 Transport
void FP_print_species_properties(s_carac_fp *, int, FILE *); // NG : 12/08/2020 Transport
// Hydro mass balance-related functions
void FP_write_mb_hydro(int, double, double, double, s_out_io *, s_carac_fp *, FILE *);             // NG : 24/07/2020
void FP_write_mb_hydro_formatted(int, s_carac_fp *, double, s_out_io *, double, FILE *);           // NG : 24/07/2020
void FP_write_mb_hydro_bin(int, s_carac_fp *, double, s_out_io *, double, FILE *);                 // NG : 24/07/2020
double *FP_ele_bu_budget(s_bal_unit_fp *, s_input_fp *, double, s_out_io *, double, FILE *);       // NG : 25/07/2020
double *FP_cprod_budget(s_cprod_fp *, s_input_fp *, s_out_io *, FILE *);                           // NG : 25/07/2020
double **FP_get_mb_val_hydro_bin(int, int, s_out_io *, s_carac_fp *, double, int, double, FILE *); // NG : 24/07/2020
void FP_print_budget_ele_bu(s_bal_unit_fp *, s_input_fp *, double, double, s_out_io *, FILE *);    // NG : 25/07/2020
void FP_print_budget_cprod(s_cprod_fp *, s_input_fp *, double, s_out_io *, FILE *);                // NG : 25/07/2020
// Transport outputs-related functions
void FP_write_mb_transport(int, int, double, double, double, s_out_io *, s_carac_fp *, FILE *);     // NG : 23/07/2020 Transport
void FP_write_mb_transport_formatted(int, int, s_carac_fp *, double, s_out_io *, double, FILE *);   // NG : 25/07/2020 Transport
void FP_write_mb_transport_bin(int, int, s_carac_fp *, s_out_io *, double, FILE *);                 // NG : 23/07/2020 Transport
double **FP_get_mb_val_transport_bin(int, int, int, s_out_io *, s_carac_fp *, int, double, FILE *); // NG : 23/07/2020 Transport
double *FP_transport_budget_ele_bu(s_bal_unit_fp *, int, double, FILE *);                           // NG : 25/07/2020 Transport
double *FP_transport_budget_cprod(s_cprod_fp *, int, FILE *);                                       // NG : 25/07/2020 Transport
// Display-type functions
void FP_print_corresp(s_carac_fp *, int, int, FILE *); // NG: 31/08/2021 : Printing the corresp file according to the spatial scale the user asked for
void FP_print_abstract(s_carac_fp *, FILE *);
void FP_print_FP_hyperdermic_abstract(s_carac_fp *, FILE *);       // NG : 18/02/2020
void FP_print_corresp_all_species(s_carac_fp *, int, int, FILE *); // NG : 19/07/2020 Transport