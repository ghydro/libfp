/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_mto.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_mto.c
 * @brief       Functions dealing instances of the meteo object structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Creates and initializes a pointer to the s_mto_fp structure
 * @return Newly created *s_mto_fp pointer
 */
s_mto_fp *FP_create_mto(int id, int itype) {
  s_mto_fp *pmto;
  int i;

  pmto = new_meteo();
  bzero((char *)pmto, sizeof(s_mto_fp));
  pmto->p_ft = (s_ft **)malloc(FP_NMETEO * sizeof(s_ft *));
  bzero((char *)pmto->p_ft, FP_NMETEO * sizeof(s_ft *));
  pmto->id[itype] = id;

  return pmto;
}

/**
 * @brief Stores all the s_mto_fp* pointer into a single array
 * @return Filled **s_mto_fp array
 */
s_mto_fp **FP_tab_mto(s_mto_fp *pmto, int nb_mto, FILE *fpout) {
  int i = 0;
  s_mto_fp **tab_mto;
  tab_mto = (s_mto_fp **)malloc(nb_mto * sizeof(s_mto_fp *));
  bzero((char *)tab_mto, nb_mto * sizeof(s_mto_fp *));
  pmto = (s_mto_fp *)SPA_browse_all_struct(MTO_SPA, pmto, BEGINNING_FP);

  for (i = 0; i < nb_mto; i++) {
    pmto->id[FP_INTERN] = i;
    tab_mto[i] = pmto;
    pmto = pmto->next;
  }
  return tab_mto;
}

/**
 * @brief Chains two s_mto_fp* pointers
 */
s_mto_fp *chain_mto(s_mto_fp *mto1, s_mto_fp *mto2) {
  mto2->prev = mto1;
  mto1->next = mto2;
  return mto1;
};

/**
 * @brief Chains two s_mto_fp* pointers
 */
s_mto_fp *FP_secured_chain_mto_fwd(s_mto_fp *mto1, s_mto_fp *mto2) {
  if (mto1 != NULL)
    mto1 = chain_mto(mto1, mto2);
  else {
    mto1 = mto2;
  }
  return mto1;
}

/**
 * @brief Extract the MTO cell id from the associated structure
 * @return MTO cell ID if the pointer is found.
 */
int FP_get_MTO_id(s_mto_fp *pmto, int type) {
  int id;
  if (type < FP_NDID)
    id = pmto->id[type];
  return id;
}

/**
 * @brief TBD
 * @return s_mto_fp* pointer
 */
s_mto_fp *FP_link_mto(s_carac_fp *pcarac_fp, int id_mto, int id_bu, double area_bu, FILE *fpout) {
  s_mto_fp *pmto;
  s_id_spa *link_tmp;

  pmto = FP_get_mto_cell(pcarac_fp, id_mto, FP_GIS, fpout);
  link_tmp = SPA_create_ids(id_bu, area_bu);
  pmto->link = SPA_secured_chain_id_fwd(pmto->link, link_tmp);
  pmto->area += area_bu;
  return pmto;
}

/**
 * @brief Fetch a *s_mto_fp pointer (mto cell) according to its ID
 * @return *s_mto_fp pointer if found
 */
s_mto_fp *FP_get_mto_cell(s_carac_fp *pcarac_fp, int id_mto, int id_type, FILE *fpout) {
  s_mto_fp *pmto;
  int i = 0;
  int nb_mto;
  int verif_id = CODE_TS;
  if (id_type == FP_INTERN) {
    pmto = pcarac_fp->pinputs->mto[id_mto];
  } else {
    nb_mto = pcarac_fp->pinputs->nb_mto;

    if (id_mto < nb_mto) {
      pmto = pcarac_fp->pinputs->mto[id_mto];
      verif_id = pmto->id[FP_GIS];
    }

    if (verif_id != id_mto) {
      while (verif_id != id_mto) {
        if (i > nb_mto)
          break;
        pmto = pcarac_fp->pinputs->mto[i];
        verif_id = pmto->id[FP_GIS];
        i++;
      }
      if (i > nb_mto) {
        LP_error(fpout, " No mto cell with GIS id : %d check your input file \n", id_mto);
      }
    }
  }
  return pmto;
}

/**
 * @brief TBD
 * @return None
 */
void FP_finalize_link_mto(s_carac_fp *pcarac_fp, FILE *fpout) {
  int nb_mto, i;
  double area_mto;
  s_mto_fp *pmto;
  s_id_spa *link;
  nb_mto = pcarac_fp->pinputs->nb_mto;
  for (i = 0; i < nb_mto; i++) {
    pmto = pcarac_fp->pinputs->mto[i];
    area_mto = pmto->area;
    if (pmto->link != NULL) {
      link = SPA_divide_ids(pmto->link, area_mto);
      pmto->link = SPA_free_ids(pmto->link, fpout);
      pmto->link = link;
    }
  }
}

/**
 * @brief Debug function. Prints MTO cells attributes in the log file, including associated time series
 * @return None
 */
void FP_print_MTO(s_carac_fp *pcarac_fp, FILE *fpout) {
  int i, j, nb_mto;
  s_mto_fp *pmto;
  nb_mto = pcarac_fp->pinputs->nb_mto;
  for (i = 0; i < nb_mto; i++) {

    pmto = pcarac_fp->pinputs->mto[i];
    LP_printf(fpout, "MTO cell %d GIS %d INTERN\n", pmto->id[FP_GIS], pmto->id[FP_INTERN]);
    for (j = 0; j < FP_NMETEO; j++) {
      LP_printf(fpout, " %s data : \n", FP_name_mto(j));
      TS_print_ts(pmto->p_ft[j], fpout);
    }
  }
}

/**
 * @brief Launches general MTO reading procedures depending on the selected input format (either FORMATTED or UNFORMATTED)
 * @return None
 * @internal NG : 07/08/2021 : BL function initially. Rewritten.
 */
void FP_read_all_MTO(s_carac_fp *pcarac_fp, int year, FILE *fpout) {
  int j;
  char name[STRING_LENGTH_LP];
  char *mto_path = pcarac_fp->pinputs->input_paths[FP_MTO];
  s_input_fp *pinput = pcarac_fp->pinputs;
  FILE *fpin;

  for (j = 0; j < FP_NMETEO; j++) {
    if (pinput->format == UNFORMATTED_IO) {
      sprintf(name, "%s/%s.%d%d", mto_path, pinput->prefix[j], year, year + 1);
      fpin = fopen(name, "rb");
      if (fpin == NULL) {
        sprintf(name, "%s/%s.%d%d.dir", mto_path, pinput->prefix[j], year, year + 1);
        fpin = fopen(name, "rb");
        if (fpin == NULL)
          LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s : Can't open the file %s. Please check the folder input path and/or the files prefixes declared.\n", VERSION_FP, __FILE__, __func__, name);
      }

      FP_read_all_mto_year_bin(pcarac_fp, name, fpin, year, j, fpout);
      fclose(fpin);
    } else // FORMATTED input
    {
      sprintf(name, "%s/%s_%d%d.txt", mto_path, pinput->prefix[j], year, year + 1);
      fpin = fopen(name, "r");
      if (fpin == NULL) {
        sprintf(name, "%s/%s_%d%d.dat", mto_path, pinput->prefix[j], year, year + 1);
        fpin = fopen(name, "r");
        if (fpin == NULL)
          LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s : Can't open the file %s. Please check the folder input path and/or the files prefixes declared.\n", VERSION_FP, __FILE__, __func__, name);
      }
      FP_read_all_mto_year_formatted(pcarac_fp, name, fpin, year, j, fpout);
      fclose(fpin);
    }
  }
}

/**
 * @brief Creates and stores all s_ft* time series for all MTO cells, for one MTO parameter (param_type) in UNFORMATTED format
 * @return None
 * @internal NG : 07/08/2021 : BL function initially. rewritten.
 */
void FP_read_all_mto_year_bin(s_carac_fp *pcarac_fp, char *name, FILE *fpin, int year, int param_type, FILE *fpout) {
  int errno, location, nj, i, j, nb_cell, size_bloc, mto_cell;
  float tmp;
  double size_file, verif, value, julian;
  int nb_mto = pcarac_fp->pinputs->nb_mto;
  s_ft *pftmp, *pft;
  s_mto_fp *pmto;

  // Making sure file size is OK
  location = 0;
  errno = fseek(fpin, location, SEEK_END);
  size_file = (double)ftell(fpin);

  nj = TS_nb_jour_an(year + 1);
  verif = size_file / ((double)nj * sizeof(float));
  nb_cell = (int)verif;
  // LP_printf(fpout,"Number of records in file : %d\n",nb_cell);  // NG check

  if (fabs(nb_cell - nb_mto) > EPS_FP)
    LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s at line %d : File %s is not shaped properly. Number of MTO cells is not consistent with mto_cell file data. MTO file should be a binary file filled with FLOAT type data. \n", VERSION_FP, __FILE__, __func__, __LINE__, name);

  // Building MTO time series for all MTO cells
  for (i = 0; i < nb_mto; i++) {
    pft = NULL;
    pftmp = NULL;
    pmto = pcarac_fp->pinputs->mto[i];
    mto_cell = pmto->id[FP_INTERN]; // We're refering to the order in which MTO cells are written in the mto_cell file. Not their GIS id !

    if (pmto->p_ft[param_type] != NULL)
      pmto->p_ft[param_type] = TS_free_ts(pmto->p_ft[param_type], fpout); // Emptying ts of the previous year

    for (j = 0; j < nj; j++) {
      location = (j * nb_mto + (mto_cell)) * sizeof(float);
      if (location < 0 || location > size_file)
        LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s at line %d : Forbidden location in binary file %s at day %d cell %d. Searched location = %d (Max location = %f). \n", VERSION_FP, __FILE__, __func__, __LINE__, name, j, mto_cell, location, size_file);

      errno = fseek(fpin, location, SEEK_SET);
      fread(&tmp, sizeof(float), 1, fpin);
      // LP_printf(fpout,"value %f\n",tmp);
      value = (double)tmp;
      julian = (double)TS_calculate_jour_julien_j_nbday(year, CODE_FP, j);

      if (value > MAX_RAIN_FP)
        LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s at line %d : while reading %s, suspicious value read : %f", __FILE__, __func__, __LINE__, name, value);

      pftmp = TS_create_function(julian, value);
      pft = TS_secured_chain_fwd_ts(pft, pftmp);
    }
    pft = TS_browse_ft(pft, BEGINNING_TS);
    pmto->p_ft[param_type] = pft;
  }
}

/**
 * @brief Creates and stores all s_ft* time series for all MTO cells, for one MTO parameter (param_type) in FORMATTED format
 * @return None
 */
void FP_read_all_mto_year_formatted(s_carac_fp *pcarac_fp, char *name, FILE *fpin, int year, int param_type, FILE *fpout) {
  int i, j, nl, nc, nb_days, size = 0, cpt = 0, eof = 0;
  double **tab = NULL;
  double julian, val;
  int mto_cell, nb_mto = pcarac_fp->pinputs->nb_mto;
  s_mto_fp *pmto;
  s_input_fp *pinput = pcarac_fp->pinputs;
  s_ft *pftmp, *pft;

  nb_days = TS_nb_jour_an(year + 1);
  size = nb_days * nb_mto;

  // LP_printf(fpout, "In libfp%4.2f : Expected number of values in formatted MTO file %s : %d\n",VERSION_FP,name,size);    // NG check

  // File is only read and stored once for all cells. Convenient in case of large formatted file. Otherwise, reading takes way too much time !!
  tab = (double **)malloc(nb_days * sizeof(double *));
  for (i = 0; i < nb_days; i++)
    tab[i] = (double *)calloc(nb_mto, sizeof(double));

  nl = 0;
  while (eof != EOF) {
    for (nc = 0; nc < nb_mto; nc++) {
      eof = fscanf(fpin, "%lf", &val);
      if (eof == EOF)
        break;
      cpt++;
      tab[nl][nc] = val;
    }
    nl++;
  }

  // Making sure if file size is OK
  if (fabs(cpt - size) > EPS_FP) {
    // Deallocating...
    for (i = 0; i < nb_days; i++)
      free(tab[i]);
    free(tab);
    LP_error(fpout, "In libfp%4.2f : Error in file %s, in function %s at line %d : FORMATTED MTO file %s is not shaped properly. Check your input data. It needs to be a matrix sized over the number of days (as lines) and the number of MTO cells (as columns).\n", VERSION_FP, __FILE__, __func__, __LINE__, name);
  }

  // Creating time series for all MTO cells...
  for (i = 0; i < nb_mto; i++) {
    pft = NULL;
    pftmp = NULL;
    pmto = pcarac_fp->pinputs->mto[i];
    mto_cell = pmto->id[FP_INTERN]; // We're refering to the order in which MTO cells are written in the mto_cell file. Not their GIS id !

    if (pmto->p_ft[param_type] != NULL)
      pmto->p_ft[param_type] = TS_free_ts(pmto->p_ft[param_type], fpout);

    for (j = 0; j < nb_days; j++) {
      julian = (double)TS_calculate_jour_julien_j_nbday(year, CODE_FP, j);
      pftmp = TS_create_function(julian, tab[j][mto_cell]);
      pft = TS_secured_chain_fwd_ts(pft, pftmp);
    }
    pft = TS_browse_ft(pft, BEGINNING_TS);
    pmto->p_ft[param_type] = pft;
  }

  // Deallocating...
  for (i = 0; i < nb_days; i++)
    free(tab[i]);
  free(tab);
}