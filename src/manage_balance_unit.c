/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_balance_unit.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_balance_unit.c
 * @brief       Functions dealing with attributes of the instances of the wat_bal_calc_unit structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Chains two watbal units
 * @return -
 */
s_bal_unit_fp *chain_bu(s_bal_unit_fp *prod1, s_bal_unit_fp *prod2) {

  prod1->next = prod2;
  prod2->prev = prod1;
  return prod1;
};

/**
 * @brief Chains two watbal units
 * @return -
 */
s_bal_unit_fp *FP_secured_chain_bu_fwd(s_bal_unit_fp *prod1, s_bal_unit_fp *prod2) {
  if (prod1 != NULL) {
    prod1 = chain_bu(prod1, prod2);
  } else {
    prod1 = prod2;
  }
  return prod1;
}

/**
 * @brief Creates and initializes a water balance unit
 * @return Nelwy initialized water balance unit pointer
 */
s_bal_unit_fp *FP_create_bal_unit(int indice) {

  s_bal_unit_fp *pBU;
  int i;
  pBU = new_bal_unit();
  bzero((char *)pBU, sizeof(s_bal_unit_fp));
  pBU->id = indice;
  pBU->pwat_bal = RSV_create_wat_bal();

  // Transport-related attributes
  pBU->link = NULL;
  pBU->pbound = NULL;
  pBU->input_mflux = NULL;

  return pBU;
};

/**
 * @brief Stores all water balance unit pointers into a single array
 * @return Filled s_bal_unit_fp** array
 */
s_bal_unit_fp **FP_tab_BU(s_bal_unit_fp *pBU, int nb_BU, FILE *fpout) {

  int i = 0;
  s_bal_unit_fp **tab_BU;

  tab_BU = (s_bal_unit_fp **)malloc(nb_BU * sizeof(s_bal_unit_fp *));
  bzero((char *)tab_BU, nb_BU * sizeof(s_bal_unit_fp *));
  pBU = (s_bal_unit_fp *)SPA_browse_all_struct(BAL_UNIT_SPA, pBU, BEGINNING_FP);
  for (i = 0; i < nb_BU; i++) {
    // LP_printf(fpout," %d pbu_ID %d\n",i,pBU->id); //BL to debug
    pBU->id = i;
    tab_BU[i] = pBU;
    pBU = pBU->next;
  }
  return tab_BU;
}

/**
 * @brief Launches water balance calculations (aet, effective rainfall fractioning,
 * infiltration and runoff bucket discharges) for a single water balance
 */
void FP_calc_wat_bal_BU(s_bal_unit_fp *pBU, double t, double dt, FILE *fpout, int lai_type) {

  FP_calc_wat_bal(pBU, t, dt, fpout, lai_type);
  FP_balance_distrib(pBU, fpout);
  FP_runoff_infiltration(pBU, fpout);
}

/**
 * @brief Creates a BU, and fills its properties (IDS, etc.)
 * @return Newly created BU pointer
 */
s_bal_unit_fp *FP_define_bal_unit(s_carac_fp *pcarac_fp, int id_bu, int id_fp, int id_mto, double area_bu, FILE *fpout) {
  s_bal_unit_fp *pbu;
  int i;
  pbu = FP_create_bal_unit(id_bu);
  pbu->param_fp = pcarac_fp->p_param_fp[id_fp - 1];
  pbu->area = area_bu;
  pbu->pmto = FP_link_mto(pcarac_fp, id_mto, id_bu, area_bu, fpout);
  return pbu;
};

/**
 * @brief Prints BU properties
 */
void FP_print_BU(s_carac_fp *pcarac_fp, FILE *fpout) {

  int n_bu, i, j;
  s_bal_unit_fp *pbu;
  n_bu = pcarac_fp->nb_BU;

  for (i = 0; i < n_bu; i++) {
    pbu = pcarac_fp->p_bu[i];
    LP_printf(fpout, "Balance unit %d \n", pbu->id);
    LP_printf(fpout, "of param :");
    FP_print_param(pbu->param_fp, fpout);
    LP_printf(fpout, "linked with mto : %d\n", pbu->pmto->id[FP_GIS]);
  }
}

/**
 * @brief Based on the intern id, verifies if a ele_bu already has its pointer defined
 * @return Pointer towards the wat_bal unit
 */
s_bal_unit_fp *FP_check_is_ele_bu_defined(s_bal_unit_fp **tab_ele, int nb_ele_bu, int id, FILE *fpout) {

  int i, found;
  int id_intern = id - 1; // elebu intern ids start at 0.
  s_bal_unit_fp *pwatbal = NULL;

  if (tab_ele == NULL) {
    LP_error(fpout, "libfp%4.2f, File %s in %s line %d : Pointer table is null.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  }

  for (i = 0; i < nb_ele_bu; i++) {
    found = NO;
    if (tab_ele[i]->id == id_intern) {
      pwatbal = tab_ele[i];
      found = YES;
      break;
    }
  }
  if (found == NO) {
    LP_error(fpout,
             "In libfp%4.2f, File %s in %s line %d : Ele_bu ID %d not defined yet. Check consistency between species_cell, "
             "bu_transport and forcing_boundary files.\n",
             VERSION_FP, __FILE__, __func__, __LINE__, id);
  }
  return pwatbal;
}

/**
 * @brief General function launching allocations or reallocations for transport watbal type attributes
   for the total number of species read
 * @return -
 */
void FP_transport_manage_memory_watbal_ele_bu(s_bal_unit_fp *pbu, int nb_species, FILE *flog) // nb_species = total number of species
{
  RSV_manage_memory_transport(pbu->pwat_bal, nb_species, flog); // RSV_watbal
  FP_manage_memory_input_mflux_ele_bu(pbu, nb_species, flog);   // Inputfluxes
}

/**
 * @brief (Re)-allocates **link according to the current number of species being declared.
 */
void FP_manage_memory_links_ele_bu_species(s_bal_unit_fp *pbu, int nb_species, FILE *flog) {

  int j, id_species = nb_species - 1;

  if (pbu->link == NULL) // i.e. Reading the first specie
  {
    pbu->link = (s_id_spa **)malloc(nb_species * sizeof(s_id_spa *));

    if (pbu->link == NULL) {
      LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Bad memory allocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    }
  } else { // At least on specie has been read. Extending and initializing array and pointers where needed.
    pbu->link = (s_id_spa **)realloc(pbu->link, nb_species * sizeof(s_id_spa *));
    if (pbu->link == NULL) {
      LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Bad memory reallocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    }
  }
  pbu->link[id_species] = NULL;
}

/**
 * @brief (Re)-allocates *input_mflux according to the total number of species declared
 */
void FP_manage_memory_input_mflux_ele_bu(s_bal_unit_fp *pbu, int nb_species, FILE *flog) // nb_species = total number of species
{
  int i, p;

  if (pbu->input_mflux != NULL) {
    LP_error(flog, "In libfp%4.2f : Error in file %s, function %s at line %d. Memory has already been allocated.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  }

  pbu->input_mflux = (double **)malloc(nb_species * sizeof(double *));
  if (pbu->input_mflux == NULL)
    LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Bad memory allocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  for (p = 0; p < nb_species; p++) {
    pbu->input_mflux[p] = (double *)malloc(FP_FIN_TYPE * sizeof(double));
    for (i = 0; i < FP_FIN_TYPE; i++)
      pbu->input_mflux[p][i] = 0.;
  }
}

/**
 * @brief Reset to zero all input matter fluxes for a given specie
 * @return -
 */
void FP_reinit_input_mflux(double **input_mflux, int id_species) {
  int j;
  for (j = 0; j < FP_FIN_TYPE; j++)
    input_mflux[id_species][j] = 0.;
}

/**
 * @brief Global function to verify all links between bu and a specie grid cell (if needed)
 * @return -
 */
void FP_check_BU_species_links(s_carac_fp *pcarac_fp, int nb_species, FILE *flog) {

  int i, nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp *pwatbal;

  for (i = 0; i < nb_BU; i++) {
    pwatbal = pcarac_fp->p_bu[i];
    FP_print_BU_species_links(pwatbal, nb_species, flog);
  }
}

/**
 * @brief Function to print out links between a bu and a specie grid cell.
 * @return -
 */
void FP_print_BU_species_links(s_bal_unit_fp *pwatbal, int nb_species, FILE *flog) {

  int id_species = nb_species - 1;
  double coverage = 0;
  s_id_spa *ptemp;

  if (pwatbal->link[id_species] != NULL) {
    ptemp = SPA_browse_id(pwatbal->link[id_species], BEGINNING_SPA, flog);
    while (ptemp != NULL) {
      coverage += ptemp->prct;
#ifdef DEBUG
      LP_printf(flog, "Species id %d : ele_bu id %d link to specie cell GIS id %d. Coverage percent : %f.\n", id_species, pwatbal->id, ptemp->id, ptemp->prct); // NG check
#endif
      ptemp = ptemp->next;
    }

    if (fabs(coverage - 1) > EPSILON_SURF_FP)
      LP_warning(flog, "Species INTERN id %d : Total coverage of ele_bu INTERN id %d is only %7.2f prct. Make sure it's correct !\n", id_species, pwatbal->id, coverage * 1.e2);
  } else {
    LP_warning(flog, "Species INTERN id %d : ele_bu INTERN id %d is not covered by specie grid. Make sure it's correct !\n", id_species, pwatbal->id, coverage * 1.e2);
  }
}

/**
 * @brief Calculates all sub-root fluxes for one BU in FP_TOTAL_TRFLUX forcing mode (for SOLUTE specie)
 */
void FP_transport_solute_total_trflux_one_BU(s_bal_unit_fp *pBU, s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) // dt en jours
{

  s_id_spa *link = pBU->link[id_species];
  s_specie_unit_fp **tab_speunit = pcarac_fp->p_species_unit[id_species];
  s_forcing_bound_fp ***pbound = pBU->pbound;
  int j, nb_speunit = pcarac_fp->nb_specie_unit[id_species];
  s_id_spa *link_tmp;
  s_specie_unit_fp *pspeunit;
  double coverage = 0., remain_surf = 0., bound_value = 0., vinf = 0., vruis = 0., csr = 0.;
  double fluxtot = 0., fact_conv = 1.;
  double *forcing_values;

  // Watbal resets
  RSV_reinit_matter_fluxes_bal(pBU->pwat_bal, id_species);
  FP_reinit_input_mflux(pBU->input_mflux, id_species);

  // Fetching the ele_bu water heights (mm) - Conversion into water volume (m3/timestep)
  vinf = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_INFILT], pBU->area, dt, SEC_TS, flog);
  vruis = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_RUIS], pBU->area, dt, SEC_TS, flog);

  // Calculation molar masses ratio for element to specie conversion
  if (mmass_ele != CODE_FP)
    fact_conv = mmass_spe / mmass_ele;

  // Calculating total flux at the ele_bu scale, accounting for intersections with specie units cells
  if (link != NULL) {
    link_tmp = SPA_browse_id(link, BEGINNING_SPA, flog);
    while (link_tmp != NULL) {
      coverage += link_tmp->prct;
      pspeunit = FP_check_is_ele_specie_unit_defined(tab_speunit, nb_speunit, link_tmp->id, flog);
      forcing_values = FP_get_forcing_values(t, pspeunit, flog);
      fluxtot += ((link_tmp->prct * pBU->area) * forcing_values[FP_TRFLUX] * fact_conv); // Specie mass (not element) in g.
      link_tmp = link_tmp->next;
      free(forcing_values);
    }
  }

  // Mass balance shortcut
  pBU->input_mflux[id_species][FP_MFORCED] = fluxtot;

  // Filling of the non-covered area of the elebu using allowed boundaries type for this forcing mode (i.e. using matter flux or concentration), if defined.
  remain_surf = fabs(coverage - 1);

  if (remain_surf > EPSILON_SURF_FP) {
    // LP_printf(flog,"Specie id %d - ele_bu intern id %d - coverage restant : %f\n",id_species,pBU->id,remain_surf); // check

    if (pbound != NULL) {
      if (pbound[id_species] != NULL) {
        /*if (pbound[id_species][FP_MATTER] == NULL && pbound[id_species][FP_CONC] == NULL)  // NG : Warning excessif dans le log --> suspendu
          LP_warning(flog,"Specie ID %d ele_bu intern ID %d : Neither matter flux or concentration boundary have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2);*/

        if (pbound[id_species][FP_TRFLUX] != NULL && pbound[id_species][FP_TRVAR] != NULL) {
          LP_error(flog, "Specie ID %d : Concentration and matter flux-type boundary conditions have BOTH been defined for ele_bu intern ID %d. Make a decision !\n", id_species, pBU->id);
        }

        // In case if the boundary is defined using matter flux
        if (pbound[id_species][FP_TRFLUX] != NULL) {
          bound_value = FP_get_boundary_value(t, pbound, id_species, FP_TRFLUX, flog);
          // LP_printf(flog,"valeur de value flux bound %f\n",bound_value); // NG check
          fluxtot += ((remain_surf * pBU->area) * bound_value * fact_conv); // Total flux in g
          pBU->input_mflux[id_species][FP_MBOUND] += ((remain_surf * pBU->area) * bound_value * fact_conv);
        }

        // In case if the boundary is defined using concentration
        if (pbound[id_species][FP_TRVAR] != NULL) {
          bound_value = FP_get_boundary_value(t, pbound, id_species, FP_TRVAR, flog);
          bound_value = bound_value * fact_conv * (vinf + vruis) * remain_surf; // Converting concentration in specie mass in g
          fluxtot += bound_value;
          pBU->input_mflux[id_species][FP_MBOUND] += bound_value;
        }
      } else {
        // LP_warning(flog,"Specie id %d ele_bu GIS id %d : No boundaries have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id+1,remain_surf*1.e2);
      }
    }
  }

  // Subroot concentration in g/m3
  if ((vinf + vruis) > EPS_VOL_FP)
    csr = fluxtot / (vinf + vruis);

  // Storing in watbals
  pBU->pwat_bal->trflux[id_species][RSV_INFILT] = (csr * vinf) / pBU->area; // g/m2
  pBU->pwat_bal->trflux[id_species][RSV_RUIS] = (csr * vruis) / pBU->area;  // g/m2
  if (vinf > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_INFILT] = csr; // g/m3
  if (vruis > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_RUIS] = csr; // g/m3

  for (j = 0; j < FP_FIN_TYPE; j++)
    pBU->input_mflux[id_species][j] /= pBU->area; // g/m2
}

/**
 * @brief Calculates all sub-root fluxes for one BU in 'FP_TOTAL_TRFLUX_WATER forcing mode (for SOLUTE specie)
 * @return -
 */
void FP_transport_solute_total_trflux_water_one_BU(s_bal_unit_fp *pBU, s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {
  int j;
  int nb_speunit = pcarac_fp->nb_specie_unit[id_species];
  double coverage = 0., remain_surf = 0., bound_value = 0., vinf = 0., vruis = 0., csr = 0.;
  double fluxtot = 0., fact_conv = 1.;
  double *forcing_values;
  double partition = 0.;
  double eautot = 0.;
  s_id_spa *link_tmp;
  s_specie_unit_fp *pspeunit;
  s_specie_unit_fp **tab_speunit = pcarac_fp->p_species_unit[id_species];
  s_id_spa *link = pBU->link[id_species];
  s_forcing_bound_fp ***pbound = pBU->pbound;

  // Watbal resets
  RSV_reinit_matter_fluxes_bal(pBU->pwat_bal, id_species);
  FP_reinit_input_mflux(pBU->input_mflux, id_species);

  // Fetching the ele_bu water heights (mm) - Conversion into water volume (m3 per timestep)
  vinf = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_INFILT], pBU->area, dt, SEC_TS, flog);
  vruis = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_RUIS], pBU->area, dt, SEC_TS, flog);

  if ((vinf + vruis) > EPS_FP)
    partition = vinf / (vinf + vruis); // Here, total water flux is divided using Peff partionning

  // Calculation molar masses ratio for element to specie conversion
  if (mmass_ele != CODE_FP)
    fact_conv = mmass_spe / mmass_ele;

  // Calculating total flux at the ele_bu scale, accounting for intersections with specie units cells
  if (link != NULL) {
    link_tmp = SPA_browse_id(link, BEGINNING_SPA, flog);
    while (link_tmp != NULL) {
      coverage += link_tmp->prct;
      pspeunit = FP_check_is_ele_specie_unit_defined(tab_speunit, nb_speunit, link_tmp->id, flog);
      forcing_values = FP_get_forcing_values(t, pspeunit, flog);
      fluxtot += ((link_tmp->prct * pBU->area) * forcing_values[FP_TRFLUX] * fact_conv); // Specie mass (not element) in g.
      eautot += (forcing_values[FP_WATER] * pBU->area * link_tmp->prct);                 // Total water in m3.
      link_tmp = link_tmp->next;
      free(forcing_values);
    }
  }

  // Mass balance shortcut
  pBU->input_mflux[id_species][FP_MFORCED] = fluxtot;
  remain_surf = fabs(coverage - 1);

  if (remain_surf > EPSILON_SURF_FP) {
    if (pbound != NULL) {
      if (pbound[id_species] != NULL) {
        if (pbound[id_species][FP_TRFLUX] == NULL && pbound[id_species][FP_TRVAR] == NULL) {
          eautot += ((vinf + vruis) * remain_surf); // NG: 30/07/2020 - Using CaWaQS/hydro libfp budget
          //	LP_warning(flog,"Specie ID %d ele_bu intern ID %d : Neither matter flux or concentration boundary have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2); // NG : Warning excessif dans le log --> suspendu
        }

        if (pbound[id_species][FP_TRFLUX] != NULL && pbound[id_species][FP_TRVAR] != NULL) {
          LP_error(flog, "Specie ID %d : Concentration and matter flux-type boundary conditions have BOTH been defined for ele_bu intern ID %d. Make a decision !\n", id_species, pBU->id);
        }

        // In case the boundary is defined using matter flux
        if (pbound[id_species][FP_TRFLUX] != NULL) {
          bound_value = FP_get_boundary_value(t, pbound, id_species, FP_TRFLUX, flog);
          fluxtot += ((remain_surf * pBU->area) * bound_value * fact_conv); // Total flux in g
          pBU->input_mflux[id_species][FP_MBOUND] += ((remain_surf * pBU->area) * bound_value * fact_conv);
          eautot += ((vinf + vruis) * remain_surf);
        }

        // In case the boundary is defined using concentration
        if (pbound[id_species][FP_TRVAR] != NULL) {
          bound_value = FP_get_boundary_value(t, pbound, id_species, FP_TRVAR, flog);
          bound_value = bound_value * fact_conv * (vinf + vruis) * remain_surf; // Concentration conversion into specie mass in g
          eautot += ((vinf + vruis) * remain_surf);
          fluxtot += bound_value;
          pBU->input_mflux[id_species][FP_MBOUND] += bound_value;
        }
      } else {
        eautot += ((vinf + vruis) * remain_surf); // NG: 30/07/2020 : If ele_bu is not covered, qi=qiCAWAQS and qruis=qrCAWAQS
        // LP_warning(flog,"Specie id %d ele_bu intern %d : No boundaries have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2); // NG : Warning excessif dans le log --> suspendu
      }
    } else {
      eautot += ((vinf + vruis) * remain_surf);
    }
  }

  // Partionning
  vinf = partition * eautot;
  vruis = (1 - partition) * eautot;

  // Sub-root concentrations in g/m3
  if (eautot > EPS_VOL_FP)
    csr = fluxtot / eautot;

  // Storing fluxes and concentration in watbals
  pBU->pwat_bal->trflux[id_species][RSV_INFILT] = (partition * fluxtot) / pBU->area;     // g/m2
  pBU->pwat_bal->trflux[id_species][RSV_RUIS] = ((1 - partition) * fluxtot) / pBU->area; // g/m2

  if (vinf > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_INFILT] = (partition * fluxtot) / vinf; // g/m3
  if (vruis > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_RUIS] = ((1 - partition) * fluxtot) / vruis; // g/m3

  // Overwriting the native CaWaQS hydro budger (which is stored in mm)
  pBU->pwat_bal->q[RSV_INFILT] = RSV_volume_to_mm(vinf, pBU->area, dt, SEC_TS, flog); // mm
  pBU->pwat_bal->q[RSV_RUIS] = RSV_volume_to_mm(vruis, pBU->area, dt, SEC_TS, flog);  // mm

  for (j = 0; j < FP_FIN_TYPE; j++)
    pBU->input_mflux[id_species][j] /= pBU->area; // g/m2
}

/**
 * @brief Calculates all sub-root fluxes for one BU in 'FP_DISSOCIATED_TRFLUX_WATER forcing mode (for SOLUTE specie)
 * @return -
 */
void FP_transport_solute_dissociated_trflux_water_one_BU(s_bal_unit_fp *pBU, s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {

  int j, nb_speunit = pcarac_fp->nb_specie_unit[id_species];
  double coverage = 0., remain_surf = 0., vinf = 0., vruis = 0., cinf = 0., crunoff = 0.;
  double flux_runoff = 0., flux_inf = 0., eau_runoff = 0., eau_inf = 0., fact_conv = 1.;
  double bound_runoff = 0., bound_inf = 0.;
  double *forcing_values;
  s_id_spa *link_tmp;
  s_specie_unit_fp *pspeunit;
  s_specie_unit_fp **tab_speunit = pcarac_fp->p_species_unit[id_species];
  s_id_spa *link = pBU->link[id_species];
  s_forcing_bound_fp ***pbound = pBU->pbound;

  // Watbal resets
  RSV_reinit_matter_fluxes_bal(pBU->pwat_bal, id_species);
  FP_reinit_input_mflux(pBU->input_mflux, id_species);

  // Fetching the ele_bu water heights (mm) - Conversion into water volume (m3 per timestep)
  vinf = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_INFILT], pBU->area, dt, SEC_TS, flog);
  vruis = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_RUIS], pBU->area, dt, SEC_TS, flog);

  // Calculation molar masses ratio for element to specie conversion
  if (mmass_ele != CODE_FP)
    fact_conv = mmass_spe / mmass_ele;

  // Calculating runoff and infiltration fluxes, for water and matter at the ele_bu scale accounting for intersections with specie units cells
  if (link != NULL) {
    link_tmp = SPA_browse_id(link, BEGINNING_SPA, flog);
    while (link_tmp != NULL) {
      coverage += link_tmp->prct;
      pspeunit = FP_check_is_ele_specie_unit_defined(tab_speunit, nb_speunit, link_tmp->id, flog);
      forcing_values = FP_get_forcing_values(t, pspeunit, flog);
      flux_runoff += ((link_tmp->prct * pBU->area) * forcing_values[FP_TRFLUX_RUNOFF] * fact_conv); // Runoff specie mass in g (not element !)
      flux_inf += ((link_tmp->prct * pBU->area) * forcing_values[FP_TRFLUX_INF] * fact_conv);       // Infiltrated specie mass in g
      eau_runoff += ((link_tmp->prct * pBU->area) * forcing_values[FP_WATER_RUNOFF]);               // Water runoff in m3
      eau_inf += ((link_tmp->prct * pBU->area) * forcing_values[FP_WATER_INF]);                     // Infiltrated water in m3
      link_tmp = link_tmp->next;
      free(forcing_values);
    }
  }

  // Mass balance shortcut
  pBU->input_mflux[id_species][FP_MFORCED] = (flux_runoff + flux_inf);
  remain_surf = fabs(coverage - 1);

  if (remain_surf > EPSILON_SURF_FP) {
    if (pbound != NULL) {
      if (pbound[id_species] != NULL) {
        if (pbound[id_species][FP_TRFLUX_RUNOFF] == NULL && pbound[id_species][FP_TRFLUX_INF] == NULL && pbound[id_species][FP_TRVAR_INF] == NULL && pbound[id_species][FP_TRVAR_RUNOFF] == NULL) {
          eau_runoff += (remain_surf * vruis); // Using CaWaQS water budget over the non-covered area of the ele_bu
          eau_inf += (remain_surf * vinf);
          // LP_warning(flog,"Specie ID %d ele_bu intern ID %d : Neither matter flux or concentration boundary have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. All matter fluxes set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2);
        }

        if ((pbound[id_species][FP_TRFLUX_RUNOFF] != NULL && pbound[id_species][FP_TRFLUX_INF] != NULL) || (pbound[id_species][FP_TRVAR_INF] != NULL && pbound[id_species][FP_TRVAR_RUNOFF] != NULL)) {
          LP_error(flog, "Specie ID %d : Concentration and matter flux-type boundary conditions have BOTH been defined for ele_bu intern ID %d. Make a decision !\n", id_species, pBU->id);
        }

        // In case the boundary is defined using matter flux
        if ((pbound[id_species][FP_TRFLUX_RUNOFF] != NULL) && (pbound[id_species][FP_TRFLUX_INF] != NULL)) {
          bound_inf = FP_get_boundary_value(t, pbound, id_species, FP_TRFLUX_INF, flog);
          bound_runoff = FP_get_boundary_value(t, pbound, id_species, FP_TRFLUX_RUNOFF, flog);
          flux_runoff += ((remain_surf * pBU->area) * bound_runoff * fact_conv); // masse ruissellée en g de specie
          flux_inf += ((remain_surf * pBU->area) * bound_inf * fact_conv);       // masse infiltrée en g de specie

          pBU->input_mflux[id_species][FP_MBOUND] += ((remain_surf * pBU->area * fact_conv) * (bound_runoff + bound_inf));

          eau_runoff += (remain_surf * vruis); // Using CaWaQS water budget over the non-covered area of the ele_bu
          eau_inf += (remain_surf * vinf);
        }

        // In case the boundary is defined using concentration
        if ((pbound[id_species][FP_TRVAR_INF] != NULL) && (pbound[id_species][FP_TRVAR_RUNOFF] != NULL)) {

          // Convertinf inf. and runoff concentrations in specie mass (g)
          bound_inf = FP_get_boundary_value(t, pbound, id_species, FP_TRVAR_INF, flog) * fact_conv * vinf * remain_surf;
          bound_runoff = FP_get_boundary_value(t, pbound, id_species, FP_TRVAR_RUNOFF, flog) * fact_conv * vruis * remain_surf;

          flux_runoff += bound_runoff; // Specie runoff mass (in g)
          flux_inf += bound_inf;       // Specie infiltrated mass (in g)

          eau_runoff += (remain_surf * vruis); // Using CaWaQS water budget over the non-covered area of the ele_bu
          eau_inf += (remain_surf * vinf);

          pBU->input_mflux[id_species][FP_MBOUND] += (bound_inf + bound_runoff);
        }
      } else {
        eau_runoff += (remain_surf * vruis); // Using CaWaQS water budget if no boundary defined
        eau_inf += (remain_surf * vinf);
        // LP_warning(flog,"Specie id %d ele_bu intern %d : No boundaries have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2);
      }
    } else {
      eau_runoff += (remain_surf * vruis); // Using CaWaQS water budget if no boundary defined
      eau_inf += (remain_surf * vinf);
      // LP_warning(flog,"Specie id %d ele_bu intern %d : No boundaries have been defined. Yet, %6.2f %% of surface remains to be associated with a forcing input. Matter flux set to 0 on this surface.\n",id_species,pBU->id,remain_surf*1.e2);
    }
  }

  // Storing fluxes and concentrations in watbal
  pBU->pwat_bal->trflux[id_species][RSV_INFILT] = flux_inf / pBU->area;  // g/m2
  pBU->pwat_bal->trflux[id_species][RSV_RUIS] = flux_runoff / pBU->area; // g/m2

  if (eau_inf > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_INFILT] = flux_inf / eau_inf; // g/m3
  if (eau_runoff > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_RUIS] = flux_runoff / eau_runoff; // g/m3

  // Overwriting the native CaWaQS hydro budget (which is stored in mm)
  pBU->pwat_bal->q[RSV_INFILT] = RSV_volume_to_mm(eau_inf, pBU->area, dt, SEC_TS, flog);  // mm
  pBU->pwat_bal->q[RSV_RUIS] = RSV_volume_to_mm(eau_runoff, pBU->area, dt, SEC_TS, flog); // mm

  for (j = 0; j < FP_FIN_TYPE; j++)
    pBU->input_mflux[id_species][j] /= pBU->area; // g/m2
}

/**
 * @brief Calculates all sub-root fluxes for one BU in 'FP_DISSOCIATED_TRVAR_WATER' forcing mode (for HEAT transfer)
 * @return -
 * @warning Function do not account for partially covered surface extension yet. To be updated.
 */
void FP_transport_heat_dissociated_trvar_water_one_BU(s_bal_unit_fp *pBU, s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {
  int j, nb_speunit = pcarac_fp->nb_specie_unit[id_species];
  double coverage = 0., remain_surf = 0., vinf = 0., vruis = 0., cinf = 0., crunoff = 0.;
  double flux_runoff = 0., flux_inf = 0., eau_runoff = 0., eau_inf = 0.;
  double bound_runoff = 0., bound_inf = 0.;
  double *forcing_values;
  s_id_spa *link_tmp;
  s_specie_unit_fp *pspeunit;
  s_specie_unit_fp **tab_speunit = pcarac_fp->p_species_unit[id_species];
  s_id_spa *link = pBU->link[id_species];
  s_forcing_bound_fp ***pbound = pBU->pbound;

  // Watbal resets
  RSV_reinit_matter_fluxes_bal(pBU->pwat_bal, id_species);
  FP_reinit_input_mflux(pBU->input_mflux, id_species);

  // Fetching the ele_bu water heights (mm) - Conversion into water volume (m3 per timestep)
  vinf = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_INFILT], pBU->area, dt, SEC_TS, flog);
  vruis = RSV_mm_to_volume(pBU->pwat_bal->q[RSV_RUIS], pBU->area, dt, SEC_TS, flog);

  // Calculating runoff and infiltration fluxes, for water and energy at the ele_bu scale while accounting for intersections with specie units cells
  if (link != NULL) {
    link_tmp = SPA_browse_id(link, BEGINNING_SPA, flog);
    while (link_tmp != NULL) {
      coverage += link_tmp->prct;
      pspeunit = FP_check_is_ele_specie_unit_defined(tab_speunit, nb_speunit, link_tmp->id, flog);
      forcing_values = FP_get_forcing_values(t, pspeunit, flog);

      eau_runoff += ((link_tmp->prct * pBU->area) * forcing_values[FP_WATER_RUNOFF]); // Runoff water volume in m3
      eau_inf += ((link_tmp->prct * pBU->area) * forcing_values[FP_WATER_INF]);       // Infiltrated water volume in m3

      flux_runoff += (forcing_values[FP_TRVAR_RUNOFF] * (forcing_values[FP_WATER_RUNOFF] * (link_tmp->prct * pBU->area) / (dt * TS_unit2sec(DAY_TS, flog))) * RHO_WATER * CP_WATER); // Runoff energy 'flux' in W
      flux_inf += (forcing_values[FP_TRVAR_INF] * (forcing_values[FP_WATER_INF] * (link_tmp->prct * pBU->area) / (dt * TS_unit2sec(DAY_TS, flog))) * RHO_WATER * CP_WATER);          // Infiltrated energy 'flux' in W

      link_tmp = link_tmp->next;
      free(forcing_values);
    }
  }

  // Adding forcing fluxes in MB.
  pBU->input_mflux[id_species][FP_MFORCED] = (flux_runoff + flux_inf);

  // Storing energy fluxes in watbal
  pBU->pwat_bal->trflux[id_species][RSV_INFILT] = flux_inf / pBU->area;  // W/m2
  pBU->pwat_bal->trflux[id_species][RSV_RUIS] = flux_runoff / pBU->area; // W/m2

  // Storing temperatures in watbal
  if (eau_inf > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_INFILT] = (flux_inf) / (eau_inf / (dt * TS_unit2sec(DAY_TS, flog)) * RHO_WATER * CP_WATER); // K
  if (eau_runoff > EPS_VOL_FP)
    pBU->pwat_bal->trvar[id_species][RSV_RUIS] = (flux_runoff) / (eau_runoff / (dt * TS_unit2sec(DAY_TS, flog)) * RHO_WATER * CP_WATER); // K

  // Overwriting the native CaWaQS hydro budget (which is stored in mm)
  pBU->pwat_bal->q[RSV_INFILT] = RSV_volume_to_mm(eau_inf, pBU->area, dt, SEC_TS, flog);  // mm
  pBU->pwat_bal->q[RSV_RUIS] = RSV_volume_to_mm(eau_runoff, pBU->area, dt, SEC_TS, flog); // mm

  for (j = 0; j < FP_FIN_TYPE; j++)
    pBU->input_mflux[id_species][j] /= pBU->area;
}