/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        itos.c
 * @brief       Interger-to-string conversion functions
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Fetches climate input data name
 * @return Corresponding name if found
 */
char *FP_name_mto(int iname) {

  char *name;

  switch (iname) {
  case FP_ETP: {
    name = strdup("ETP");
    break;
  }
  case FP_RAIN: {
    name = strdup("RAINFALL");
    break;
  }
  default: {
    name = strdup("Unknown MTO parameter type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches production function calibration parameter name
 * @return Corresponding name if found
 */
char *FP_name_param(int iname) {
  char *name;

  switch (iname) {
  case FP_DCRT: {
    name = strdup("DCRT");
    break;
  }
  case FP_CRT: {
    name = strdup("CRT");
    break;
  }
  case FP_RMAX: {
    name = strdup("RMAX");
    break;
  }
  case FP_FN: {
    name = strdup("FN");
    break;
  }
  case FP_QRMAX: {
    name = strdup("QRMAX");
    break;
  }
  case FP_CQR: {
    name = strdup("CQR");
    break;
  }
  case FP_QIMAX: {
    name = strdup("QIMAX");
    break;
  }
  case FP_CQI: {
    name = strdup("CQI");
    break;
  }
  case FP_LCRT: {
    name = strdup("LAI_CRT");
    break;
  }
  case FP_LDCR: {
    name = strdup("LAI_DCRT");
    break;
  }
  default: {
    name = strdup("Unknown production-function parameter type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches water balance storage term name
 * @return Corresponding name if found
 */
char *FP_name_stock(int iname) {

  char *name;

  switch (iname) {
  case FP_RUIS_STCK: {
    name = strdup("STOCK_IN_RUISS");
    break;
  }
  case FP_INFILT_STCK: {
    name = strdup("STOCK_IN_INFILT");
    break;
  }
  case FP_SOIL_STCK: {
    name = strdup("STOCK_IN_SOIL");
    break;
  }
  default: {
    name = strdup("Not param");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches catchment cell prod output type name
 * @return Corresponding name if found
 */
char *FP_cprod_type(int itype) {
  char *name;

  switch (itype) {
  case FP_CATCH_RIV: {
    name = strdup("River cell");
    break;
  }
  case FP_CATCH_CHASM: {
    name = strdup("Chasm cell");
    break;
  }
  default: {
    name = strdup("Unknown production cell type");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches transient input file set name
 * @return Corresponding name if found
 */
char *FP_input_types(int itype) {
  char *name;

  switch (itype) {
  case FP_MTO: {
    name = strdup("Climate inputs");
    break;
  }
  case FP_LAI: {
    name = strdup("LAI inputs");
    break;
  }
  default: {
    name = strdup("Unknown FP input type");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches catchment type name
 * @return Corresponding name if found
 */
char *FP_catchment_type(int itype) {
  char *name;

  switch (itype) {
  case FP_CATCH_RIV: {
    name = strdup("River catchment");
    break;
  }
  case FP_CATCH_CHASM: {
    name = strdup("Chasm catchment");
    break;
  }
  default: {
    name = strdup("Unknown catchment type");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches transport specie type name
 * @return Corresponding name if found
 */
char *FP_transport_spetype(int itype) {
  char *name;

  switch (itype) {
  case FP_SOLUTE: {
    name = strdup("SOLUTE");
    break;
  }
  case FP_HEAT: {
    name = strdup("HEAT");
    break;
  }
  default: {
    name = strdup("Unknown transport specie type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches external forcing mode name
 * @return Corresponding name if found
 */
char *FP_transport_forcing_conditions(int itype) {
  char *name;

  switch (itype) {
  case FP_TOTAL_TRFLUX: {
    name = strdup("External total flux (matter/energy)");
    break;
  }
  case FP_DISSOCIATED_TRFLUX: {
    name = strdup("External dissociated infiltrated and run-off fluxes (matter/energy)");
    break;
  }
  case FP_TOTAL_TRFLUX_WATER: {
    name = strdup("External total fluxes (matter/energy) and water");
    break;
  }
  case FP_DISSOCIATED_TRFLUX_WATER: {
    name = strdup("External dissociated infiltrated and run-off fluxes (matter/energy) and water");
    break;
  }
  case FP_TOTAL_TRVAR: {
    name = strdup("External total concentration/temperature");
    break;
  }
  case FP_TOTAL_TRVAR_WATER: {
    name = strdup("External concentration/temperature and water");
    break;
  }
  case FP_DISSOCIATED_TRVAR_WATER: {
    name = strdup("External dissociated infiltrated and run-off concentration/temperature and water");
    break;
  }
  default: {
    name = strdup("Unknown forcing type for surface transport");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches external forcing variable type name
 * @return Corresponding name if found
 */
char *FP_transport_input_variable_name(int itype) {
  char *name;

  switch (itype) {
  case FP_TRFLUX: {
    name = strdup("Total flux (matter/energy)");
    break;
  }
  case FP_WATER: {
    name = strdup("Total water");
    break;
  }
  case FP_TRVAR: {
    name = strdup("Total concentration/temperature");
    break;
  }
  case FP_TRFLUX_INF: {
    name = strdup("Infiltrated flux (matter/energy)");
    break;
  }
  case FP_TRFLUX_RUNOFF: {
    name = strdup("Run-off flux (matter/energy)");
    break;
  }
  case FP_WATER_INF: {
    name = strdup("External infiltrated water");
    break;
  }
  case FP_WATER_RUNOFF: {
    name = strdup("External run-off water");
    break;
  }
  case FP_TRVAR_INF: {
    name = strdup("Infiltrated concentration/temperature");
    break;
  }
  case FP_TRVAR_RUNOFF: {
    name = strdup("Run-off concentration/temperature");
    break;
  }
  default: {
    name = strdup("Unknown input forcing variable type for transport.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches short name for external forcing variable type
 * @return Corresponding name if found
 */
char *FP_transport_outvar_shortname(int itype) {
  char *name;

  switch (itype) {
  case FP_TRFLUX: {
    name = strdup("FLUX_TOT");
    break;
  }
  case FP_WATER: {
    name = strdup("WATER_TOT");
    break;
  }
  case FP_TRVAR: {
    name = strdup("VAR_TOT");
    break;
  }
  case FP_TRFLUX_INF: {
    name = strdup("FLUX_INF");
    break;
  }
  case FP_TRFLUX_RUNOFF: {
    name = strdup("FLUX_RUNOFF");
    break;
  }
  case FP_WATER_INF: {
    name = strdup("WATER_INF");
    break;
  }
  case FP_WATER_RUNOFF: {
    name = strdup("WATER_RUNOFF");
    break;
  }
  case FP_TRVAR_INF: {
    name = strdup("VAR_INF");
    break;
  }
  case FP_TRVAR_RUNOFF: {
    name = strdup("VAR_RUNOFF");
    break;
  }
  case FP_TRFLUX_DIRECT: {
    name = strdup("FLUX_DIRECT");
    break;
  }
  case FP_TRVAR_DIRECT: {
    name = strdup("VAR_DIRECT");
    break;
  }
  default: {
    name = strdup("Unknown input forcing variable type for transport.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches external forcing boundary type name
 * @return Corresponding name if found
 */
char *FP_transport_input_mflux(int itype) {
  char *name;

  switch (itype) {
  case FP_MFORCED: {
    name = strdup("FLUX_FORCED");
    break;
  }
  case FP_MBOUND: {
    name = strdup("FLUX_BOUND");
    break;
  }
  default: {
    name = strdup("Unknown transport input flux type.");
    break;
  }
  }
  return name;
}

/**
 * @brief Fetches forcing time serie type name
 * @return Corresponding name if found
 */
char *FP_boundary_frequency(int itype) {
  char *name;

  switch (itype) {
  case FP_CST: {
    name = strdup("Constant boundary");
    break;
  }
  case FP_VAR: {
    name = strdup("Variable boundary");
    break;
  }
  default: {
    name = strdup("Unknown boundary frequency parameter.");
    break;
  }
  }
  return name;
}