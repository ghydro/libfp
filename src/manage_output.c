/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_output.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_output.c
 * @brief       Functions dealing with output files and objects
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Prints the header for WATBAL output files (FORMATTED format)
 * @return None
 */
void FP_print_header(FILE *fp) {
  int j;
  char *name;

  fprintf(fp, "%s  %s  %s  ", "DAY", "ID_GIS", "ID_INTERN");

  for (j = 0; j < FP_NMETEO; j++) {
    name = FP_name_mto(j);
    fprintf(fp, "%s(m3/s)  ", name);
    free(name);
  }
  for (j = 0; j < RSV_NTYPE; j++) {
    if ((j == RSV_RUIS || j == RSV_INFILT) || (j == RSV_ETR || j == RSV_DIRECT)) {
      name = RSV_name_res(j);
      fprintf(fp, "%s(m3/s)  ", name);
      free(name);
    }
  }
  for (j = 0; j < FP_NSTCK; j++) {
    name = FP_name_stock(j);
    fprintf(fp, "%s(m3/s)  ", name);
    free(name);
  }
  fprintf(fp, "ERROR(m3/s)  ");
  fprintf(fp, "\n");
}

/**
 * @brief Prints out the header of WATBAL_TRANSPORT output file in formatted format.
 * Accounts for specie type and multispecies.
 * @return None
 */
void FP_print_header_transport(int nb_species, int *spe_types, FILE *fp) {
  int p;

  fprintf(fp, "%s  %s  %s  ", "DAY", "ID_GIS", "ID_INTERN");

  for (p = 0; p < nb_species; p++) {
    if (spe_types[p] == FP_SOLUTE) {
      fprintf(fp, "%s(g/m2) ", FP_transport_input_mflux(FP_MFORCED));
      fprintf(fp, "%s(g/m2) ", FP_transport_input_mflux(FP_MBOUND));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_INFILT));
      fprintf(fp, "%s(g/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_INF));
      fprintf(fp, "%s(g/m3) ", FP_transport_outvar_shortname(FP_TRVAR_INF));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_RUIS));
      fprintf(fp, "%s(g/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_RUNOFF));
      fprintf(fp, "%s(g/m3) ", FP_transport_outvar_shortname(FP_TRVAR_RUNOFF));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_DIRECT));
      fprintf(fp, "%s(g/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_DIRECT));
      fprintf(fp, "%s(g/m3) ", FP_transport_outvar_shortname(FP_TRVAR_DIRECT));
      fprintf(fp, "ERROR(g/m2) ");
    } else { // FP_HEAT case
      fprintf(fp, "%s(W/m2) ", FP_transport_input_mflux(FP_MFORCED));
      fprintf(fp, "%s(W/m2) ", FP_transport_input_mflux(FP_MBOUND));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_INFILT));
      fprintf(fp, "%s(W/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_INF));
      fprintf(fp, "%s(°C) ", FP_transport_outvar_shortname(FP_TRVAR_INF));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_RUIS));
      fprintf(fp, "%s(W/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_RUNOFF));
      fprintf(fp, "%s(°C) ", FP_transport_outvar_shortname(FP_TRVAR_RUNOFF));
      fprintf(fp, "%s(m3/s) ", RSV_name_res(RSV_DIRECT));
      fprintf(fp, "%s(W/m2) ", FP_transport_outvar_shortname(FP_TRFLUX_DIRECT));
      fprintf(fp, "%s(°C) ", FP_transport_outvar_shortname(FP_TRVAR_DIRECT));
      fprintf(fp, "ERROR(W/m2) ");
    }
  }
  fprintf(fp, "\n");
}

/**
 * @brief General function launching hydro WATBAL output file printing.
 * @return None
 */
void FP_write_mb_hydro(int spatial_scale, double t, double dt, double t_day, s_out_io *pout, s_carac_fp *pcarac_fp, FILE *fp) {

  FILE *fout = pout->fout;
  double t_out;

  t_out = pout->t_out[CUR_IO];

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      FP_write_mb_hydro_formatted(spatial_scale, pcarac_fp, t_day, pout, dt, fp);
    } else {
      FP_write_mb_hydro_bin(spatial_scale, pcarac_fp, t_day, pout, dt, fp);
    }
  }
}

/**
 * @brief Writes WATBAL_MB hydro mass balance at the selected unit scale in a text file for all elements
 * @return -
 */
void FP_write_mb_hydro_formatted(int spatial_scale, s_carac_fp *pcarac_fp, double t_day, s_out_io *pout, double dt, FILE *fp) {

  int i, j;
  double *mb_values;
  s_id_io *id_out;
  s_input_fp *pinputs = pcarac_fp->pinputs;

  if (pout->type_out == ALL_IO) {
    if (spatial_scale == FP_ELE_BU_IO) {
      for (i = 0; i < pcarac_fp->nb_BU; i++)
        FP_print_budget_ele_bu(pcarac_fp->p_bu[i], pinputs, t_day, dt, pout, fp);
    } else // --> CPROD scale by default
    {
      for (i = 0; i < pcarac_fp->nb_cprod; i++)
        FP_print_budget_cprod(pcarac_fp->p_cprod[i], pinputs, t_day, pout, fp);
    }
  } else // If a selection mask is activated
  {
    id_out = pout->id_output;
    while (id_out != NULL) {
      i = id_out->id; // i is the element intern_id
      if (spatial_scale == FP_ELE_BU_IO) {
        FP_print_budget_ele_bu(pcarac_fp->p_bu[i], pinputs, t_day, dt, pout, fp);
      } else {
        FP_print_budget_cprod(pcarac_fp->p_cprod[i], pinputs, t_day, pout, fp);
      }

      id_out = id_out->next;
    }
  }
}

/**
 * @brief Computes and prints water budget for one bu_element in formatted format
 * @return none
 */
void FP_print_budget_ele_bu(s_bal_unit_fp *pbu, s_input_fp *pinputs, double t_day, double dt, s_out_io *pout, FILE *fp) {

  double *mb_values;
  int j;

  fprintf(pout->fout, "%f  %d  %d ", t_day, pbu->id + 1, pbu->id);
  mb_values = FP_ele_bu_budget(pbu, pinputs, t_day, pout, dt, fp);
  for (j = 0; j < NB_MB_VAL_FP; j++)
    fprintf(pout->fout, "%15.5e ", mb_values[j]);
  fprintf(pout->fout, "\n");
  free(mb_values);
}

/**
 * @brief Computes and prints water budget for one cprod cell in formatted format
 * @return none
 */
void FP_print_budget_cprod(s_cprod_fp *pcprod, s_input_fp *pinputs, double t_day, s_out_io *pout, FILE *fp) {

  double *mb_values;
  int j;

  fprintf(pout->fout, "%f  %d  %d  ", t_day, pcprod->id[FP_GIS], pcprod->id[FP_INTERN]);
  mb_values = FP_cprod_budget(pcprod, pinputs, pout, fp);

  for (j = 0; j < NB_MB_VAL_FP; j++)
    fprintf(pout->fout, "%15.5e  ", mb_values[j]);
  fprintf(pout->fout, "\n");
  free(mb_values);
}

/**
 * @brief Computes a hydro budget for a bal_unit.
 * @return Table of doubles containing all values of the water budget (values in m3/s)
 */
double *FP_ele_bu_budget(s_bal_unit_fp *pbu, s_input_fp *pinputs, double t_day, s_out_io *pout, double dt, FILE *fp) {

  int i, j;
  double *mb_values;
  int pos = 0;
  double *mto_values, *stock_values;
  double error = 0.;
  double area = pbu->area;
  double qruis = 0., qinf = 0., etr = 0., dsout = 0.;
  s_wat_bal_rsv *pwatbal = pbu->pwat_bal;
  s_id_spa *link;

  // Fetching transport mode if transport is on
  int forcing_mode = CODE_FP;
  if (pinputs->transport_forcing_conditions != NULL) {
    forcing_mode = pinputs->transport_forcing_conditions[0];
  }

  mb_values = (double *)malloc(NB_MB_VAL_FP * sizeof(double));
  if (mb_values == NULL)
    LP_error(fp, "In libfp%d : File %s, function %s at line %d : Bad memory allocation while writing mass balance.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  bzero(mb_values, NB_MB_VAL_FP * sizeof(double));

  mto_values = FP_get_mto(t_day, pbu->pmto, fp); // mm

  for (j = 0; j < FP_NMETEO; j++) {
    mto_values[j] = RSV_mm_to_q(mto_values[j], area, dt, SEC_TS, fp); // conversion to m3/s
    mb_values[pos] = mto_values[j];
    pos++;
  }

  error += mto_values[FP_RAIN];
  free(mto_values);

  // Reservoirs values in m3/s
  qruis = RSV_mm_to_q(pwatbal->q[RSV_RUIS], area, dt, SEC_TS, fp);
  error -= qruis;
  mb_values[pos] = qruis;
  pos++;

  qinf = RSV_mm_to_q(pwatbal->q[RSV_INFILT], area, dt, SEC_TS, fp);
  error -= qinf;
  mb_values[pos] = qinf;
  pos++;

  etr = RSV_mm_to_q(pwatbal->q[RSV_ETR], area, dt, SEC_TS, fp);
  error -= etr;
  mb_values[pos] = etr;
  pos++;

  /* NG : 12/06/2023 : MEMO - For a print at the ELE_BU scale, the RSV_DIRECT field
   * is left to zeros. Corresponding flux value is left as infiltration. Mentioned in user guide. */
  dsout = RSV_mm_to_q(pwatbal->q[RSV_DIRECT], area, dt, SEC_TS, fp);
  error -= dsout;
  mb_values[pos] = dsout;
  pos++;

  // Storage in m3/s
  stock_values = pbu->stock;
  for (j = 0; j < FP_NSTCK; j++) {
    stock_values[j] = RSV_mm_to_q(stock_values[j], area, dt, SEC_TS, fp); // conversion to m3/s
    error -= stock_values[j];
    mb_values[pos] = stock_values[j];
    pos++;
  }

  mb_values[pos] = error;

  /* NG : 07/11/2024 : Overwriting by-passed water balance terms in case transport with forced water fluxes
  is ON, to avoid confusion in the MB output file. */
  if (forcing_mode == FP_TOTAL_TRFLUX_WATER || forcing_mode == FP_DISSOCIATED_TRFLUX_WATER || forcing_mode == FP_TOTAL_TRVAR_WATER || forcing_mode == FP_DISSOCIATED_TRVAR_WATER) {
    mb_values[FP_MB_RAIN] = CODE_FP;
    mb_values[FP_MB_PET] = CODE_FP;
    mb_values[FP_MB_AET] = CODE_FP;
    mb_values[FP_MB_STOCK_ROFF] = CODE_FP;
    mb_values[FP_MB_STOCK_SOIL] = CODE_FP;
    mb_values[FP_MB_STOCK_INF] = CODE_FP;
    mb_values[FP_MB_ERROR] = CODE_FP;
  }

  return mb_values;
}

/**
 * @brief Computes a hydro budget for a cprod unit.
 * @return Table of double containing all values of the budget (Output in m3/s).
 */
double *FP_cprod_budget(s_cprod_fp *pcprod, s_input_fp *pinputs, s_out_io *pout, FILE *fp) {

  int i, j;
  double error = 0.;
  int pos = 0;
  double *mb_values;
  s_wat_bal_rsv *pwatbal = pcprod->pwat_bal;

  // Fetching transport mode if transport is on
  int forcing_mode = CODE_FP;
  if (pinputs->transport_forcing_conditions != NULL) {
    forcing_mode = pinputs->transport_forcing_conditions[0];
  }

  mb_values = (double *)malloc(NB_MB_VAL_FP * sizeof(double));
  if (mb_values == NULL)
    LP_error(fp, "In libfp%d : File %s, function %s at line %d : Bad memory allocation while writing mass balance.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  bzero(mb_values, NB_MB_VAL_FP * sizeof(double));

  for (j = 0; j < FP_NMETEO; j++) {
    mb_values[pos] = pcprod->mto[j];
    pos++;
  }

  error += pcprod->mto[FP_RAIN];

  mb_values[pos] = pwatbal->q[RSV_RUIS];
  pos++;
  error -= pwatbal->q[RSV_RUIS];
  mb_values[pos] = pwatbal->q[RSV_INFILT];
  pos++;
  error -= pwatbal->q[RSV_INFILT];
  mb_values[pos] = pwatbal->q[RSV_ETR];
  pos++;
  error -= pwatbal->q[RSV_ETR];
  mb_values[pos] = pwatbal->q[RSV_DIRECT];
  pos++;
  error -= pwatbal->q[RSV_DIRECT];

  for (j = 0; j < FP_NSTCK; j++) {
    mb_values[pos] = pcprod->stock[j];
    error -= pcprod->stock[j];
    pos++;
  }

  mb_values[pos] = error;

  /* NG : 07/11/2024 : Overwriting by-passed water balance terms in case transport with forced water fluxes
  is ON, to avoid confusion in the MB output file. */
  if (forcing_mode == FP_TOTAL_TRFLUX_WATER || forcing_mode == FP_DISSOCIATED_TRFLUX_WATER || forcing_mode == FP_TOTAL_TRVAR_WATER || forcing_mode == FP_DISSOCIATED_TRVAR_WATER) {
    mb_values[FP_MB_RAIN] = CODE_FP;
    mb_values[FP_MB_PET] = CODE_FP;
    mb_values[FP_MB_AET] = CODE_FP;
    mb_values[FP_MB_STOCK_ROFF] = CODE_FP;
    mb_values[FP_MB_STOCK_SOIL] = CODE_FP;
    mb_values[FP_MB_STOCK_INF] = CODE_FP;
    mb_values[FP_MB_ERROR] = CODE_FP;
  }

  return mb_values;
}

/**
 * @brief Writes hydro FP mass balance at each time step in unformatted format
 */
void FP_write_mb_hydro_bin(int spatial_scale, s_carac_fp *pcarac_fp, double t_day, s_out_io *pout, double dt, FILE *fp) {

  int type_out = pout->type_out;
  double **val;
  int neletot;
  FILE *fpout = pout->fout;

  switch (spatial_scale) {
  case FP_ELE_BU_IO:
    neletot = pcarac_fp->nb_BU;
    break;
  case FP_CPROD_IO:
    neletot = pcarac_fp->nb_cprod;
    break;
  case CODE_IO: // = Print at the CPROD scale by default if left undefined.
    neletot = pcarac_fp->nb_cprod;
    break;
  default:
    LP_error(fpout, "libfp%4.2f, File %s in %s line %d : Selected spatial scale type incoherent with the FP module outputs.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    break;
  }

  if (type_out != ALL_IO)
    neletot = IO_length_id(pout->id_output);

  val = FP_get_mb_val_hydro_bin(spatial_scale, neletot, pout, pcarac_fp, t_day, type_out, dt, fp);
  IO_print_bloc_bin(val, neletot, NB_MB_VAL_FP, fpout);
}

/**
 * @brief Builds the matrix of all values of the water budget, for all elements and all records at each time step at the selected spatial scale
 * @return Matrix of doubles of all values
 */
double **FP_get_mb_val_hydro_bin(int spatial_scale, int neletot, s_out_io *pout, s_carac_fp *pcarac_fp, double t_day, int type_out, double dt, FILE *fp) {
  double **val;
  int i, j;
  double *mb_values;
  s_bal_unit_fp *pbu;
  s_cprod_fp *pcprod;
  s_id_io *id_out;
  s_input_fp *pinputs = pcarac_fp->pinputs;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
#endif

  val = (double **)malloc(NB_MB_VAL_FP * sizeof(double *));
  for (i = 0; i < NB_MB_VAL_FP; i++)
    val[i] = (double *)malloc(neletot * sizeof(double));

  if (type_out == ALL_IO) {
#ifdef OMP
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fp, neletot, nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(neletot, pcarac_fp, val) private(i, j, mb_values, pbu, pcprod)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (i = 0; i < neletot; i++) {
        if (spatial_scale == FP_ELE_BU_IO) {
          pbu = pcarac_fp->p_bu[i];
          mb_values = FP_ele_bu_budget(pbu, pinputs, t_day, pout, dt, fp);
        } else {
          pcprod = pcarac_fp->p_cprod[i];
          mb_values = FP_cprod_budget(pcprod, pinputs, pout, fp);
        }
        for (j = 0; j < NB_MB_VAL_FP; j++)
          val[j][i] = mb_values[j];
        free(mb_values);
      }
#ifdef OMP
    } // End of parallel section
#endif
  } else {
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;

      if (spatial_scale == FP_ELE_BU_IO) {
        pbu = pcarac_fp->p_bu[i];
        mb_values = FP_ele_bu_budget(pbu, pinputs, t_day, pout, dt, fp);
      } else {
        pcprod = pcarac_fp->p_cprod[i];
        mb_values = FP_cprod_budget(pcprod, pinputs, pout, fp);
      }
      for (j = 0; j < NB_MB_VAL_FP; j++)
        val[j][i] = mb_values[j];
      free(mb_values);
      id_out = id_out->next;
    }
  }
  return val;
}

/**
 * @brief Prints the WATBAL_corresp_file, with geometry info according to the selected geometry
 * (i.e. either water balance unit or cprod).
 * @return None
 * @internal  NG : 31/08/2021 : Introducing spatial scale so the infos printed in
 * WATBAL_corresp_file.txt are coherent with the output spatial scale the user
 * asked for in the COMM file. Area info added. @endinternal
 */
void FP_print_corresp(s_carac_fp *pcarac_fp, int mod, int spatial_scale, FILE *fp) {
  int neletot, i;
  char *name_mod, *name, *name_scale;
  s_cprod_fp *cprod;
  s_bal_unit_fp *pbu;
  FILE *fpout;

  if (spatial_scale == CODE_IO) {
    spatial_scale = FP_CPROD_IO; // Print at the CPORD scale if left undefined
  }

  name_mod = IO_name_mod(mod);
  name_scale = IO_geometry_short_name(spatial_scale);

  name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/%s_%s_corresp_file.txt", getenv("RESULT"), name_mod, name_scale);
  fpout = fopen(name, "w");
  free(name_mod);
  free(name_scale);

  if (fpout == NULL)
    LP_error(fp, "In libfp%4.2f : Error in file %s, function %s : Cannot open file %s.", VERSION_FP, __FILE__, __func__, name);

  switch (spatial_scale) {
  case FP_CPROD_IO: {
    fprintf(fpout, "ID_INTERN ID_ABS ID_GIS AREA\n");
    neletot = pcarac_fp->nb_cprod;
    for (i = 0; i < neletot; i++) {
      cprod = pcarac_fp->p_cprod[i];
      fprintf(fpout, "%d %d %d %f\n", cprod->id[FP_INTERN], cprod->id[FP_ABS], cprod->id[FP_GIS], cprod->area);
    }
    break;
  }
  case FP_ELE_BU_IO: {
    neletot = pcarac_fp->nb_BU;
    fprintf(fpout, "ID_INTERN AREA\n");
    for (i = 0; i < neletot; i++) {
      pbu = pcarac_fp->p_bu[i];
      fprintf(fpout, "%d %f\n", pbu->id, pbu->area);
    }
    break;
  }
  default:
    LP_error(fpout, "libfp%4.2f, File %s in %s : Spatial scale type incoherent with the FP module outputs.\n", VERSION_FP, __FILE__, __func__);
    break;
  }

  fclose(fpout);
  free(name);
}

/**
 * \brief Print out the WATBAL TRANSPORT corresp file for all the species forcing grids
 * \return None
 */
void FP_print_corresp_all_species(s_carac_fp *pcarac_fp, int nb_species, int mod, FILE *flog) {
  int i, j, nb_species_units;
  char *name_mod, *name;
  FILE *fpout = NULL;
  s_specie_unit_fp *p_speunit = NULL;

  name_mod = IO_name_mod(mod);

  for (i = 0; i < nb_species; i++) {

    name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));
    sprintf(name, "%s/%s_species_%d_corresp_file.txt", getenv("RESULT"), name_mod, i);
    fpout = fopen(name, "w");

    if (fpout == NULL)
      LP_error(flog, "Cannot open file %s", name);

    fprintf(fpout, "ID_INTERN ID_ABS ID_GIS AREA_m2\n");
    nb_species_units = pcarac_fp->nb_specie_unit[i];
    for (j = 0; j < nb_species_units; j++) {
      p_speunit = pcarac_fp->p_species_unit[i][j];
      fprintf(fpout, "%d %d %d %.2f\n", p_speunit->id[FP_INTERN], p_speunit->id[FP_ABS], p_speunit->id[FP_GIS], p_speunit->area); // NG : 13/04/2023 : Area (m2) added for more convenience.
    }
    fclose(fpout);
    free(name);
  }
  free(name_mod);
}

/**
 * @brief Print short abstract for hydro objects in the log file
 * @return None
 */
void FP_print_abstract(s_carac_fp *pchar_fp, FILE *flog) {

  int i, j;
  int nb_cprod, nb_BU, nb_FP, nb_mto;
  double Surf_cprod = 0, Surf_BU = 0, Surf_mto = 0;
  nb_cprod = pchar_fp->nb_cprod;
  nb_BU = pchar_fp->nb_BU;
  nb_mto = pchar_fp->pinputs->nb_mto;
  nb_FP = pchar_fp->nb_FP;
  for (i = 0; i < nb_cprod; i++) {
    Surf_cprod += pchar_fp->p_cprod[i]->area;
  }

  for (i = 0; i < nb_BU; i++) {
    Surf_BU += pchar_fp->p_bu[i]->area;
  }

  for (i = 0; i < nb_mto; i++) {
    Surf_mto += pchar_fp->pinputs->mto[i]->area;
  }

  LP_printf(flog, "\t --> Number of watersheds in the basin : %d \n", nb_cprod);
  LP_printf(flog, "Surface : %f m2\n", Surf_cprod);
  LP_printf(flog, "\t --> Number of water balance calculation units : %d\n", nb_BU);
  LP_printf(flog, "Surface : %f m2\n", Surf_BU);
  LP_printf(flog, "\t --> Number of meteo cells : %d\n", nb_mto);
  LP_printf(flog, "Surface : %f m2\n", Surf_mto);
  LP_printf(flog, "\t --> Number of production functions : %d\n", nb_FP);
  LP_printf(flog, "Parameters : \n");
  FP_print_all_FP_param(pchar_fp, flog);
}

/**
 * @brief Print out the abstract for libfp objets used in the hyperdermic module
 * @return None
 */
void FP_print_FP_hyperdermic_abstract(s_carac_fp *pchar_fp_hderm, FILE *flog) {

  int i, nb_cprod = pchar_fp_hderm->nb_cprod;
  int nb_catch = pchar_fp_hderm->nb_catchment;
  int nbcatch_recalc = 0;
  double treshold = pchar_fp_hderm->thresh_mean_surf;
  double surf_cprod_tot = 0., surf_catch_tot = 0.;
  double min_tc = BIG_FP;
  double max_tc = SMALL_FP;
  s_catchment_fp *pcatch, *pcatch_tmp;
  s_cprod_fp *pcprod;

  for (i = 0; i < nb_cprod; i++) {
    pcprod = pchar_fp_hderm->p_cprod[i];
    surf_cprod_tot += pcprod->area;
  }

  pcatch_tmp = FP_browse_catchment_chain(pchar_fp_hderm->p_catchment, BEGINNING_IO, flog);
  while (pcatch_tmp != NULL) {
    surf_catch_tot += pcatch_tmp->area;
    if (pcatch_tmp->area > treshold)
      nbcatch_recalc++;
    if (pcatch_tmp->concentration_time > max_tc)
      max_tc = pcatch_tmp->concentration_time;
    if (pcatch_tmp->concentration_time < min_tc)
      min_tc = pcatch_tmp->concentration_time;
    pcatch_tmp = pcatch_tmp->next;
  }

  LP_printf(flog, "\t --> Number of Cprod cells of the hyperdermic zone : %d \n", nb_cprod);
  LP_printf(flog, "Surface : %f m2\n", surf_cprod_tot);
  LP_printf(flog, "\t --> Number of catchments of the hyperdermic zone : %d \n", nb_catch);
  LP_printf(flog, "Surface : %f m2\n", surf_catch_tot);

  LP_printf(flog, "\t --> Number of catchments bigger than the threshold surface of %.1f m2 (i.e. Tc recalibrated) : %d \n", treshold, nbcatch_recalc);
  LP_printf(flog, "\t --> Minimum and maximum concentration times for all hyperdermic catchments : %.1f days, %.1f days\n", min_tc / TS_unit2sec(DAY_TS, flog), max_tc / TS_unit2sec(DAY_TS, flog));
}

/**
 * @brief Launches general printing for WATBAL_TRANSPORT output file.
 * @return None
 */
void FP_write_mb_transport(int nb_species, int spatial_scale, double t, double dt, double t_day, s_out_io *pout, s_carac_fp *pcarac_fp, FILE *fp) {

  FILE *fout = pout->fout;
  double t_out = pout->t_out[CUR_IO];

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      FP_write_mb_transport_formatted(nb_species, spatial_scale, pcarac_fp, t_day, pout, dt, fp);
    } else {
      FP_write_mb_transport_bin(nb_species, spatial_scale, pcarac_fp, pout, dt, fp);
    }
  }
}

/**
 * @brief Writes transport output file for fp module at the selected unit scale in a text file
 * @return -
 */
void FP_write_mb_transport_formatted(int nb_species, int spatial_scale, s_carac_fp *pcarac_fp, double t_day, s_out_io *pout, double dt, FILE *fp) {
  int i, j, p;
  s_bal_unit_fp *pbu;
  s_wat_bal_rsv *pwatbal;
  s_cprod_fp *pcprod;
  s_id_io *id_out;
  double *mb_values;
  int nb_rec = NB_MB_VAL_TRANSPORT_FP * nb_species;

  if (pout->type_out == ALL_IO) {
    if (spatial_scale == FP_ELE_BU_IO) {
      for (i = 0; i < pcarac_fp->nb_BU; i++) {
        pbu = pcarac_fp->p_bu[i];
        fprintf(pout->fout, "%7.1f  %d  %d  ", t_day, pbu->id + 1, pbu->id);
        mb_values = FP_transport_budget_ele_bu(pbu, nb_species, dt, fp);
        for (j = 0; j < nb_rec; j++)
          fprintf(pout->fout, "%13.4e  ", mb_values[j]);
        free(mb_values);
        fprintf(pout->fout, "\n");
      }
    } else // All CPROD
    {
      for (i = 0; i < pcarac_fp->nb_cprod; i++) {
        pcprod = pcarac_fp->p_cprod[i];
        fprintf(pout->fout, "%7.1f  %d  %d  ", t_day, pcprod->id[FP_GIS], pcprod->id[FP_INTERN]);
        mb_values = FP_transport_budget_cprod(pcprod, nb_species, fp);
        for (j = 0; j < nb_rec; j++)
          fprintf(pout->fout, "%13.4e  ", mb_values[j]);
        free(mb_values);
        fprintf(pout->fout, "\n");
      }
    }
  } else // Print over an extraction mask
  {
    id_out = pout->id_output;

    while (id_out != NULL) {
      i = id_out->id;

      if (spatial_scale == FP_ELE_BU_IO) {
        pbu = pcarac_fp->p_bu[i];
        fprintf(pout->fout, "%f  %d  %d  ", t_day, pbu->id + 1, pbu->id);
        mb_values = FP_transport_budget_ele_bu(pbu, nb_species, dt, fp);
        for (j = 0; j < nb_rec; j++)
          fprintf(pout->fout, "%f  ", mb_values[j]);
        free(mb_values);
        fprintf(pout->fout, "\n");
      } else // All CPRODs
      {
        pcprod = pcarac_fp->p_cprod[i];
        fprintf(pout->fout, "%f  %d  %d  ", t_day, pcprod->id[FP_GIS], pcprod->id[FP_INTERN]);
        mb_values = FP_transport_budget_cprod(pcprod, nb_species, fp);
        for (j = 0; j < nb_rec; j++)
          fprintf(pout->fout, "%f  ", mb_values[j]);
        free(mb_values);
        fprintf(pout->fout, "\n");
      }

      id_out = id_out->next;
    }
  }
}

/**
 * @brief Computes transport matter mass balance at the ele_bu spatial scale for one time step
 * @return Table of doubles containing all budget values for all species
 */
double *FP_transport_budget_ele_bu(s_bal_unit_fp *pbu, int nb_species, double dt, FILE *fp) {
  int p, j;
  int pos = 0;
  s_wat_bal_rsv *pwatbal = pbu->pwat_bal;
  double *mb_values;
  int nb_rec = NB_MB_VAL_TRANSPORT_FP * nb_species;
  double error = 0.;

  mb_values = (double *)malloc(nb_rec * sizeof(double));
  if (mb_values == NULL)
    LP_error(fp, "libfp%d : File %s, function %s at line %d : Bad memory allocation while writing transport mass balance.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  bzero(mb_values, nb_rec * sizeof(double));

  for (p = 0; p < nb_species; p++) {
    error = 0.;
    for (j = 0; j < FP_FIN_TYPE; j++) {
      mb_values[pos] = pbu->input_mflux[p][j]; // Input fluxes (g/m2 or W/m2)
      error += mb_values[pos];
      pos++;
    }
    mb_values[pos] = RSV_mm_to_q(pwatbal->q[RSV_INFILT], pbu->area, dt, SEC_TS, fp); // Water (m3/s)
    pos++;                                                                           // Infiltration component
    mb_values[pos] = pwatbal->trflux[p][RSV_INFILT];                                 // Flux (g/m2 or W/m2)
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_INFILT]; // Variable (K or g/m3)
    pos++;

    mb_values[pos] = RSV_mm_to_q(pwatbal->q[RSV_RUIS], pbu->area, dt, SEC_TS, fp);
    pos++; // Runoff component
    mb_values[pos] = pwatbal->trflux[p][RSV_RUIS];
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_RUIS];
    pos++;

    /* NG : 12/06/2023 : MEMO - As for the hydro, for a print at the ELE_BU scale, the RSV_DIRECT field
     * is left to zeros. Corresponding flux value is left as infiltration. */

    mb_values[pos] = RSV_mm_to_q(pwatbal->q[RSV_DIRECT], pbu->area, dt, SEC_TS, fp);
    pos++; // Direct rerouting (if no underground compartment)
    mb_values[pos] = pwatbal->trflux[p][RSV_DIRECT];
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_DIRECT];
    pos++;

    mb_values[pos] = error;
    pos++;
  }
  return mb_values;
}

/**
 * @brief Computes transport matter budget at the CPROD spatial scale for one time step
 * @return Table of doubles containing all budget values for all species
 */
double *FP_transport_budget_cprod(s_cprod_fp *pcprod, int nb_species, FILE *fp) {
  int j, p;
  int pos = 0;
  s_wat_bal_rsv *pwatbal = pcprod->pwat_bal;
  double *mb_values;
  int nb_rec = NB_MB_VAL_TRANSPORT_FP * nb_species;
  double error = 0.;

  mb_values = (double *)malloc(nb_rec * sizeof(double));
  if (mb_values == NULL)
    LP_error(fp, "libfp%d : File %s, function %s at line %d : Bad memory allocation while writing transport mass balance.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  bzero(mb_values, nb_rec * sizeof(double));

  for (p = 0; p < nb_species; p++) {
    error = 0.;
    for (j = 0; j < FP_FIN_TYPE; j++) {
      mb_values[pos] = pcprod->input_mflux[p][j]; // Input fluxes (g/m2 or W/m2)
      error += mb_values[pos];
      pos++;
    }
    mb_values[pos] = pwatbal->q[RSV_INFILT];         // Water (m3/s)
    pos++;                                           // Infiltration component
    mb_values[pos] = pwatbal->trflux[p][RSV_INFILT]; // Flux (g/m2 or W/m2)
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_INFILT]; // Variable (K or g/m3)
    pos++;

    mb_values[pos] = pwatbal->q[RSV_RUIS];
    pos++; // Runoff component
    mb_values[pos] = pwatbal->trflux[p][RSV_RUIS];
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_RUIS];
    pos++;

    mb_values[pos] = pwatbal->q[RSV_DIRECT];
    pos++; // Direct rerouting
    mb_values[pos] = pwatbal->trflux[p][RSV_DIRECT];
    error -= mb_values[pos];
    pos++;
    mb_values[pos] = pwatbal->trvar[p][RSV_DIRECT];
    pos++;

    mb_values[pos] = error;
    pos++;
  }
  return mb_values;
}

/**
 * @brief Writes surface transport budget at each time step in unformatted format at the selected 'spatial_scale' (ele_bu or cprod)
 * @return None
 */
void FP_write_mb_transport_bin(int nb_species, int spatial_scale, s_carac_fp *pcarac_fp, s_out_io *pout, double dt, FILE *fp) {

  int type_out = pout->type_out;
  double **val;
  int neletot;
  int nb_rec = nb_species * NB_MB_VAL_TRANSPORT_FP;
  FILE *fpout = pout->fout;

  switch (spatial_scale) {
  case FP_ELE_BU_IO:
    neletot = pcarac_fp->nb_BU;
    break;
  case FP_CPROD_IO:
    neletot = pcarac_fp->nb_cprod;
    break;
  case CODE_IO: // Printing at the CPROD scale if left undefined
    neletot = pcarac_fp->nb_cprod;
    break;
  default:
    LP_error(fpout, "In libfp%4.2f, File %s in %s line %d : Selected spatial scale type incoherent with the FP module outputs.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    break;
  }

  if (type_out != ALL_IO)
    neletot = IO_length_id(pout->id_output);

  val = FP_get_mb_val_transport_bin(nb_species, spatial_scale, neletot, pout, pcarac_fp, type_out, dt, fp);
  IO_print_bloc_bin(val, neletot, nb_rec, fpout);

  for (int i = 0; i < nb_rec; i++) {
    free(val[i]);
  }
  free(val);
}

/**
 * @brief Builds the matrix of all values to write at each time step for the selected spatial scale
 * @return Matrix of all values
 */
double **FP_get_mb_val_transport_bin(int nb_species, int spatial_scale, int neletot, s_out_io *pout, s_carac_fp *pcarac_fp, int type_out, double dt, FILE *fp) {
  double **val;
  int nb_rec = nb_species * NB_MB_VAL_TRANSPORT_FP;
  int i, j, p, id_rec, nb_BU;
  s_bal_unit_fp *pbu;
  s_cprod_fp *pcprod;
  s_wat_bal_rsv *pwatbal;
  s_id_io *id_out;
  double *mb_values;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
#endif

  val = (double **)malloc(nb_rec * sizeof(double *));
  for (i = 0; i < nb_rec; i++)
    val[i] = (double *)malloc(neletot * sizeof(double));

  if (type_out == ALL_IO) {
#ifdef OMP
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);
    psmp->chunk = PC_set_chunk_size_silent(fp, neletot, nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(neletot, pcarac_fp, val) private(i, j, mb_values, pbu, pcprod)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (i = 0; i < neletot; i++) {
        if (spatial_scale == FP_ELE_BU_IO) {
          pbu = pcarac_fp->p_bu[i];
          mb_values = FP_transport_budget_ele_bu(pbu, nb_species, dt, fp);
        } else {
          pcprod = pcarac_fp->p_cprod[i];
          mb_values = FP_transport_budget_cprod(pcprod, nb_species, fp);
        }
        for (j = 0; j < nb_rec; j++)
          val[j][i] = mb_values[j];
        free(mb_values);
      }
    }
#ifdef OMP
  } // End of parallel section
#endif
  else {
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;

      if (spatial_scale == FP_ELE_BU_IO) {
        pbu = pcarac_fp->p_bu[i];
        mb_values = FP_transport_budget_ele_bu(pbu, nb_species, dt, fp);
      } else {
        pcprod = pcarac_fp->p_cprod[i];
        mb_values = FP_transport_budget_cprod(pcprod, nb_species, fp);
      }
      for (j = 0; j < NB_MB_VAL_FP; j++)
        val[j][i] = mb_values[j];
      free(mb_values);
      id_out = id_out->next;
    }
  }
  return val;
}

/**
 * @brief Displays in log file all species attributes to make sure they have been set up properly
 * @return None
 */
void FP_print_species_properties(s_carac_fp *pcarac_fp, int nb_species, FILE *flog) {

  int i, j;
  s_input_fp *pinputs = pcarac_fp->pinputs;
  double surf_bu = 0;
  double *surf_species;

  surf_species = (double *)calloc(nb_species, sizeof(double));
  if (surf_species == NULL)
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory allocation. \n", VERSION_FP, __FILE__, __func__, __LINE__);

  for (i = 0; i < pcarac_fp->nb_BU; i++)
    surf_bu += pcarac_fp->p_bu[i]->area;

  for (i = 0; i < nb_species; i++) {
    for (j = 0; j < pcarac_fp->nb_specie_unit[i]; j++)
      surf_species[i] += pcarac_fp->p_species_unit[i][j]->area;
  }

  for (i = 0; i < nb_species; i++) {
    LP_printf(flog, "Specie INTERN id : %d\n", i);
    LP_printf(flog, "Number of species cells read : %d\n", pcarac_fp->nb_specie_unit[i]);
    LP_printf(flog, "Forcing coverage percentage : %7.2f %% (Ele_bu surface : %7.2f km2 - Specie grid surface : %7.2f km2).\n", (surf_species[i] * 1.e2) / surf_bu, surf_bu * 1.e-6, surf_species[i] * 1.e-6);
    LP_printf(flog, "Forcing condition : %s\n", FP_transport_forcing_conditions(pinputs->transport_forcing_conditions[i]));
    LP_printf(flog, "Format of input forcing files : %s\n", IO_name_ext(pinputs->transport_input_format[i]));
    LP_printf(flog, "List of prefixes and variables types declared : \n");
    for (j = 0; j < FP_NVAR_INPUT_TRANSPORT; j++) {
      if (pinputs->transport_input_prefixes[i][j] != NULL) {
        // LP_printf(flog,"File prefix : %s - Variable : %s - Multiplication factor for forcing values series : %8.2e\n",pinputs->transport_input_prefixes[i][j],FP_transport_input_variable_name(j),pinputs->forcing_unit[i][j]);
        LP_printf(flog, "File prefix : %s - Variable : %s\n", pinputs->transport_input_prefixes[i][j], FP_transport_input_variable_name(j));
      }
    }
    LP_printf(flog, "\n");
  }
}