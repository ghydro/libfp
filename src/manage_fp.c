/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_fp.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_fp.c
 * @brief       Functions dealing with instances and attributes of the param_fp structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <errno.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Creates and initializes an instance of the param_fp structure
 * @return Newly created s_param_fp* pointer
 */
s_param_fp *FP_create_param(int id_FP, char *name) {
  s_param_fp *param_fp;
  param_fp = new_param_fp();
  bzero((char *)param_fp, sizeof(s_param_fp));
  param_fp->id = id_FP;
  sprintf(param_fp->name, "%s", name);
  param_fp->p_ft = NULL;           // NG : 26/11/2019 : Time serie for LAI values
  param_fp->io_inf_to_runoff = NO; // NG : 05/12/2023 : I/O boolean for rerouting feature on bucket overflow.
  return param_fp;
}

/**
 * @brief Fills the FP parameters
 * @return
 */
void FP_fill_param(s_param_fp *param, double CRT, double DCRT, double FN, double QR, double CQRMAX, double QI, double CQIMAX, double LCRT, double LDCR, int io_rerouting) {
  param->param[FP_CRT] = CRT;
  param->param[FP_DCRT] = DCRT;
  param->param[FP_FN] = FN;
  param->param[FP_QRMAX] = CQRMAX;
  param->param[FP_CQR] = QR;
  param->param[FP_QIMAX] = CQIMAX;
  param->param[FP_CQI] = QI;
  param->param[FP_RMAX] = 2 * (param->param[FP_CRT] - param->param[FP_DCRT]) + param->param[FP_DCRT];
  param->param[FP_LCRT] = LCRT; // NG : 26/11/2019 : LCRT and LDCR added
  param->param[FP_LDCR] = LDCR;
  param->io_inf_to_runoff = io_rerouting; // NG : 05/12/2023 : io_rerouting = YES/NO boolean for rerouting feature on overflow from inf bucket to runoff bucket.
}

/**
 * @brief Chains to s_param_fp* pointers
 */
s_param_fp *chain_param_fp(s_param_fp *param1, s_param_fp *param2) {
  param2->prev = param1;
  param1->next = param2;
  return param1;
}

/**
 * @brief Chains to s_param_fp* pointers
 */
s_param_fp *FP_secured_chain_param_fwd(s_param_fp *param1, s_param_fp *param2) {
  if (param1 != NULL)
    param1 = chain_param_fp(param1, param2);
  else {
    param1 = param2;
  }
  return param1;
}

/**
 * @brief Stores all the s_param_fp* into a single array
 * @return Filled s_param_fp** array
 */
s_param_fp **FP_tab_param(s_param_fp *param, int nb_param_fp) {
  int i = 0;
  s_param_fp **tab_param;

  tab_param = (s_param_fp **)malloc(nb_param_fp * sizeof(s_param_fp *));
  bzero((char *)tab_param, nb_param_fp * sizeof(s_param_fp *));

  while (param->prev != NULL)
    param = param->prev;

  for (i = 0; i < nb_param_fp; i++) {
    param->id = i;
    tab_param[i] = param;
    param = param->next;
  }
  return tab_param;
}

/**
 * @brief Elementary function to modify with time CRT or DCRT parameters with LAI(t) values
 * @return Modified value of the function input parameter
 */
double FP_LAI_ponderation(double parameter, double alpha, double lai) { return parameter + alpha * lai; }

/**
 * @brief  Computes the effective rainfall = the water flow either going through inf or runoff buckets
 * @return None
 * @internal  NG 26/11/2019 : CRT and DCRT parameters can now be modulated at each time
 * step by optional LAI data.
 * NG 31/05/2021 : Pointer chains reading procedures to get MTO (FP_get_MTO()) and LAI (FP_get_LAI()) values
 * have been modified in order to speed calculations up a bit. @endinternal
 */
void FP_calc_wat_bal(s_bal_unit_fp *pBU, double t, double dt_bassin, FILE *fpout, int lai_type) {
  double rba, rha, dr, water, etr, crtP, dcrtP, rmaxP;
  double *mto_val;
  double lai_value = 0.;
  s_ft *ptmp;
  s_mto_fp *pmto;
  s_param_fp *param_fp;
  s_wat_bal_rsv *pwat_bal;

  param_fp = pBU->param_fp;

  if (lai_type == YES) {
    pBU->param_fp->p_ft = TS_function_t_pointed(t, pBU->param_fp->p_ft, fpout);
    lai_value = FP_get_LAI(t, pBU->param_fp->p_ft, fpout);
  }

  crtP = FP_LAI_ponderation(param_fp->param[FP_CRT], param_fp->param[FP_LCRT], lai_value);
  dcrtP = FP_LAI_ponderation(param_fp->param[FP_DCRT], param_fp->param[FP_LDCR], lai_value);

  // NG 13/02/2020 : Bugfix : RMAX needs to be updated if LAI is on !!
  rmaxP = 2 * (crtP - dcrtP) + dcrtP;

  mto_val = FP_get_mto(t, pBU->pmto, fpout);

  pwat_bal = pBU->pwat_bal;

  pBU->stock[FP_SOIL_STCK] = (-pwat_bal->r[RSV_BIL]);

  rba = TS_max(dcrtP, pwat_bal->r[RSV_BIL]) - dcrtP;
  rha = TS_min((pwat_bal->r[RSV_BIL] + mto_val[FP_RAIN]), rmaxP) - dcrtP;
  dr = TS_max(0, (rha - rba));

  if (fabs(crtP - dcrtP) < EPS)
    LP_error(fpout, "Denominator nul in file : %s, Function %s, Cprod %d (CRT-DCRT = 0).\n", __FILE__, __func__, pBU->id);

  water = TS_max((pwat_bal->r[RSV_BIL] + mto_val[FP_RAIN] - rmaxP), 0) + dr * (2 * rba + dr) / (4 * (crtP - dcrtP));

  etr = TS_min((pwat_bal->r[RSV_BIL] + mto_val[FP_RAIN] - water), mto_val[FP_ETP]);

  pwat_bal->r[RSV_BIL] = pwat_bal->r[RSV_BIL] + mto_val[FP_RAIN] - water - etr;
  pwat_bal->q[RSV_ETR] = etr;
  pBU->stock[FP_SOIL_STCK] += pwat_bal->r[RSV_BIL];

  // LP_printf(fpout,"Id_ele_bu %d t %f prcp %f etp %f etr %f\n",pBU->id,t,mto_val[FP_RAIN],mto_val[FP_ETP],pwat_bal->q[RSV_ETR]); // NG check

  pwat_bal->q[RSV_BIL] = water;
  free(mto_val);
}

/**
 * @brief Computes effective rainfall fractionation. Dissociates runoff and infiltration flows.
 * Computes the fluxes which enter the infiltration and runoff buckets.
 * @return None
 */
void FP_balance_distrib(s_bal_unit_fp *pBU, FILE *fpout) {

  double water, water_tot = 0; // NF 30/7/03

  s_wat_bal_rsv *pwat_bal;
  char name[STRING_LENGTH_LP]; // NF 15/5/13
  FILE *fp;                    // NF 15/5/13

  pwat_bal = pBU->pwat_bal;
  water = pwat_bal->q[RSV_BIL];
  // LP_printf(fpout,"Water %f\n",water); //BL to debug

  pwat_bal->q[RSV_INFILT] = TS_min(water, pBU->param_fp->param[FP_FN]); // Flux which enters the infiltration bucket.
  pwat_bal->q[RSV_RUIS] = TS_max(0, (water - pwat_bal->q[RSV_INFILT])); // Flux which enters the runoff bucket.

  // LP_printf(fpout,"Lame d'eau entrant dans infilt %f\n",pwat_bal->q[RSV_INFILT]); //BL to debug
  // LP_printf(fpout,"Lame d'eau entrant dans ruiss %f\n",pwat_bal->q[RSV_RUIS]); //BL to debug
}

/**
 * @brief Computes flows outgoing from the infiltration and runoff buckets.
 * @return None
 */
void FP_runoff_infiltration(s_bal_unit_fp *pBU, FILE *fpout) {

  int i;
  double qmax, cq, q;
  s_wat_bal_rsv *pwat_bal;
  s_param_fp *param_fp;
  pwat_bal = pBU->pwat_bal;
  param_fp = pBU->param_fp;

  for (i = 0; i < FP_NSTCK; i++) {

    switch (i) {
    case FP_SOIL_STCK: {
      break;
    }

    case FP_INFILT_STCK: {
      qmax = param_fp->param[FP_QIMAX];
      cq = param_fp->param[FP_CQI];
      pBU->stock[i] = (-pwat_bal->r[RSV_INFILT]);
      q = RSV_transit_simple(pwat_bal, qmax, cq, 0., RSV_INFILT, NULL, param_fp->io_inf_to_runoff, RSV_INFILT, fpout);
      if (isnan(q) || isinf(q)) {
        LP_error(fpout, "In libfp%4.2f : Function %d : anormal value produced while calculating %s at wbu %d\n", VERSION_FP, __func__, RSV_name_res(RSV_INFILT), pBU->id);
      }
      // LP_printf(fpout,"q_%s = %f\n",RSV_name_res(RSV_INFILT),pwat_bal->q[RSV_INFILT]); //BL to debug
      pBU->stock[i] += pwat_bal->r[RSV_INFILT];
      break;
    }

    case FP_RUIS_STCK: {
      qmax = param_fp->param[FP_QRMAX];
      cq = param_fp->param[FP_CQR];
      pBU->stock[i] = (-pwat_bal->r[RSV_RUIS]);
      q = RSV_transit_simple(pwat_bal, qmax, cq, 0., RSV_RUIS, NULL, NO, RSV_INFILT, fpout);
      pBU->stock[i] += pwat_bal->r[RSV_RUIS];
      if (isnan(q) || isinf(q)) {
        LP_error(fpout, "In libfp%4.2f : Function %d : anormal value produced while calculating %s at wbu %d\n", VERSION_FP, __func__, RSV_name_res(RSV_RUIS), pBU->id);
      }
      // LP_printf(fpout,"q_%s = %f\n",RSV_name_res(RSV_RUIS),pwat_bal->q[RSV_RUIS]); //BL to debug
      break;
    }

    default:
      LP_error(fpout, "In libfp%4.2f : In function %s, line %d : Unknown FP reservoir.\n", VERSION_FP, __func__, __LINE__);
    }
  }
}

/**
 * @brief Fetch climate input values (rainfall, PET) at the current time step
 * @return Array filled with found values
 */
double *FP_get_mto(double t, s_mto_fp *pmto, FILE *fpout) {
  double *mean_val;
  int i = 0;
  s_ft *ptmp;
  s_id_spa *link;

  mean_val = (double *)malloc(FP_NMETEO * sizeof(double));
  bzero((char *)mean_val, (FP_NMETEO * sizeof(double)));

  for (i = 0; i < FP_NMETEO; i++) {
    pmto->p_ft[i] = TS_function_t_pointed(t, pmto->p_ft[i], fpout);
    mean_val[i] = TS_function_value_t(t, pmto->p_ft[i], fpout);
    if (mean_val[i] < 0)
      LP_error(fpout, "libfp%4.2f : Error in file %s, function %s at line %d : %s input value is negative for mto cell GIS id %d.\n", VERSION_FP, __FILE__, __func__, __LINE__, FP_name_mto(i), pmto->id[FP_GIS]);
  }
  return mean_val;
}

/**
 * @brief Master function for LAI input reading procedure. Launched from main_cawaqs.c
 * Stores the yearly LAI time serie into the corresponding param_fp structure
 * @return None
 */
void FP_read_all_LAI(s_carac_fp *pcarac_fp, int year, FILE *fpout) {
  int i, id_FP;
  char *lai_path;
  s_param_fp *p_param;
  s_input_fp *pinput;

  pinput = pcarac_fp->pinputs;
  lai_path = pinput->input_paths[FP_LAI];

  for (i = 0; i < pcarac_fp->nb_FP; i++) {
    id_FP = pcarac_fp->p_param_fp[i]->id;
    p_param = pcarac_fp->p_param_fp[i];

    if (p_param->p_ft != NULL)
      p_param->p_ft = TS_free_ts(p_param->p_ft, fpout); // Yearly TS deallocations

    p_param->p_ft = FP_read_LAI_data(lai_path, pinput->prefix_lai, pinput->format_lai, year, id_FP, pcarac_fp->nb_FP, fpout);
  }
}

/**
 * @brief Launches the reading procedure of LAI input files according to the format of the file
 * and fetches time serie data for one given production-function (built by FP_read_LAI_year_bin())
 * @return Filled LAI s_ft* time serie
 */
s_ft *FP_read_LAI_data(char *path, char *prefixe, int type_fic, int year, int numFP, int nb_FP, FILE *fpout) {
  char *name;
  FILE *fpin;
  s_ft *ft_out;

  name = (char *)malloc(STRING_LENGTH_LP * sizeof(char));

  if (type_fic == UNFORMATTED_IO) {
    sprintf(name, "%s/%s.%d%d", path, prefixe, year, year + 1);
    fpin = fopen(name, "rb");

    if (fpin == NULL) {
      sprintf(name, "%s/%s.%d%d.dir", path, prefixe, year, year + 1);
      fpin = fopen(name, "rb");
      if (fpin == NULL)
        LP_error(fpout, "In libfp%4.2f : Error in file %s function : %s : LAI file %s unreachable. Check your input data.\n", VERSION_FP, __FILE__, __func__, name);
    }
    ft_out = FP_read_LAI_year_bin(name, fpin, year, numFP, nb_FP, fpout);
    fclose(fpin);
  } else
    LP_error(fpout, "In libfp%4.2f : Error in file %s function : %s : Formatted-type LAI input data not implemented yet. Check the user guide.\n", VERSION_FP, __FILE__, __func__);

  free(name);
  return ft_out;
}

/**
 * @brief Reads a LAI input file in binary format, extract the time serie for a given FP
 * @return Filled s_ft* chain (time serie)
 */
s_ft *FP_read_LAI_year_bin(char *name, FILE *fpin, int year, int numFP, int nb_FP, FILE *fpout) {
  s_ft *pft = NULL, *pftmp = NULL;
  int i, errno, pos = 0, nb_days = 0, nbFPread = 0;
  double size_file = 0., verif = 0., julian, lai;
  float val;

  errno = fseek(fpin, pos, SEEK_END);
  size_file = (double)ftell(fpin); // Returns -1 if failure
  if (size_file < 0) {
    LP_error(fpout, "Cannot determine size of file  %s : %s, in file %s, function %s, line %d.\n", name, strerror(errno), __FILE__, __func__, __LINE__);
  }

  nb_days = TS_nb_jour_an(year + 1);
  verif = size_file / ((double)nb_days * sizeof(float));
  nbFPread = (int)verif;

  if (fabs(nbFPread - nb_FP) > EPS_FP)
    LP_error(fpout, "Incoherent total record number in the LAI file %s. (Expected : %d, Read : %d) - File : %s, function : %s, line : %d.\n", name, nb_FP, nbFPread, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_days; i++) {
    pos = (i * nbFPread + numFP) * sizeof(float); // numFP starts at 0.
    errno = fseek(fpin, pos, SEEK_SET);

    if (errno < 0)
      LP_error(fpout, "Wrong location in LAI binary file for year %d-%d, day %d production-function %d. Searched location  \
                      is %d (max = %d). - File : %s, function : %s, line : %d.\n",
               year, year + 1, i, numFP, pos, size_file, __FILE__, __func__, __LINE__);

    fread(&val, sizeof(float), 1, fpin);

    lai = (double)val;
    julian = (double)TS_calculate_jour_julien_j_nbday(year, CODE_FP, i);

    pftmp = TS_create_function(julian, lai);
    pft = TS_secured_chain_fwd_ts(pft, pftmp);
  }

  pft = TS_browse_ft(pft, BEGINNING_TS);

  return pft;
}

/**
 * @brief Fetches the LAI value from a s_ft* time_serie at a given time.
 * @return Found value
 */
double FP_get_LAI(double t, s_ft *pft, FILE *fpout) {
  double val = 0;
  val = TS_function_value_t(t, pft, fpout);
  return val;
}

/**
 * @brief TBD
 * @return TBD
 */
double *FP_calc_mean_mto(double tin, double tf, s_mto_fp *pmto, FILE *fpout) {
  double *mean_val;
  int i;
  s_ft *pft, *pft2, *pftdel;
  s_id_spa *link;
  i = 0;
  mean_val = (double *)malloc(FP_NMETEO * sizeof(double));
  bzero((char *)mean_val, (FP_NMETEO * sizeof(double)));

  for (i = 0; i < FP_NMETEO; i++) {

    pft = pmto->p_ft[i];
    pft2 = pft;

    pft = TS_compare_time(pft, tf);           //??
    pftdel = pft = TS_compare_time(pft, tin); // NF 19/12/04

    pft = TS_browse_ft(pft, BEGINNING_TS); // NF 19/12/04
    while (pft != pftdel)                  // NF 19/12/04
    {                                      // NF 19/12/04
      pft = pft->next;                     // NF 19/12/04
      pft->prev = TS_free_ft(pft->prev);   // NF 19/12/04
    }                                      // NF 19/12/04

    while (pft->t <= tin) {
      pft2 = pft;
      pft = pft->next;
    }

    while (pft != NULL) {
      if ((pft->t > tin) && (pft->t <= tf)) {
        mean_val[i] += TS_integrate(pft2->t, pft2->ft, pft->t, pft->ft);
      }
      pft2 = pft;
      pft = pft->next;
    }
    mean_val[i] /= (tf - tin);
  }
  return mean_val;
}

/**
 * @brief Prints param_fp calibration parameters in the log file
 * @return None
 * @internal NG : 05/12/2023 : Modified to include inf to runoff overflow rerouting option.
 */
void FP_print_all_FP_param(s_carac_fp *pchar_fp, FILE *flog) {

  s_param_fp *param_fp;
  int nb_FP, i, j;
  char *name;

  nb_FP = pchar_fp->nb_FP;
  LP_printf(flog, "FP Name ");
  for (j = 0; j < FP_NPRODUCTION; j++) {
    name = FP_name_param(j);
    LP_printf(flog, "%s ", name);
    free(name);
  }
  LP_printf(flog, "REROUTING_OPTION \n");

  for (i = 0; i < nb_FP; i++) {
    param_fp = pchar_fp->p_param_fp[i];
    FP_print_param(param_fp, flog);
  }
}

/**
 * @brief Prints parameter for a single FP
 * @return None
 */
void FP_print_param(s_param_fp *param_fp, FILE *fpout) {
  int j;
  LP_printf(fpout, "%s ", param_fp->name);
  for (j = 0; j < FP_NPRODUCTION; j++) {
    LP_printf(fpout, "%.3f ", param_fp->param[j]);
  }
  LP_printf(fpout, " %s \n", LP_answer(param_fp->io_inf_to_runoff, fpout));
}