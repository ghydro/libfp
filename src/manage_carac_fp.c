/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_carac_fp.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_carac_fp.c
 * @brief       Functions dealing with attributes of the instances of the carac_fp structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Creates and initializes with default settings the carac structure
 * @return -
 */
s_carac_fp *FP_create_carac_fp() {

  int i;

  s_carac_fp *pcarac_fp;
  pcarac_fp = new_carac_fp();
  bzero((char *)pcarac_fp, sizeof(s_carac_fp));
  pcarac_fp->input_lai = NO;
  pcarac_fp->nb_catchment = 1;
  pcarac_fp->p_catchment = NULL;
#ifdef OMP
  pcarac_fp->psmp = new_smp();
  pcarac_fp->psmp->nthreads = 1;
#endif
  pcarac_fp->default_concentration_time = TC_DEFAULT_FP;
  pcarac_fp->thresh_mean_surf = SMOY_THRESH_TC;
  for (i = 0; i < FP_TC_PARAMHDERM; i++)
    pcarac_fp->param_tc[i] = 0.;

  // Transport-related attributes
  pcarac_fp->p_species_unit = NULL;

  return pcarac_fp;
};

/**
 * @brief Launches the surface water balance calculation at the water balance unit scale
 * for all units. Accounts for parallelization. Parent function launched from main_cawaqs.c
 * @return None
 */
void FP_calc_wat_bal_BU_all(s_carac_fp *pcarac_fp, double t, double dt, FILE *fpout) {

  int lai_type, nb_BU, i;

  s_bal_unit_fp **p_bu;
  nb_BU = pcarac_fp->nb_BU;
  p_bu = pcarac_fp->p_bu;

  lai_type = pcarac_fp->input_lai;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_fp->nb_BU, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_bu, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_BU; i++) {
      FP_calc_wat_bal_BU(p_bu[i], t, dt, fpout, lai_type);
      // LP_printf(fpout,"Id_ele_bu %d t %f inf %f ruis %f\n",p_bu[i]->id,t,p_bu[i]->pwat_bal->q[RSV_INFILT],p_bu[i]->pwat_bal->q[RSV_RUIS]); // Check NG
    }

#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief (Re)allocates transport-related carac_fp attributes each time a new specie is read
         (counters of elements and tabulars to store pointers for all new specie units)
 * @return None
 */
void FP_manage_memory_carac_species(s_carac_fp *pcarac_fp, int nb_species, FILE *flog) {

  int id_species = nb_species - 1;

  if (pcarac_fp->p_species_unit == NULL) {
    pcarac_fp->p_species_unit = (s_specie_unit_fp ***)malloc(nb_species * sizeof(s_specie_unit_fp **));
    pcarac_fp->nb_specie_unit = (int *)malloc(nb_species * sizeof(int));

    if (pcarac_fp->p_species_unit == NULL || pcarac_fp->nb_specie_unit == NULL) {
      LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory allocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    }

    pcarac_fp->p_species_unit[id_species] = NULL;
    pcarac_fp->nb_specie_unit[id_species] = 0;

  } else { // At least a first specie has been read --> Extending and initializing array where needed.
    pcarac_fp->p_species_unit = (s_specie_unit_fp ***)realloc(pcarac_fp->p_species_unit, nb_species * sizeof(s_specie_unit_fp **));
    pcarac_fp->nb_specie_unit = (int *)realloc(pcarac_fp->nb_specie_unit, nb_species * sizeof(int));

    if (pcarac_fp->p_species_unit == NULL || pcarac_fp->nb_specie_unit == NULL) {
      LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory reallocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    }

    pcarac_fp->p_species_unit[id_species] = NULL;
    pcarac_fp->nb_specie_unit[id_species] = 0;
  }
}

/**
 * @brief Launches flux calculations depending on the forcing mode activated via the input file
 * @return None
 * @internal NG : 24/03/2023 : Renaming of all forcing modes to be more suitable to both temperature and solute cases
 * Adding int* spetypes to have access to all declared specie types in order to launch functions accordingly
 * (i.e. for heat or solute). @endinternal
 */
void FP_calc_transport_all_surface_fluxes(s_carac_fp *pcarac_fp, int nb_species, int *spetypes, double *mmass_spe, double *mmass_ele, double t, double dt, FILE *flog) {

  int p, forcing_mode;

  for (p = 0; p < nb_species; p++) {
    forcing_mode = pcarac_fp->pinputs->transport_forcing_conditions[p];

    if (spetypes[p] == FP_SOLUTE) {

      switch (forcing_mode) {

      case FP_TOTAL_TRFLUX:
        FP_transport_solute_total_trflux_mode(pcarac_fp, mmass_spe[p], mmass_ele[p], p, t, dt, flog);
        break;

      case FP_TOTAL_TRFLUX_WATER:
        FP_transport_solute_total_trflux_water_mode(pcarac_fp, mmass_spe[p], mmass_ele[p], p, t, dt, flog);
        break;

      case FP_DISSOCIATED_TRFLUX:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;

      case FP_DISSOCIATED_TRFLUX_WATER:
        FP_transport_solute_dissociated_trflux_water_mode(pcarac_fp, mmass_spe[p], mmass_ele[p], p, t, dt, flog);
        break;

      case FP_TOTAL_TRVAR:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;

      case FP_TOTAL_TRVAR_WATER:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;

      case FP_DISSOCIATED_TRVAR_WATER:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      default:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Unknown forcing mode for species id %d\n", VERSION_FP, __FILE__, __func__, __LINE__, p);
        break;
      }
    } else // FP_HEAT
    {
      switch (forcing_mode) {
      case FP_TOTAL_TRFLUX:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_TOTAL_TRFLUX_WATER:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_DISSOCIATED_TRFLUX:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_DISSOCIATED_TRFLUX_WATER:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_TOTAL_TRVAR:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_TOTAL_TRVAR_WATER:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Cannot compute transport for species %d (type : %s) : %s mode not implemented yet.\n", VERSION_FP, __FILE__, __func__, __LINE__, p, FP_transport_spetype(spetypes[p]), FP_transport_forcing_conditions(forcing_mode));
        break;
      case FP_DISSOCIATED_TRVAR_WATER:
        FP_transport_heat_dissociated_trvar_water_mode(pcarac_fp, mmass_spe[p], mmass_ele[p], p, t, dt, flog);
        break;
      default:
        LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Unknown forcing mode for species id %d\n", VERSION_FP, __FILE__, __func__, __LINE__, p);
        break;
      }
    }
  }
}

/**
 * @brief Allocates and initialize the fluxes and concentration attributes in the RSV wat_bal
 * structure for all species, all ele_bus and all cprod if transport in surface module is activated.
 * @return None
 */
void FP_manage_memory_rsv_transport(int nb_species, s_carac_fp *pcarac_fp, FILE *flog) {

  int nb_BU = pcarac_fp->nb_BU;
  int nb_cprod = pcarac_fp->nb_cprod;
  s_bal_unit_fp *pbu;
  s_cprod_fp *pcprod;
  int i;

  for (i = 0; i < nb_BU; i++) {
    pbu = pcarac_fp->p_bu[i];
    RSV_manage_memory_transport(pbu->pwat_bal, nb_species, flog);
  }
  for (i = 0; i < nb_cprod; i++) {
    pcprod = pcarac_fp->p_cprod[i];
    RSV_manage_memory_transport(pcprod->pwat_bal, nb_species, flog);
  }
}

/**
 * @brief Launches all fluxes calculation for all ele_bu in FP_TRFLUX
 * forcing mode (for SOLUTE TRANSPORT)
 * @return None
 */
void FP_transport_solute_total_trflux_mode(s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {

  int i, nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp **p_bu = pcarac_fp->p_bu;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(flog, nb_BU, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_bu, pcarac_fp, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_BU; i++)
      FP_transport_solute_total_trflux_one_BU(p_bu[i], pcarac_fp, mmass_spe, mmass_ele, id_species, t, dt, flog);

#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief Launches all fluxes calculation for all ele_bu in FP_TOTAL_TRFLUX_WATER
 * forcing mode (for SOLUTE TRANSPORT)
 * @return None
 */
void FP_transport_solute_total_trflux_water_mode(s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {
  int i;
  int nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp **p_bu = pcarac_fp->p_bu;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(flog, nb_BU, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_bu, pcarac_fp, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_BU; i++)
      FP_transport_solute_total_trflux_water_one_BU(p_bu[i], pcarac_fp, mmass_spe, mmass_ele, id_species, t, dt, flog);

#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief Launches all fluxes calculation for all ele_bu in FP_DISSOCIATED_TRFLUX_WATER
 * forcing mode (for SOLUTE TRANSPORT)
 * @return -
 */
void FP_transport_solute_dissociated_trflux_water_mode(s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {
  int i;
  int nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp **p_bu = pcarac_fp->p_bu;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(flog, nb_BU, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_bu, pcarac_fp, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_BU; i++)
      FP_transport_solute_dissociated_trflux_water_one_BU(p_bu[i], pcarac_fp, mmass_spe, mmass_ele, id_species, t, dt, flog);

#ifdef OMP
  } // end of parallel section
#endif
}

/**
 * @brief Launches all fluxes calculation for all ele_bu in FP_DISSOCIATED_TRVAR_WATER
 * forcing mode (for HEAT TRANSPORT)
 * @return -
 */
void FP_transport_heat_dissociated_trvar_water_mode(s_carac_fp *pcarac_fp, double mmass_spe, double mmass_ele, int id_species, double t, double dt, FILE *flog) {

  int i, nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp **p_bu = pcarac_fp->p_bu;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(flog, nb_BU, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_bu, pcarac_fp, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_BU; i++)
      FP_transport_heat_dissociated_trvar_water_one_BU(p_bu[i], pcarac_fp, mmass_spe, mmass_ele, id_species, t, dt, flog);

#ifdef OMP
  } // end of parallel section
#endif
}