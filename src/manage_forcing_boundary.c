/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_forcing_boundary.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_forcing_boundary.c
 * @brief       Functions dealing with attributes of the instances of the forcing_boundary structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Creates and initializes with default settings a new boundary
 * @return Pointer towards newly created instance of the forcing_boundary structure
 */
s_forcing_bound_fp *FP_create_forcing_boundary() {
  int i;
  s_forcing_bound_fp *pbound = NULL;
  pbound = new_forcing_boundary();
  bzero((char *)pbound, sizeof(s_forcing_bound_fp));

  // Default initialization
  pbound->freq = FP_CST;
  pbound->pft = NULL;

  return pbound;
}

/**
 * @brief (Re)allocates ***pbound for each watbal unit according to the current number
 * of specie which is being read
 * @return None
 */
void FP_manage_memory_boundary_ele_bu(s_bal_unit_fp *pbu, int nb_species, FILE *flog) {
  int i;
  int id_species = nb_species - 1;

  if (pbu->pbound == NULL) // In case of the first specie being read
  {
    pbu->pbound = (s_forcing_bound_fp ***)malloc(nb_species * sizeof(s_forcing_bound_fp **));
    if (pbu->pbound == NULL)
      LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory allocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
    bzero((char *)pbu->pbound, nb_species * sizeof(s_forcing_bound_fp **));
  } else { // At least one specie has already been read --> Array is extended. Newly allocated part is initialized.
    pbu->pbound = (s_forcing_bound_fp ***)realloc(pbu->pbound, nb_species * sizeof(s_forcing_bound_fp **));
    if (pbu->pbound == NULL)
      LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory reallocation.\n", VERSION_FP, __FILE__, __func__, __LINE__);
  }
  pbu->pbound[id_species] = NULL;
}

/**
 * @brief Assigns forcing boundaries time serie to a watbal unit.
 * @return None
 */
void FP_define_forcing_boundary(s_carac_fp *pcarac_fp, int id_species, int id_variable, s_id_io *id_list, s_ft *pft_bound, FILE *flog) {
  int i, nb_BU = pcarac_fp->nb_BU;
  s_id_io *id_list_tmp;
  s_bal_unit_fp *pbalunit;
  s_bal_unit_fp **tab_watbal = pcarac_fp->p_bu;

  pft_bound = TS_browse_ft(pft_bound, BEGINNING_FP);
  id_list = IO_browse_id(id_list, BEGINNING_FP);

  id_list_tmp = id_list;

  if (pft_bound == NULL)
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Time serie pointer chain is NULL.\n", VERSION_FP, __FILE__, __func__, __LINE__);

  while (id_list_tmp != NULL) {

    pbalunit = FP_check_is_ele_bu_defined(tab_watbal, nb_BU, id_list_tmp->id, flog); // Checking if the target watbal unit does exist

    if (pbalunit->pbound[id_species] == NULL) // Allocating since nothing has been assigned yet
    {
      pbalunit->pbound[id_species] = (s_forcing_bound_fp **)malloc(FP_NVAR_INPUT_TRANSPORT * sizeof(s_forcing_bound_fp *));
      bzero((char *)pbalunit->pbound[id_species], FP_NVAR_INPUT_TRANSPORT * sizeof(s_forcing_bound_fp *));
      for (i = 0; i < FP_NVAR_INPUT_TRANSPORT; i++)
        pbalunit->pbound[id_species][id_variable] = NULL;
      pbalunit->pbound[id_species][id_variable] = FP_create_forcing_boundary();
      pbalunit->pbound[id_species][id_variable]->pft = pft_bound;
      if (pbalunit->pbound[id_species][id_variable]->pft->next != NULL)
        pbalunit->pbound[id_species][id_variable]->freq = FP_VAR;
    } else {
      if (pbalunit->pbound[id_species][id_variable] != NULL)
        LP_warning(flog, "libfp%4.2f, File %s in %s line %d : A boundary condition has already been assigned to ele_bu INTERN id %d for forcing variable %s. Ignoring...\n", VERSION_FP, __FILE__, __func__, __LINE__, pbalunit->id, FP_transport_input_variable_name(id_variable));
      else {
        pbalunit->pbound[id_species][id_variable] = FP_create_forcing_boundary();
        pbalunit->pbound[id_species][id_variable]->pft = pft_bound;
        if (pbalunit->pbound[id_species][id_variable]->pft->next != NULL)
          pbalunit->pbound[id_species][id_variable]->freq = FP_VAR;
      }
    }
    id_list_tmp = id_list_tmp->next;
  }
}

/**
 * @brief Prints all boundaries assigned to watbat unit for a single specie, including raw (non-interpolated
 * variable time serie with time) time series
 * @return -
 */
void FP_print_forcing_boundary_one_specie(s_carac_fp *pcarac_fp, int id_species, FILE *flog) {
  int nb_BU = pcarac_fp->nb_BU;
  s_bal_unit_fp **tab_watbal = pcarac_fp->p_bu;
  int i, j;
  s_forcing_bound_fp **pbound;
  s_ft *pftmp;

  for (i = 0; i < nb_BU; i++) {
    if (tab_watbal[i]->pbound[id_species] != NULL) {
      LP_printf(flog, "*** Specie id %d - Forcing boundaries for INTERN id ele_bu %d\n", id_species, tab_watbal[i]->id + 1);

      for (j = 0; j < FP_NVAR_INPUT_TRANSPORT; j++) {
        if (tab_watbal[i]->pbound[id_species][j] != NULL) {
          LP_printf(flog, "Forcing variable : %s - Boundary frequency : %s \n", FP_transport_input_variable_name(j), FP_boundary_frequency(tab_watbal[i]->pbound[id_species][j]->freq));
          pftmp = TS_browse_ft(tab_watbal[i]->pbound[id_species][j]->pft, BEGINNING_FP);
          while (pftmp != NULL) {
            LP_printf(flog, "Time : %f - Value %f\n", pftmp->t, pftmp->ft);
            pftmp = pftmp->next;
          }
        }
      }
    }
  }
}

/**
 * @brief  Retrieves the value of a pBU->pbound for a given specie and a given forcing variable at time t
 * @return boundary value at time t
 */
double FP_get_boundary_value(double t, s_forcing_bound_fp ***pbound, int id_species, int variable_type, FILE *flog) {
  double value = 0.;
  value = TS_interpolate_ts(t, pbound[id_species][variable_type]->pft);
  return value;
}