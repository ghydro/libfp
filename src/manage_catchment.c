/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_catchment.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_catchment.c
 * @brief       Functions dealing with attributes of the instances of the catchment structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Creates a new catchment. Initializes with default settings.
 * @return -
 */
s_catchment_fp *FP_create_catchment(int num_gis, int nb_cprod, s_carac_fp *pcarac) {
  s_catchment_fp *pcatch = NULL;

  pcatch = new_catchment_fp();
  pcatch->id_gis = num_gis;
  pcatch->catchment_name = NULL;
  pcatch->area = 0.;
  pcatch->concentration_time = pcarac->default_concentration_time;
  pcatch->Tc_Formula = pcarac->default_concentration_time;
  pcatch->nb_cprod_catchment = nb_cprod;
  pcatch->catchment_type = FP_CATCH_RIV;
  pcatch->next = NULL;
  pcatch->prev = NULL;
  pcatch->pcprod_outlet = NULL;
  pcatch->Q_chasm = 0.;
  pcatch->Itr_max = SMALL_FP;
  pcatch->infil_coef = CHASM_INF_DEFAULT;
  // Allocating array to store cprods which belongs to the catchment
  pcatch->cprod_catchment = (s_cprod_fp **)malloc(nb_cprod * sizeof(s_cprod_fp *));
  bzero(pcatch->cprod_catchment, nb_cprod * sizeof(s_cprod_fp *));

  return pcatch;
}

/**
 * @brief Chains two catchment objects
 */
s_catchment_fp *FP_chain_catchment(s_catchment_fp *pcatch1, s_catchment_fp *pcatch2) {
  pcatch2->prev = pcatch1;
  pcatch1->next = pcatch2;
  return pcatch1;
}

/**
 * @brief Chains two catchment objects
 */
s_catchment_fp *FP_secured_chain_catchment_fwd(s_catchment_fp *pcatch1, s_catchment_fp *pcatch2) {
  if (pcatch1 != NULL) {
    pcatch1 = FP_chain_catchment(pcatch1, pcatch2);
  } else {
    pcatch1 = pcatch2;
  }
  return pcatch1;
}

/**
 * @brief Browses catchments objects based on a s_catchment_fp* pointer chain
 * @return Returns either first or last catchment pointer depending on browse_type direction.
 */
s_catchment_fp *FP_browse_catchment_chain(s_catchment_fp *pcatch, int browse_type, FILE *fpout) {

  if (pcatch == NULL)
    LP_error(fpout, "Nothing to browse. Catchment chain is NULL. Error in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);

  switch (browse_type) {
  case BEGINNING_IO:
    while (pcatch->prev != NULL)
      pcatch = pcatch->prev;
    break;
  case END_IO:
    while (pcatch->next != NULL)
      pcatch = pcatch->next;
    break;
  default:
    LP_error(fpout, "Unknown browse type. Error in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);
    break;
  }
  return pcatch;
}

/**
 * @brief Displays catchments properties (name, size, number of cprod within the catchment, etc.)
 * @return None
 */
void FP_print_catchments_properties_from_chain(s_catchment_fp *pcatch, FILE *fpout, int browse_type) {
  s_catchment_fp *ptmp;
  char *catch_name;

  if (pcatch == NULL)
    LP_error(fpout, "Catchment properties display impossible. Catchment chain is NULL. Error in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);
  ptmp = FP_browse_catchment_chain(pcatch, browse_type, fpout);
  while (ptmp != NULL) {
    catch_name = FP_catchment_type(ptmp->catchment_type);
    LP_printf(fpout, "Catchment name : %s. Type : %s. GIS ID %d. Area : %.2f (m2). Concentration time (sec) : %.1f. Number of cprod within catchment : %d\n", ptmp->catchment_name, catch_name, ptmp->id_gis, ptmp->area, ptmp->concentration_time, ptmp->nb_cprod_catchment);
    ptmp = ptmp->next;
    free(catch_name);
  }
}

/**
 * @brief Debug function : Prints the catchment ID the cprod belongs to.
 * @return None
 */
void FP_check_cprod_catchment_connexion(s_carac_fp *pcarac, FILE *fpout) {
  int nb_cprod = pcarac->nb_cprod;
  int i;
  char *cprod_type;

  for (i = 0; i < nb_cprod; i++) {
    if (pcarac->p_cprod[i]->p_catch != NULL) {
      cprod_type = FP_cprod_type(pcarac->p_cprod[i]->cell_prod_type);
      LP_printf(fpout, "Cprod GIS ID %d belongs to the catchment %s. Cprod type : %s\n", pcarac->p_cprod[i]->id[FP_GIS], pcarac->p_cprod[i]->p_catch->catchment_name, cprod_type);
      free(cprod_type);
    }
  }
}

/**
 * @brief Prints entire set of catchment properties with a
 * focus on the outlet cell prod attributes.
 * @return None
 */
void FP_print_outlet_cprod_type(s_carac_fp *pcarac_fp, FILE *fpout) {
  s_catchment_fp *ptmp;
  char *catch_type;
  char *cprod_type;

  ptmp = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_IO, fpout);

  LP_printf(fpout, " ********* Catchments characteristics  ********* \n");

  while (ptmp != NULL) {
    catch_type = FP_catchment_type(ptmp->catchment_type);
    cprod_type = FP_cprod_type(ptmp->pcprod_outlet->cell_prod_type);
    LP_printf(fpout, "Catchment : Name : %s. Type : %s. GIS ID : %d. Surface [m2] : %.1f. Concentration time [d] : %.1f. Number of Cprod cells : %d. OUTLET : Cprod outlet GIS ID %d. Outlet type : %s.\n", ptmp->catchment_name, catch_type, ptmp->id_gis, ptmp->area, ptmp->concentration_time / TS_unit2sec(DAY_TS, fpout), ptmp->nb_cprod_catchment, ptmp->pcprod_outlet->id[FP_GIS], cprod_type);
    ptmp = ptmp->next;
    free(catch_type);
    free(cprod_type);
  }
}

/**
 * @brief Checks if user-defined input catchement IDs are all different (no duplicates).
 * Function accounts for all catchement types (i.e. river and chasm).
 * @return None
 * @internal Useful in case of definition of both types of catchments (which is made
 * in two different cawaqs inputfiles). @endinternal
 */
void FP_check_catchment_duplicate_ids(s_catchment_fp *pchain, FILE *fpout) {

  s_catchment_fp *ptmp = NULL, *pref = NULL;
  int count;

  if (pchain == NULL)
    LP_error(fpout, "libfp%4.2f : Error in file %s, function %s at line %d : %s. Catchment pointer chain is NULL\n.", VERSION_FP, __FILE__, __func__, __LINE__);

  pref = FP_browse_catchment_chain(pchain, BEGINNING_IO, fpout);

  while (pref != NULL) {
    count = 0;
    ptmp = FP_browse_catchment_chain(pchain, BEGINNING_IO, fpout);
    while (ptmp != NULL) {
      if (ptmp->id_gis == pref->id_gis) {
        count++;
        if (count > 1)
          LP_error(fpout, "libfp%4.2f : Error in file %s, function %s at line %d. Duplicate catchement GIS ID %d detected. Check your inputfiles.\n", VERSION_FP, __FILE__, __func__, __LINE__, pref->id_gis);
      }
      ptmp = ptmp->next;
    }
    pref = pref->next;
  }
}

/**
 * @brief Determines the catchment concentration time based on its surface. Overwrites the default
 * value defined at initialization.
 * @return None
 * @internal Useful to automatically set catchment TCs for HDERM module.
 */
void FP_calibrate_hderm_catchment_tc(s_catchment_fp *pcatch, s_carac_fp *pcarac, FILE *fpout) {

  double tc_old = 0.;
  double alpha = pcarac->param_tc[FP_ALPHA];
  double beta = pcarac->param_tc[FP_BETA];
  double surf_threshold = pcarac->thresh_mean_surf;

  if (pcatch == NULL)
    LP_error(fpout, "Input catchment pointer is NULL. Error in function %s, file %s at line %d.\n", __func__, __FILE__, __LINE__);

  if (pcatch->area > pcarac->thresh_mean_surf) {
    tc_old = pcatch->concentration_time;
    pcatch->concentration_time = floor((alpha * pow(pcatch->area / surf_threshold, beta)) * tc_old);
    LP_printf(fpout, "Concentration time for catchment %d (Surf %f) has been automatically calibrated : %f sec. (old value : %f sec. )\n", pcatch->id_gis, pcatch->area, pcatch->concentration_time, tc_old); // check NG
  }
}