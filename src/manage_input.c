/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libfp
 * FILE NAME: manage_input.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-Soil water balance calculations
 * using a conceptual 7-parameter reservoir model.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libfp Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_input.c
 * @brief       Functions dealing with instances and attributes of the input_fp structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP_full.h"

/**
 * @brief Creates and initializes a *s_input_fp pointer
 * @return Newly created *s_input_fp pointer
 */
s_input_fp *FP_create_input() {

  int i;
  s_input_fp *pinput;

  pinput = new_input_fp();
  bzero((char *)pinput, sizeof(s_input_fp));

  pinput->prefix = (char **)malloc(FP_NMETEO * sizeof(char *));
  for (i = 0; i < FP_NMETEO; i++)
    pinput->prefix[i] = (char *)malloc(STRING_LENGTH_LP * sizeof(char));

  pinput->input_paths = (char **)malloc(FP_NPATHS * sizeof(char *));
  for (i = 0; i < FP_NPATHS; i++)
    pinput->input_paths[i] = (char *)malloc(STRING_LENGTH_LP * sizeof(char)); // NG : 10/06/2021

  FP_set_default_input(pinput);

  // Transport-related attributes initializations
  pinput->transport_forcing_conditions = NULL;
  pinput->transport_input_format = NULL;
  pinput->transport_input_prefixes = NULL;
  pinput->forcing_unit = NULL;
  pinput->transport_paths = NULL;
  return pinput;
}

/**
 * @brief Sets default attributes for meteo files (prefixes and format)
 * @return None
 */
void FP_set_default_input(s_input_fp *pinput) {

  int i;

  for (i = 0; i < FP_NPATHS; i++)
    pinput->input_paths[i] = NULL;

  sprintf(pinput->prefix[FP_RAIN], "%s", "prcp");
  sprintf(pinput->prefix[FP_ETP], "%s", "pet");
  pinput->format = UNFORMATTED_IO;
}

/**
 * @brief Prints out input configuration attributes for a given type of files
 * @return None
 */
void FP_check_input_setup(s_input_fp *pinput, int input_type, FILE *fpout) {

  int i;

  if (pinput->input_paths[input_type] == NULL) {
    LP_error(fpout, "In libfp%4.2f : Error in %s, in function %s : %s folder path left undeclared. Check your command file.\n", VERSION_FP, __FILE__, __func__, FP_input_types(input_type));
  } else {
    LP_printf(fpout, "%s folder path : %s\n", FP_input_types(input_type), pinput->input_paths[input_type]);
  }

  LP_printf(fpout, "File format type : %s\n", IO_name_ext(pinput->format));

  switch (input_type) {
  case FP_MTO: {
    for (i = 0; i < FP_NMETEO; i++)
      LP_printf(fpout, "%s prefix : %s\n", FP_name_mto(i), pinput->prefix[i]);
  } break;
  case FP_LAI:
    LP_printf(fpout, "%s prefix : %s\n", FP_input_types(input_type), pinput->prefix_lai);
    break;
  default:
    LP_error(fpout, "libfp%4.2f, File %s in %s line %d : Unknown input type\n", VERSION_FP, __FILE__, __func__, __LINE__);
    break;
  }
}

/**
 * @brief Allocates or reallocates pinput transport-related attributes each time
 * a new specie is being read
 * @return None
 */
void FP_manage_memory_input_species(s_carac_fp *pcarac_fp, int nb_species, FILE *flog) {
  int id_species = nb_species - 1;
  int i;
  s_input_fp *pinput = pcarac_fp->pinputs;

  if (pinput == NULL)
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : pinput pointer is NULL !\n", VERSION_FP, __FILE__, __func__, __LINE__);

  if (pinput->transport_forcing_conditions != NULL) { // At least, a first specie has been detected already
    pinput->transport_forcing_conditions = (int *)realloc(pinput->transport_forcing_conditions, nb_species * sizeof(int));
    pinput->transport_input_format = (int *)realloc(pinput->transport_input_format, nb_species * sizeof(int));
    pinput->forcing_unit = (double **)realloc(pinput->forcing_unit, nb_species * sizeof(double));
    pinput->transport_input_prefixes = (char ***)realloc(pinput->transport_input_prefixes, nb_species * sizeof(char **));
    pinput->transport_paths = (char **)realloc(pinput->transport_paths, nb_species * sizeof(char *));
  } else {
    pinput->transport_forcing_conditions = (int *)malloc(nb_species * sizeof(int));
    pinput->transport_input_format = (int *)malloc(nb_species * sizeof(int));
    pinput->transport_input_prefixes = (char ***)malloc(nb_species * sizeof(char **));
    pinput->forcing_unit = (double **)malloc(nb_species * sizeof(double *));
    pinput->transport_paths = (char **)malloc(nb_species * sizeof(char *));
  }

  // Re-allocating
  pinput->transport_input_prefixes[id_species] = (char **)malloc(FP_NVAR_INPUT_TRANSPORT * sizeof(char *));
  pinput->forcing_unit[id_species] = (double *)malloc(FP_NVAR_INPUT_TRANSPORT * sizeof(double));

  if (pinput->transport_forcing_conditions == NULL || pinput->transport_input_format == NULL || pinput->transport_input_prefixes == NULL || pinput->transport_input_prefixes[id_species] == NULL || pinput->forcing_unit == NULL || pinput->forcing_unit[id_species] == NULL) {
    LP_error(flog, "libfp%4.2f, File %s in %s line %d : Bad memory management for inputs attributes in pointers for species id %d !\n", VERSION_FP, __FILE__, __func__, __LINE__, id_species);
  }

  FP_set_default_input_settings_species(pinput, id_species, flog);
}

/**
 * @brief Sets some input default values for pinput transport-related attributes after
 * memory has been allocated
 * @return None
 */
void FP_set_default_input_settings_species(s_input_fp *pinput, int id_species, FILE *flog) {

  int i;

  pinput->transport_input_format[id_species] = UNFORMATTED_IO;
  pinput->transport_paths[id_species] = NULL;
  for (i = 0; i < FP_NVAR_INPUT_TRANSPORT; i++) {
    pinput->transport_input_prefixes[id_species][i] = NULL;
    pinput->forcing_unit[id_species][i] = 1.; // Default multiplication factor !
  }
}

/**
 * @brief Prints general input attributes for a given specie. Also checks the
 * consistency between forcing mode and input files.
 * @return None
 */
void FP_check_input_transport_setup(s_input_fp *pinput, int id_species, FILE *fpout) {

  int i;

  // Checking the path for the current specie
  if (pinput->transport_paths[id_species] == NULL) {
    LP_error(fpout, "In libfp%4.2f : Error in %s, in function %s : Transport folder path left undeclared for specie %d. Check your command file.\n", VERSION_FP, __FILE__, __func__, id_species);
  } else {
    LP_printf(fpout, "Data folder path for specie %d : %s\n", id_species, pinput->transport_paths[id_species]);
  }

  // Printing general attributes settings
  LP_printf(fpout, "Id_specie : %d\n", id_species);
  LP_printf(fpout, "Selected forcing mode : %s.\n", FP_transport_forcing_conditions(pinput->transport_forcing_conditions[id_species]));
  for (i = 0; i < FP_NVAR_INPUT_TRANSPORT; i++) {
    if (pinput->transport_input_prefixes[id_species][i] != NULL) {
      LP_printf(fpout, "Forced variable : %s. Associated prefix : %s\n", FP_transport_input_variable_name(i), pinput->transport_input_prefixes[id_species][i]);
    }
  }
  FP_check_transport_input_types(pinput, id_species, fpout);
}

/**
 * @brief Checks the adequacy between surface forcing conditions and input types each type
 * a new specie is being read. Throws an error if a inconsistency is detected.
 * @return None
 */
void FP_check_transport_input_types(s_input_fp *pinput, int id_species, FILE *flog) {

  int forcing_mode, failure, i;
  forcing_mode = pinput->transport_forcing_conditions[id_species];
  failure = NO;

  switch (forcing_mode) {
  case FP_TOTAL_TRFLUX: {
    if (pinput->transport_input_prefixes[id_species][FP_TRFLUX] == NULL) {
      failure = YES;
    }
  } break;
  case FP_DISSOCIATED_TRFLUX: {
    if (pinput->transport_input_prefixes[id_species][FP_TRFLUX_INF] == NULL || pinput->transport_input_prefixes[id_species][FP_TRFLUX_RUNOFF] == NULL) {
      failure = YES;
    }
  } break;
  case FP_TOTAL_TRFLUX_WATER: {
    if (pinput->transport_input_prefixes[id_species][FP_TRFLUX] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER] == NULL) {
      failure = YES;
    }
  } break;
  case FP_DISSOCIATED_TRFLUX_WATER: {
    if (pinput->transport_input_prefixes[id_species][FP_TRFLUX_INF] == NULL || pinput->transport_input_prefixes[id_species][FP_TRFLUX_RUNOFF] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER_INF] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER_RUNOFF] == NULL) {
      failure = YES;
    }
  } break;
  case FP_TOTAL_TRVAR: {
    if (pinput->transport_input_prefixes[id_species][FP_TRVAR] == NULL) {
      failure = YES;
    }
  } break;
  case FP_TOTAL_TRVAR_WATER: {
    if (pinput->transport_input_prefixes[id_species][FP_TRVAR] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER] == NULL) {
      failure = YES;
    }
  } break;
  case FP_DISSOCIATED_TRVAR_WATER: {
    if (pinput->transport_input_prefixes[id_species][FP_TRVAR_INF] == NULL || pinput->transport_input_prefixes[id_species][FP_TRVAR_RUNOFF] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER_INF] == NULL || pinput->transport_input_prefixes[id_species][FP_WATER_RUNOFF] == NULL) {
      failure = YES;
    }
  } break;
  default:
    LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Unknown forcing mode for species id %d\n", VERSION_FP, __FILE__, __func__, __LINE__, i);
    break;
  }

  if (failure == YES) {
    LP_error(flog, "In libfp%4.2f, File %s in %s line %d : Inconsistency between input forcing files and input forcing mode. Check your command file for species INTERN id : %d.\n", VERSION_FP, __FILE__, __func__, __LINE__, id_species);
  }
}